/**
 * Module provides the possibility to set the connection settings.
 * The following services paths should be provided:
 * OAuth path
 * UIBackend path
 * API path
 * @type {ConnectionSettings}
 */
module.exports = ConnectionSettings;

function ConnectionSettings(opts) {
    var settings = {
        clientSystemId: 'Reltio UI',
        globalId: 'Reltio UI'
    };
    if (opts) {
        settings.tenant = opts.tenant;
        settings.uiBackendPath = opts.uiBackend;
    }

    function setTenant(aTenant) {
        settings.tenant = aTenant;
    }

    function getTenant() {
        return settings.tenant;
    }

    function setClientSystemId(aClientSystemId) {
        settings.clientSystemId = aClientSystemId;
    }

    function getClientSystemId() {
        return settings.clientSystemId;
    }

    function setGlobalId(aGlobalId) {
        settings.globalId = aGlobalId;
    }

    function getGlobalId() {
        return settings.globalId;
    }

    function setUiBackendPath(aUiBackendPath) {
        settings.uiBackendPath = aUiBackendPath;
    }

    function getUiBackendPath() {
        return settings.uiBackendPath;
    }

    return {
        setTenant: setTenant,
        getTenant: getTenant,
        setClientSystemId: setClientSystemId,
        getClientSystemId: getClientSystemId,
        setGlobalId: setGlobalId,
        getGlobalId: getGlobalId,
        setUiBackendPath: setUiBackendPath,
        getUiBackendPath: getUiBackendPath
    }
}
