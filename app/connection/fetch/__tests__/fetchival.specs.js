jest.disableAutomock();
require('whatwg-fetch');
require('jasmine-ajax');


var fetchival = require('../fetchival');

var requestJSON = fetchival('http://localhost', {
    mode: 'cors',
    headers: {'X-TEST': 'test'}
});

var requestText = fetchival('http://localhost', {
    mode: 'cors',
    headers: {'X-TEST': 'test'},
    responseAs: 'text'
});


describe('fetchival', function () {

    describe('requestText(posts) [text]', function () {
        var postsStr = requestText('posts');
        beforeEach(function () {
            jasmine.Ajax.install();
            jasmine.Ajax.stubRequest(/localhost/).andReturn({
                responseText:''
            });
        });

        afterEach(function () {
            jasmine.Ajax.uninstall();
        });
        it('should #get()', function (done) {
            postsStr
                .get()
                .then(function (data) {
                    expect(data.substring(0, 1)).toBe('');
                    done();
                })
                .catch(done)
        })
    });

    describe('requestJSON(posts) [json]', function () {
        beforeEach(function () {
            jasmine.Ajax.install();
            jasmine.Ajax.stubRequest(/localhost/).andReturn({
                response: '[123]'
            });
        });

        afterEach(function () {
            jasmine.Ajax.uninstall();
        });
        var posts = requestJSON('posts');

        it('should #get()', function (done) {
            posts
                .get()
                .then(function (arr) {
                    expect(arr.length).toBeDefined();
                    done();
                })
                .catch(done.fail)
        });


        it('should #get({ query: })', function (done) {
            posts
                .get({userId: 1})
                .then(function (arr) {
                    expect(arr.length).toBe(1);
                    done();
                })
                .catch(done.fail)
        });

        it('should #post({ data: }', function (done) {
            posts
                .post({title: 'foo'})
                .then(function (obj) {
                    expect(obj).toBeDefined();
                    done();
                })
                .catch(done.fail)
        });

        it('should #put({ data: })', function (done) {
            posts(1)
                .put({title: 'foo'})
                .then(function (obj) {
                    expect(obj).toEqual([123]);
                    done();
                })
                .catch(done.fail);
        });

        it('should #patch({ data: })', function (done) {
            posts(1)
                .patch({title: 'foo'})
                .then(function (obj) {
                    expect(obj).toEqual([123]);
                    done();
                })
                .catch(done.fail);
        });

        it('should #delete()', function (done) {
            posts(1)
                .delete()
                .then(function (obj) {
                    expect(obj).toEqual([123]);
                    done();
                })
                .catch(done.fail);
        });
    });

    describe('requestJSON(posts/1/comments)', function () {
        var posts = requestJSON('posts');
        var comments = posts(1 + '/comments');
        beforeEach(function () {
            jasmine.Ajax.install();
            jasmine.Ajax.stubRequest(/posts/).andReturn({
                response: '[]'
            });
        });

        afterEach(function () {
            jasmine.Ajax.uninstall();
        });

        it('should #get()', function (done) {
            comments
                .get()
                .then(function (arr) {
                    expect(arr.length).toBeDefined();
                    done();
                }).catch(done.fail)
        })
    });

    describe('requestJSON(not/found)', function () {
        var notFound = requestJSON('not/found');
        beforeEach(function () {
            jasmine.Ajax.install();
            jasmine.Ajax.stubRequest(/not\/found/).andReturn({
                status:404
            });
        });

        afterEach(function () {
            jasmine.Ajax.uninstall();
        });
        it('should fail with 404', function (done) {
            notFound
                .get()
                .catch(function (err) {
                    expect(err.response.status).toBe(404);
                    done();
                })
        })
    });
    describe('204 errpr', function () {
        var notFound = requestJSON('not/found');
        beforeEach(function () {
            jasmine.Ajax.install();
            jasmine.Ajax.stubRequest(/not\/found/).andReturn({
                status:204
            });
        });

        afterEach(function () {
            jasmine.Ajax.uninstall();
        });
        it('should fail with 204', function (done) {
            notFound
                .get()
                .then(function(res){
                    expect(res).toBe(null);
                    done();
                })
                .catch(done.fail)
        })
    })

})