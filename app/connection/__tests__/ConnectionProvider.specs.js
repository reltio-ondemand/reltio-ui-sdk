jest.unmock('../ConnectionProvider.js');
var ConnectionProvider = require('../ConnectionProvider');
describe('Connection provider tests', function () {
    it('should exists', function () {
        expect(ConnectionProvider).toBeDefined();
        expect(ConnectionProvider).toBeDefined();
    });

    it('should throw exception in case of not inited config', function () {
        expect(ConnectionProvider).toBeDefined();
        var func = function () {
            ConnectionProvider.connection();
        };
        expect(func).toThrowError();
    });
    it('should return connection in case of inited config', function () {
        expect(ConnectionProvider).toBeDefined();
        ConnectionProvider.init();
        expect(ConnectionProvider.connection()).toBeDefined();
    });
});
