jest.disableAutomock();
require('jasmine-ajax');
require('whatwg-fetch');

var Promise = window.Promise || require('es6-promise').Promise;
var Connection = require('../Connection');
var ConnectionSettings = require('../ConnectionSettings');

describe('Connection tests', function () {
    beforeAll(function () {
        jest.useRealTimers();
    });
    it('should create correct connection ', function () {
        expect(Connection).toBeDefined();
    });
    describe('Connection tests', function () {
        var settings;
        var connection;
        var uiBackendPath = 'http://ui-backend.com';
        beforeEach(function () {
            var tenantName = 'testTenant';
            settings = ConnectionSettings({
                tenant: tenantName,
                uiBackend: uiBackendPath
            });
        });
        it('should be inited correctly', function () {
            connection = new Connection(settings);
            expect(connection.getConfiguration()).toBe(settings);
        });
        it('should be reset correctly', function () {
            connection = new Connection(settings);
            expect(connection.getConfiguration()).toBe(settings);
            connection.reset();
            expect(function () {
                connection.getConfiguration()
            }).toThrow(new Error('Incorrect configuration'));
        });
        describe('Compose url method tests', function () {
            beforeEach(function () {
                connection = new Connection(settings);
            });
            it('Should exists', function () {
                expect(connection.__composeUrl).toBeDefined();
            });

            it('Should return the same url if it is not request to reltio services', function () {
                var url = 'http://reltio.com';
                expect(connection.__composeUrl(url)).toBe(url);
            });
            it('Should convert params to query', function () {
                var url = uiBackendPath + '/{0}/crosswalks/{1}/{2}';
                expect(connection.__composeUrl(url, ['zero', 'one', 'two'])).toBe(uiBackendPath + '/zero/crosswalks/one/two');
            });
            it('Should convert params to query even one param', function () {
                var url = uiBackendPath + '/{0}';
                expect(connection.__composeUrl(url, 'zero')).toBe(uiBackendPath + '/zero');
            });
            it('Should compose url based on uibackend url and tenant if the url starts not from http/https', function () {
                var url = '/search/{0}';
                expect(connection.__composeUrl(url, 'zero', 'customTenant')).toBe(uiBackendPath + '/services/api/customTenant/search/zero');
            });
            it('Should compose url based on uibackend url and use defualt tenant name in case of no tenant', function () {
                var url = '/search/{0}';
                expect(connection.__composeUrl(url, 'zero')).toBe(uiBackendPath + '/services/api/tenant/search/zero');
            });
            it('Should add the options array as query string if it is not POST/PUT methods', function () {
                var url = uiBackendPath + '/{0}',
                    expected = uiBackendPath + '/zero?a=b&b=c';
                expect(connection.__composeUrl(url, 'zero', null, false, ['a=b', 'b=c'])).toBe(expected);
            });
            it('Should correctly add the options array as query string if it is not POST/PUT methods', function () {
                var url = uiBackendPath + '/{0}?xxx=test',
                    expected = uiBackendPath + '/zero?xxx=test&a=b&b=c';
                expect(connection.__composeUrl(url, 'zero', null, false, ['a=b', 'b=c'])).toBe(expected);
            });
            it('Shouldn\'t add the options array as query string in case of POST/PUT methods', function () {
                var url = uiBackendPath + '/{0}',
                    expected = uiBackendPath + '/zero';
                expect(connection.__composeUrl(url, 'zero', null, true, ['a=b', 'b=c'])).toBe(expected);
            });
        });
        describe('Check is to reltio api method', function () {
            beforeEach(function () {
                connection = new Connection(settings);
            });

            it('Should return false in case of no parameters', function () {
                expect(connection.isToReltioApi()).toBeFalsy();
            });

            it('Should return true in case of non exact match with no exact parameter', function () {
                expect(connection.isToReltioApi(uiBackendPath + '/services/api/ab03ct61')).toBeTruthy();
            });

            it('Should return false in case of non exact match with exact parameter', function () {
                expect(connection.isToReltioApi('ui-backend.com/services/api/ab03ct61', true)).toBeFalsy();
            });

            it('Should return true in case of exact match with exact parameter', function () {
                expect(connection.isToReltioApi(uiBackendPath + '/services/api', true)).toBeTruthy();
            });

            it('Should return false in case of not match parameter', function () {
                expect(connection.isToReltioApi('https://idev-01.reltio.com/services/api')).toBeFalsy();
                expect(connection.isToReltioApi('https://idev-01.reltio.com/services/api', true)).toBeFalsy();
            });
        });
        describe('Check create service request url method', function () {
            beforeEach(function () {
                connection = new Connection(settings);
            });

            it('Should return the full service url based on service name', function () {
                expect(connection.__createRequestUrl('/test')).toBe(uiBackendPath + '/services/test')
            });
            it('Should add the query string to service url based on options map', function () {
                var options = {a: 'b', c: 'd'};
                expect(connection.__createRequestUrl('/test', options)).toBe(uiBackendPath + '/services/test?a=b&c=d');
                expect(connection.__createRequestUrl('/test?x=y', options)).toBe(uiBackendPath + '/services/test?x=y&a=b&c=d');
            });
            it('Should skip the undefined or null values in the query string to service url based on options map', function () {
                var options = {a: 'b', test: null, test2: undefined, c: 'd'};
                expect(connection.__createRequestUrl('/test', options)).toBe(uiBackendPath + '/services/test?a=b&c=d');
                expect(connection.__createRequestUrl('/test?x=y', options)).toBe(uiBackendPath + '/services/test?x=y&a=b&c=d');
            });
        });

        describe('Check create service request url method', function () {
            beforeEach(function () {
                connection = new Connection(settings);
            });

            it('Should return the full service url based on service name', function () {
                expect(connection.__createRequestUrl('/test')).toBe(uiBackendPath + '/services/test')
            });
            it('Should add the query string to service url based on options map', function () {
                var options = {a: 'b', c: 'd'};
                expect(connection.__createRequestUrl('/test', options)).toBe(uiBackendPath + '/services/test?a=b&c=d');
                expect(connection.__createRequestUrl('/test?x=y', options)).toBe(uiBackendPath + '/services/test?x=y&a=b&c=d');
            });
            it('Should skip the undefined or null values in the query string to service url based on options map', function () {
                var options = {a: 'b', test: null, test2: undefined, c: 'd'};
                expect(connection.__createRequestUrl('/test', options)).toBe(uiBackendPath + '/services/test?a=b&c=d');
                expect(connection.__createRequestUrl('/test?x=y', options)).toBe(uiBackendPath + '/services/test?x=y&a=b&c=d');
            });
        });
        describe('Check serviceRequest method', function () {
            var connection = null;
            beforeEach(function () {
                var connectionSettings = new ConnectionSettings();
                connectionSettings.setUiBackendPath('https://tst-01.reltio.com');
                connection = new Connection(connectionSettings);
                jasmine.Ajax.install();
            });

            afterEach(function () {
                jasmine.Ajax.uninstall();
            });
            it('Should create service request url and return promise', function (done) {

                jasmine.Ajax.stubRequest(/services\/api/).andReturn({
                    'response': '{"status": "ok"}'
                });
                connection.serviceRequest('api/test').then(function (response) {
                    var xhr = jasmine.Ajax.requests.mostRecent();
                    expect(xhr.url).toBe('https://tst-01.reltio.com/services/api/test');
                    expect(response.status).toBe('ok');
                    done();
                }).catch(function (e) {
                    done.fail(e)
                });
            });
        });
        describe('Check send request method', function () {
            var connection = null;
            beforeEach(function () {
                var connectionSettings = new ConnectionSettings();
                connectionSettings.setUiBackendPath('https://tst-01.reltio.com');
                connection = new Connection(connectionSettings);
                jasmine.Ajax.install();
            });

            afterEach(function () {
                jasmine.Ajax.uninstall();
            });

            it('Should throw exception if there is no configuration', function () {
                connection = new Connection();
                var func = function () {
                    connection.sendRequest();
                };
                expect(func).toThrowError();
            });


            it('Should return the promise with status ok in case of get', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/api/test').andReturn({
                    'response': '{"status": "ok"}'
                });
                connection.sendRequest('https://tst-01.reltio.com/services/api/test').then(function (response) {
                    expect(response.status).toBe('ok');
                    done();
                }).catch(function (e) {
                    done.fail(e)
                });
            });

            it('Should send the reltio headers in case of request to api', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/api/test').andReturn({
                    'response': '{"status": "ok"}'
                });
                connection.sendRequest('https://tst-01.reltio.com/services/api/test').then(function () {
                    var xhr = jasmine.Ajax.requests.mostRecent();
                    expect(xhr.requestHeaders['source-system']).toBe('Reltio');
                    expect(xhr.requestHeaders['clientsystemid']).toBe('Reltio UI');
                    expect(xhr.requestHeaders['globalid']).toBe('Reltio UI');
                    expect(xhr.requestHeaders['xxx-client']).toBe('true');
                    done();
                }).catch(function (e) {
                    done.fail(e)
                });
            });

            it('Shouldn\'t send the reltio headers in case of request to non api', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/dtss/test').andReturn({
                    'response': '{"status": "ok"}'
                });
                connection.sendRequest('https://tst-01.reltio.com/services/dtss/test').then(function () {
                    var xhr = jasmine.Ajax.requests.mostRecent();
                    expect(xhr.requestHeaders['source-system']).toBeUndefined();
                    expect(xhr.requestHeaders['clientsystemid']).toBeUndefined();
                    expect(xhr.requestHeaders['globalid']).toBeUndefined();
                    expect(xhr.requestHeaders['xxx-client']).toBeUndefined();
                    done();
                }).catch(function (e) {
                    done.fail(e)
                });
            });
            it('Should convert the post requests', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/api/test').andReturn({
                    'response': '{"status": "ok"}'
                });
                connection.sendRequest('https://tst-01.reltio.com/services/api/test', null, {
                    offset: 0,
                    max: 20,
                    favoriteOnly: false,
                    sortBy: 'NAME',
                    sortOrder: 'ASC',
                    includes: null,
                    startsWith: null,
                    method: 'POST'
                }).then(function () {
                    var xhr = jasmine.Ajax.requests.mostRecent();
                    expect(JSON.parse(xhr.params)).toEqual({
                        offset: 0,
                        max: 20,
                        favoriteOnly: false,
                        sortBy: 'NAME',
                        sortOrder: 'ASC',
                        includes: null,
                        startsWith: null
                    });
                    done();
                }).catch(function (e) {
                    done.fail(e)
                });
            });

            it('Shouldn\'t  send Source-System headers if noReltioSourceSystem true', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/api/test').andReturn({
                    'response': '{"status": "ok"}'
                });
                connection.sendRequest('https://tst-01.reltio.com/services/api/test', null, {noReltioSourceSystem: true})
                    .then(function () {
                        var xhr = jasmine.Ajax.requests.mostRecent();
                        expect(xhr.requestHeaders['source-system']).toBeUndefined();
                        done();
                    }).catch(function (e) {
                    done.fail(e)
                });
            });
            it('Should send the request to tenant which was passed as parameter', function (done) {
                jasmine.Ajax.stubRequest(/customTenant/).andReturn({
                    'response': '{"status": "ok"}'
                });
                connection.sendRequest('test', null, {tenant: 'customTenant'}).then(function (response) {
                    expect(response.status).toBe('ok');
                    done();
                }).catch(function (e) {
                    done.fail(e)
                });
            });

            it('Should use the option.data value as data', function (done) {
                jasmine.Ajax.stubRequest(/services\/api/).andReturn({
                    'response': '{"status": "ok"}'
                });
                connection.sendRequest('test', null, {data: {test: 'test'}, method: 'POST'}).then(function () {
                    var xhr = jasmine.Ajax.requests.mostRecent();
                    expect(JSON.parse(xhr.params)).toEqual({test: 'test'});
                    done();
                }).catch(function (e) {
                    done.fail(e)
                });
            });

            it('Should return relogin error automaticaly in case of 401 error', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/refreshToken', null, 'POST').andReturn({
                    'responseText': '{"status": "refreshed"}'
                });
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/api').andReturn({
                    status: 401,
                    'responseText': '{"status": "invalid"}'
                });
                connection.sendRequest('https://tst-01.reltio.com/services/api').then(function () {
                }).catch(function (e) {
                    expect(e.message).toBe('relogin');
                    done();
                });
            });

            it('Should return relogin error automaticaly in case of 401 error', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/refreshToken', null, 'POST').andReturn({
                    'responseText': '{"status": "refreshed"}'
                });
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/api').andReturn({
                    status: 401,
                    'responseText': '{"status": "invalid"}'
                });
                connection.sendRequest('https://tst-01.reltio.com/services/api').then(function () {
                }).catch(function (e) {
                    expect(e.message).toBe('relogin');
                    done();
                });
            });

        });

        describe('Check refreshToken method', function () {
            var connection = null;
            beforeEach(function () {
                var connectionSettings = new ConnectionSettings();
                connectionSettings.setUiBackendPath('https://tst-01.reltio.com');
                connection = new Connection(connectionSettings);
                jasmine.Ajax.install();
            });

            afterEach(function () {
                jasmine.Ajax.uninstall();
            });

            it('Should send request to the refreshToken service', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/refreshToken', null, 'POST').andReturn({
                    'responseText': '{"status": "refreshed"}'
                });
                connection.refreshToken().then(function () {
                    done();
                }).catch(function (e) {
                    done.fail(e)
                });
            });

            it('Shouldn\'t send twice  request to the refreshToken service', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/refreshToken', null, 'POST').andReturn({
                    'responseText': '{"status": "refreshed"}'
                });
                connection.refreshToken().then(function () {
                    return new Promise(function (resolve) {
                        setTimeout(function () {
                            resolve()
                        }, 100)
                    })
                }).catch(function (e) {
                    done.fail(e);
                });
                connection.refreshToken().then(function () {
                    expect(jasmine.Ajax.requests.count()).toBe(1);
                    done();
                }).catch(function (e) {
                    done.fail(e);
                });
            });
            it('Should return error if something goes wrong', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/refreshToken', null, 'POST').andError({
                    'responseText': 'error'
                });
                connection.refreshToken().catch(function () {
                    done();
                }).catch(function (e) {
                    done.fail(e);
                });
            });
        });
        describe('login method verification', function () {
            beforeEach(function () {
                var connectionSettings = new ConnectionSettings();
                connectionSettings.setUiBackendPath('https://tst-01.reltio.com');
                connection = new Connection(connectionSettings);
                jasmine.Ajax.install();
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/login').andReturn({
                    'response': '{"status": "ok"}'
                });
            });

            afterEach(function () {
                jasmine.Ajax.uninstall();
            });

            it('Should send post request to oauth on login', function (done) {
                var expected = 'grant_type=password&username=username&password=password';
                connection.login('username', 'password').then(function () {
                    var xhr = jasmine.Ajax.requests.mostRecent();
                    expect(xhr.params).toBe(expected);
                    expect(xhr.requestHeaders['content-type']).toBe('application/x-www-form-urlencoded');
                    done();
                }).catch(function (e) {
                    done.fail(e);
                });
            });
        });

        describe('logout method verification', function () {
            beforeEach(function () {
                var connectionSettings = new ConnectionSettings();
                connectionSettings.setUiBackendPath('https://tst-01.reltio.com');
                connection = new Connection(connectionSettings);
                jasmine.Ajax.install();
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/logout').andReturn({
                    'response': '{"status": "ok"}'
                });
            });

            afterEach(function () {
                jasmine.Ajax.uninstall();
            });

            it('Should send post request to oauth on logout without params and resolve promise', function (done) {
                connection.logout().then(function () {
                    var xhr = jasmine.Ajax.requests.mostRecent();
                    expect(xhr.params).toBeNull();
                    expect(xhr.requestHeaders['content-type']).toBe('application/x-www-form-urlencoded');
                    done();
                }).catch(function (e) {
                    done.fail(e);
                });
            });
        });
        describe('isLoggedIn method verification', function () {
            beforeEach(function () {
                var connectionSettings = new ConnectionSettings();
                connectionSettings.setUiBackendPath('https://tst-01.reltio.com');
                connection = new Connection(connectionSettings);
                jasmine.Ajax.install();
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/checkToken').andReturn({
                    'response': '{"status": "ok"}'
                });
            });

            afterEach(function () {
                jasmine.Ajax.uninstall();
            });

            it('Should send post request to checkToken on isLoggedIn without params and resolve promise', function (done) {
                connection.isLoggedIn().then(function () {
                    var xhr = jasmine.Ajax.requests.mostRecent();
                    expect(xhr.params).toBeNull();
                    expect(xhr.requestHeaders['content-type']).toBe('application/x-www-form-urlencoded');
                    done();
                }).catch(function (e) {
                    done.fail(e);
                });
            });

            it('Should reject promise in case of errors', function (done) {
                jasmine.Ajax.stubRequest('https://tst-01.reltio.com/services/checkToken').andError();
                connection.isLoggedIn().catch(function () {
                    done();
                });
            });
        });
    });

    afterAll(function () {
        jest.enableAutomock();
        jest.useFakeTimers();
    });
});
