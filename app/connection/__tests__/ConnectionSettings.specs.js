jest.unmock('../ConnectionSettings.js');
var ConnectionSettings = require('../ConnectionSettings');
describe('Connection settings tests', function () {
    it('should create correct connection settings project', function () {
        expect(ConnectionSettings).toBeDefined();
        var settings = ConnectionSettings();
        expect(settings).toBeDefined();
    });
    it('should use default settings for global id and system id', function () {
        var settings = ConnectionSettings();
        expect(settings.getGlobalId()).toBe('Reltio UI');
        expect(settings.getClientSystemId()).toBe('Reltio UI');
    });
    it('should use options from the parameter', function () {
        var tenantName = 'testTenant';
        var uiBackendPath = 'http://ui-backend.com';
        var settings = ConnectionSettings({
            tenant: tenantName,
            uiBackend: uiBackendPath
        });
        expect(settings.getGlobalId()).toBe('Reltio UI');
        expect(settings.getClientSystemId()).toBe('Reltio UI');
        expect(settings.getTenant()).toBe(tenantName);
        expect(settings.getUiBackendPath()).toBe(uiBackendPath);
    });

    it('should change parameters using the setters', function () {
        var tenantName = 'testTenant';
        var uiBackendPath = 'http://ui-backend.com';
        var globalId = 'globalId';
        var clientSystemId = 'clientId';
        var settings = ConnectionSettings();
        settings.setClientSystemId(clientSystemId);
        settings.setGlobalId(globalId);
        settings.setTenant(tenantName);
        settings.setUiBackendPath(uiBackendPath);
        expect(settings.getGlobalId()).toBe(globalId);
        expect(settings.getClientSystemId()).toBe(clientSystemId);
        expect(settings.getTenant()).toBe(tenantName);
        expect(settings.getUiBackendPath()).toBe(uiBackendPath);
    });
});
