var ConnectionSettings = require('./ConnectionSettings'),
    Connection = require('./Connection');

module.exports = {
    __connection: null,
    init: function (config) {
        var connectionSettings = new ConnectionSettings(config);
        this.__connection = new Connection(connectionSettings);
    },
    connection: function () {
        if (this.__connection){
            return this.__connection;
        }
        throw  new Error('Connection is not inited');
    }
};