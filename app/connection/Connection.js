var _ = require('lodash'),
    Promise = require('es6-promise').Promise,
    fetchival = require('./fetch/fetchival');

/**
 * @constructor
 * @param config {ConnectionSettings} connection settings
 */
var Connection = function Connection(config) {
    this.API_PATH = '/services/api';
    this.SERVICES_URL = '/services/';
    this.CHECK_TOKEN = '/services/checkToken';
    this.LOGIN = '/services/login';
    this.LOGOUT = '/services/logout';
    this.refreshTokenRequest = null;
    this.configuration = config;
    this.__refreshCounter = null;
};
/**
 * Check is request to reltio or not
 * @param url {String} url for check
 * @param exact {Boolean?} should it be exact check or not
 * @returns {boolean}
 */
Connection.prototype.isToReltioApi = function (url, exact) {
    var index;
    url = url || '';
    index = url.indexOf(this.configuration.getUiBackendPath() + this.API_PATH);
    return exact ? index === 0 : index !== -1;
};
/**
 * Method returns configuration settings
 * @returns {*}
 */
Connection.prototype.getConfiguration = function () {
    if (!this.configuration) {
        throw new Error('Incorrect configuration');
    }
    return this.configuration;
};

/**
 * Method clears all the settings
 */
Connection.prototype.reset = function () {
    this.configuration = null;
};

/**
 * @private
 * @param url {String} url
 * @param params {Array?} parameters which are replace the placeholder
 * @param tenant {String?} tenant name
 * @param needToSendData {boolean?} is it POST/PUT request or not
 * @param optionsArray {Array?} options which will be added to query string. The options format is key=value
 * @returns {*}
 */
Connection.prototype.__composeUrl = function (url, params, tenant, needToSendData, optionsArray) {
    if (!_.isEmpty(params)) {
        if (!_.isArray(params)) {
            params = [params];
        }
        url = url.replace(/\{(\d+)}/g, function (match, p1) {
            return params[p1];
        });
    }

    if (url.indexOf('http:') !== 0 && url.indexOf('https:') !== 0) {
        url = '/' + (tenant || 'tenant') + url;
        url = this.configuration.getUiBackendPath() + this.API_PATH + url;
    }

    if (!needToSendData && !_.isEmpty(optionsArray)) {
        url += (url.indexOf('?') == -1 ? '?' : '&') + optionsArray.join('&');
    }
    return url;
};
/**
 * @private
 * @param service
 * @param mapParams
 * @returns {string}
 */
Connection.prototype.__createRequestUrl = function (service, mapParams) {
    var url = this.configuration.getUiBackendPath() + this.SERVICES_URL + service.replace(/^\//g, '');
    if (mapParams) {
        if (url.indexOf('?') == -1)
            url += '?';
        else
            url += '&';
        url += _.reduce(mapParams, function (result, value, key) {
            return (!_.isNull(value) && !_.isUndefined(value)) ? (result += key + '=' + value + '&') : result;
        }, '').slice(0, -1);
    }
    return url;
};

/**
 * Method sends signed request to services
 * @param service {String} service url
 * @param options {Options?} options
 * @returns {*}
 */
Connection.prototype.serviceRequest = function (service, options) {
    var url = this.__createRequestUrl(service);
    return this.sendRequest(url, null, options, true);
};

/**
 * Method sends signed request to reltio api or special url
 * Url can start from http/https or can be dynamically composed.
 * In second case the request will be send to reltio API.
 * @param url {String} url.
 * Parameters array contains the parameters which are needed to replace the placeholder in the url
 * @example if url is http://tst.reltio.com/{1}/test/{2}, and params array is ['a','b'], then the result will be
 * http://tst.reltio.com/a/test/b
 * @param params {Array?} parameters
 * @typedef {Object} Options
 * @property {String} method - method type ['POST','GET','PUT','DELETE']
 * @property {Object} data - data to send
 * @property {String} tenant - specific tenant name
 * @property {Boolean} noReltioSourceSystem - should the reltio headers be send or not
 * @property {Object} headers - request headers
 * @param options {Options|Object?} options
 * @returns {*}
 */
Connection.prototype.sendRequest = function (url, params, options) {
    if (!this.configuration) {
        throw new Error('Connection is not inited');
    }
    options = options || {};
    var optionsArray = [],
        method = null,
        data = null,
        tenant = this.configuration.getTenant(),
        noReltioSourceSystem = false,
        needToSendData;

    _.forEach(options, function (value, key) {
        if (key == 'method')
            method = value;
        else if (key == 'data')
            data = value;
        else if (key == 'tenant')
            tenant = value;
        else if (key == 'noReltioSourceSystem')
            noReltioSourceSystem = value;
        else
            optionsArray.push(key + '=' + value);
    });
    delete options.method;
    delete options.data;
    delete options.noReltioSourceSystem;
    if (data === null) {

        data = _.clone(options);
        delete data.headers;
    }
    needToSendData = method == 'POST' || method == 'PUT';

    url = this.__composeUrl(url, params, tenant, needToSendData, optionsArray);
    method = method || 'GET';
    options.responseAs = options.responseAs || 'json';
    options.headers = options.headers || {};
    if (this.isToReltioApi(url) && !noReltioSourceSystem) {
        options.headers['Source-System'] = 'Reltio';
    }
    if (needToSendData && !options.headers['Content-Type']) {
        options.headers['Content-Type'] = 'application/json'
    }
    if (this.isToReltioApi(url, true)) {
        options.headers['globalId'] = this.configuration.getGlobalId();
        options.headers['clientSystemId'] = this.configuration.getClientSystemId();
        options.headers['xxx-client'] = 'true';
    }
    method = method.toLowerCase();
    return this.__sendRequestInternal(method, url, data, options);
};
/**
 * @private
 * @param method
 * @param url
 * @param data
 * @param options
 * @returns {*}
 * @private
 */
Connection.prototype.__sendRequestInternal = function (method, url, data, options) {
    var args = Array.prototype.slice.call(arguments);
    var me = this;
    if (this.refreshTokenRequest) {
        return this.refreshTokenRequest.then(function () {
            return me.__sendRequestInternal.apply(me, args);
        })
    }
    data = _.isEmpty(data) ? null : data;
    options = options || {};
    options.credentials = 'same-origin';
    return fetchival(url, options)[method](data).then(function (e) {
        me.__refreshCounter = 0;
        return Promise.resolve(e);
    }).catch(function (e) {
        if (e.response && e.response.status === 401 && me.__refreshCounter < 3) {
            me.__refreshCounter++;
            me.refreshToken();
            return me.__sendRequestInternal.apply(me, args);
        }
        return Promise.reject(new Error('relogin'));
    });
};
/**
 * Method allows to refresh token
 * @returns {null|*}
 */
Connection.prototype.refreshToken = function () {
    var me = this;
    if (!this.refreshTokenRequest) {
        this.refreshTokenRequest = fetchival(this.__createRequestUrl('/refreshToken'), {
            credentials: 'same-origin',
            responseAs: 'response'
        }).post()
            .then(function (response) {
                me.refreshTokenRequest = null;
                return Promise.resolve(response);
            })
            .catch(function (_e) {
                me.refreshTokenRequest = null;
                return Promise.reject(new Error('relogin'));
            });
    }

    return this.refreshTokenRequest;
};
Connection.prototype.login = function (name, password) {
    var url = this.configuration.getUiBackendPath() + this.LOGIN;
    var toParam = function(json){
        return Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
    };
    var params = toParam({
        grant_type: 'password',
        username: name,
        password: password
    });
    return fetchival(url, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'same-origin',
        responseAs: 'text'
    }).post(params);
};
Connection.prototype.logout = function () {
    var url = this.configuration.getUiBackendPath() + this.LOGOUT;
    return fetchival(url, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).post();
};

Connection.prototype.isLoggedIn = function () {
    var url = this.configuration.getUiBackendPath() + this.CHECK_TOKEN;
    return fetchival(url, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        credentials: 'same-origin'
    }).post();
};
module.exports = Connection;
