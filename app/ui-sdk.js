module.exports = {
    entity: {
        AttributesDiff: require('./entity/editMode/AttributesDiff'),
        AttributesEditor: require('./entity/editMode/AttributesEditor'),
        HistoryManager: require('./entity/history/HistoryManager'),
        MultiparentTree: require('./entity/editMode/MultiparentTree'),
        Transactions: require('./entity/transactions/Transactions')
    },
    utils: {
        Colors: require('./utils/Colors'),
        Util: require('./utils/Util'),
        DependencyManager: require('./utils/DependencyManager')
    },
    Metadata : require('./metadata/Metadata')
};