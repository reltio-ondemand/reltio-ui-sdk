var Util = require('../utils/Util.js'),
    _ = require('lodash');

function Metadata() {
    this.rawConfiguration = null;
    this.configuration = null;
    this.entityTypes = null;
    this.relationTypes = null;
    this.groupTypes = null;
    this.graphTypes = null;
    this.categoryTypes = null;
    this.interactionTypes = null;
    this.xwalkTypes = null;
    this.survivorshipStrategies = null;
    this.roles = null;
}

Metadata.prototype.init = function(inititalConfiguration) {
    inititalConfiguration = _.defaults(inititalConfiguration, {
        entityTypes: [],
        roles: [],
        relationTypes: [],
        groupTypes: [],
        graphTypes: [],
        sources: [],
        categoryTypes: [],
        interactionTypes: [],
        survivorshipStrategies: []
    });

    this.rawConfiguration = _.cloneDeep(inititalConfiguration);
    this.configuration = inititalConfiguration;
    var attributeTypes = (inititalConfiguration.attributeTypes || [])
        .reduce(function(acc, type) {
            acc[type.uri] = type;
            return acc;
        }, {});
    this.entityTypes = this.processEntityTypes(inititalConfiguration.entityTypes, attributeTypes);

    this.roles = inititalConfiguration.roles.reduce(function(acc, role) {
        acc[role.uri] = role;
        return acc;
    }, {});
    this.relationTypes = this.processTypes(inititalConfiguration.relationTypes, attributeTypes);
    this.groupTypes = this.processTypes(inititalConfiguration.groupTypes, attributeTypes);
    this.graphTypes = this.processTypes(inititalConfiguration.graphTypes, attributeTypes);
    this.categoryTypes = this.processTypes(inititalConfiguration.categoryTypes, attributeTypes);
    this.interactionTypes = this.processTypes(inititalConfiguration.interactionTypes, attributeTypes);
    this.xwalkTypes = this.processXwalkTypes(inititalConfiguration.sources);
    this.survivorshipStrategies = this.processSurvivorshipStrategies(inititalConfiguration.survivorshipStrategies);
};
Metadata.prototype.getRawConfiguration = function() {
    return this.rawConfiguration;
};
Metadata.prototype.getEntityType = function(entityType) {
    return this.entityTypes[entityType];
};

Metadata.prototype.getEntityTypes = function() {
    return this.entityTypes;
};

Metadata.prototype.getRelationType = function(relationType) {
    return this.relationTypes[relationType];
};

Metadata.prototype.getRelationTypes = function() {
    return this.relationTypes;
};

Metadata.prototype.getRole = function(name) {
    return this.roles[name];
};

Metadata.prototype.getRoles = function() {
    return this.roles;
};

Metadata.prototype.getGroupType = function(name) {
    return this.groupTypes[name];
};

Metadata.prototype.getGroupTypes = function() {
    return this.groupTypes;
};

Metadata.prototype.getCategoryTypes = function() {
    return this.categoryTypes;
};


Metadata.prototype.getGraphType = function(name) {
    return this.graphTypes[name];
};

Metadata.prototype.getGraphTypes = function() {
    return this.graphTypes;
};

Metadata.prototype.getInteractionType = function(name) {
    return this.interactionTypes[name];
};

Metadata.prototype.getInteractionTypes = function() {
    return this.interactionTypes;
};

Metadata.prototype.getXwalkType = function(name) {
    return this.xwalkTypes.filter(function(xwalkType) {
        return xwalkType.uri === name ||
            xwalkType.label === name ||
            xwalkType.uri.substring(xwalkType.uri.lastIndexOf('/') + 1) === name;
    })[0]
};

Metadata.prototype.getXwalkTypes = function(sorted) {
    return sorted ? _.sortBy(this.xwalkTypes, ['label']) : this.xwalkTypes;
};

Metadata.prototype.getSurvivorshipStrategy = function(uri) {
    return this.survivorshipStrategies[uri.substring(uri.lastIndexOf('/') + 1)];
};

Metadata.prototype.getSurvivorshipStrategies = function() {
    return this.survivorshipStrategies;
};

/**
 * processXwalkTypes - Description
 * @private
 * @param {type} sources Description
 * @return {type} Description
 */
Metadata.prototype.processXwalkTypes = function(sources) {
    var result = _.cloneDeep(sources);
    var hasReltioSource = result.some(function(xwalkType) {
        return xwalkType.uri === 'configuration/sources/Reltio';
    });
    if (!hasReltioSource) {
        result.push({
            uri: 'configuration/sources/Reltio',
            name: 'Reltio',
            description: '',
            label: 'Reltio',
            icon: 'images/source/reltio.png',
            abbreviation: 'Reltio'
        })
    }
    return result;
};

/**
 * processSurvivorshipStrategies - Description
 * @private
 * @param {type} survivorshipStrategies Description
 *
 * @return {type} Description
 */
Metadata.prototype.processSurvivorshipStrategies = function(survivorshipStrategies) {
    return survivorshipStrategies.reduce(function(acc, strategy) {
        var shortUri = strategy.uri.substring(strategy.uri.lastIndexOf('/') + 1);
        acc[shortUri] = strategy;
        return acc;
    }, {});
};

/**
 * processEntityTypes - Description
 * @private
 * @param {type} entityTypes    Description
 * @param {type} attributeTypes Description
 *
 * @return {type} Description
 */
Metadata.prototype.processEntityTypes = function(entityTypes, attributeTypes) {
    var me = this;

    var uris = entityTypes.map(function(entityType) {
        return entityType.uri;
    });

    if (Util.getDuplicates(uris).length > 0) {
        // console.log(uris);
    }

    var result = entityTypes.reduce(function(acc, entityType) {
        acc[entityType.uri] = me.processEntityType(entityType, attributeTypes);
        return acc;
    }, {});

    Object.keys(result)
        .map(function(type) {
            return result[type];
        })
        .filter(function(entityType) {
            return (
                entityType.extendsTypeURI &&
                result[entityType.extendsTypeURI]
            );
        })
        .forEach(function(entityType) {
            var parent = entityType;
            while ((parent = result[parent.extendsTypeURI])) {
                Util.merge(parent, entityType, [
                    'abstract',
                    'uiConfigHiddenType'
                ]);
            }
        }, this);
    return result;
};

/**
 * processTypes - Description
 * @private
 * @param {type} types          Description
 * @param {type} attributeTypes Description
 *
 * @return {type} Description
 */
Metadata.prototype.processTypes = function(types, attributeTypes) {
    var me = this;
    return types.reduce(function(acc, type) {
        acc[type.uri] = me.marshallAttributes(attributeTypes, type);
        return acc;
    }, {});
};

/**
 * processEntityType - Description
 * @private
 * @param {type} entityType     Description
 * @param {type} attributeTypes Description
 *
 * @return {type} Description
 */
Metadata.prototype.processEntityType = function(entityType, attributeTypes) {
    var result = _.defaults(entityType, {
        entityTypeRoleURIs: [],
        attributes: [],
        businessCardAttributeURIs: []
    });
    _.flatMap(result.dependentAttributes || [], function(dependentAttribute) {
        return dependentAttribute.values;
    }).forEach(function(value) {
        value.valuesList = value.valuesList.map(function(valueItem) {
            return valueItem.toUpperCase();
        });
    });
    return this.marshallAttributes(attributeTypes, result);
};

/**
 * marshallAttributes - Description
 * @private
 * @param {type} src            Description
 * @param {type} attributeTypes Description
 *
 * @return {type} Description
 */
Metadata.prototype.marshallAttributes = function(attributeTypes, src) {
    var result = src;
    var cMarshalAttribute = _.curry(this.marshallAttribute)(
        attributeTypes
    );
    if (result.attributes && attributeTypes) {
        result.attributes = result.attributes.map(cMarshalAttribute, this);
    }
    return result;
};

/**
 * marshallAttribute - Description
 * @private
 * @param {type} attribute      Description
 * @param {type} attributeTypes Description
 *
 * @return {type} Description
 */
Metadata.prototype.marshallAttribute = function(attributeTypes, attribute) {
    if (attribute.type === 'Nested' && attribute.attributes) {
        var cMarshalAttribute = _.curry(this.marshallAttribute)(
            attributeTypes
        );
        attribute.attributes = attribute.attributes.map(cMarshalAttribute, this);
    } else {
        var attributeType = attributeTypes[attribute.type];
        if (attributeType) {
            if (attributeType.extendsTypeURI && attributeTypes[attributeType.extendsTypeURI]) {
                for (
                    var parent = attributeTypes[attributeType.extendsTypeURI]; parent; parent = attributeTypes[parent.extendsTypeURI]
                ) {
                    Util.merge(parent, attributeType, ['uri']);
                }
            }
            attribute.type = attributeType.valueType || 'String';
        }
    }
    return attribute;
};


/**
 * Returns hierarchy of attribute metadata objects for uris like 'configuration/entityTypes/Location/attributes/Zip/attributes/PostalCode'
 * Supports reference attribute aliases like 'configuration/entityTypes/HCP/attributes/Address/attributes/AddressRank', for such cases you should use skipUriCheck
 */
Metadata.prototype.getEntityAttributeHierarchy = function(uri, skipUriCheck) {
    var isAnalytic = Metadata.isAnalyticAttribute(uri);
    var attrCollectionName = isAnalytic ? 'analyticsAttributes' : 'attributes';
    var namesPath = uri.split('/' + attrCollectionName + '/');
    if (namesPath.length < 2)
        return;

    var entityTypeUri = namesPath[0];
    var entityType = this.getEntityType(entityTypeUri) || this.getRelationType(entityTypeUri);
    if (!entityType)
        return;

    // uri may look like: configuration/entityTypes/Location/attributes/Zip/attributes/PostalCode
    var parentObjAttrs = entityType[attrCollectionName];
    var parentObj = entityType;
    var ret = [parentObj];
    for (var i = 1; i < namesPath.length; ++i) { // start with 1, because parent object is already in the 'ret'
        var lookForName = namesPath[i];
        var oldLength = ret.length;

        for (var j = 0; j < parentObjAttrs.length; ++j) {
            if (parentObjAttrs[j] && parentObjAttrs[j].name == lookForName) {
                parentObj = parentObjAttrs[j];
                if (parentObj.type === 'Reference') {
                    parentObjAttrs = parentObj.referencedAttributeURIs
                        .map(function (uri) {
                            return this.getAttributeByUri(uri);
                        }, this);
                }
                else {
                    parentObjAttrs = parentObjAttrs[j][attrCollectionName] || [];
                }
                ret.push(parentObj);
                break;
            }
        }

        if (oldLength === ret.length)
            return null;
    }
    if (!skipUriCheck && parentObj.uri !== uri)
        ret = null;
    return ret;
};

/**
 * Obtain attribute value from an entity object by a given uri.
 * @param entity {Object}
 * @param attrTypeUri {String} type uri
 * @param group {Boolean} if true values will be grouped by their parents
 */
Metadata.prototype.findAttributeValueByTypeUri = function(entity, attrTypeUri, group) {
    var ret = null;
    var attrTypeChain = this.getEntityAttributeHierarchy(attrTypeUri, true);
    var entityType = this.getEntityType(entity.type);
    if (!attrTypeChain || attrTypeChain.length === 0)
        return null;

    var isAnalytic = Metadata.isAnalyticAttribute(attrTypeUri);
    var attrCollectionName = isAnalytic ? 'analyticsAttributes' : 'attributes';
    var parentObj = [entity];
    var startIndex = 1;
    if (attrTypeChain[0].uri !== entity.type) {
        startIndex = 0;
    }
    ret = null;
    var groups = group ? [] : null;
    var referencedUris = null;

    for (var i = startIndex; i < attrTypeChain.length; ++i) {
        var last = i === attrTypeChain.length - 1;
        var lookForAttr = attrTypeChain[i];
        var parentTypes = [];
        if (!Metadata.isSubAttributeAllowed(lookForAttr, referencedUris)) {
            ret = [];
            break;
        }
        if (Array.isArray(parentObj)) {
            for (var k = 0; k < parentObj.length; k++) {
                var objValue = (parentObj[k].value || parentObj[k][attrCollectionName]) || {};
                var parents = [];
                // reference attribute
                if (lookForAttr.startObject && lookForAttr.endObject || lookForAttr.typeColor) {
                    parents = entityType.attributes.filter(function (attr) {
                        return attr.relationshipTypeURI === lookForAttr.uri || attr.referencedEntityTypeURI === lookForAttr.uri;
                    });
                    referencedUris = parents.reduce(function (result, attr) {
                        var uris = attr.referencedAttributeURIs && attr.referencedAttributeURIs.filter(function (uri) {
                            return result.indexOf(uri) === -1;
                        });
                        return uris ? result.concat(uris) : result;
                    }, []);
                } else {
                    parents.push(lookForAttr);
                    referencedUris = null;
                }

                parents.forEach(function (parentAttribute) {
                    var attrValues = objValue[parentAttribute.name];
                    if (attrValues) {
                        parentTypes = parentTypes.concat(attrValues);
                    }
                    if (group && last) {
                        groups.push({
                            attrValues: attrValues || [], // can be null
                            parent: parentObj[k],
                            attrType: lookForAttr,
                            parentType: attrTypeChain[i - 1],
                            path: attrTypeChain
                        });
                    }
                }, this);

            }
            parentObj = parentTypes;
        }
        if (last) {
            ret = parentObj;
        }
    }

    return groups || ret;
};

Metadata.prototype.getSubAttributes = function(attrType) {
    var attributes = null;
    if (attrType.analyticsAttributes) {
        attributes = attrType.analyticsAttributes;
    } else if (attrType.type === 'Nested' || attrType.type === 'Image') {
        attributes = attrType.attributes;
    } else if (attrType.type === 'Reference') {
        attributes = attrType.referencedAttributeURIs
            .map(function(uri) {
                return this.getAttributeByUri(uri);
            }, this)
            .filter(function(a) {
                return !!a;
            });
    }
    return attributes;
};

/**
 * Finds attribute definition by URI. 
 * If second parameter doesn't provided, the search going through all objects. 
 * Otherwise method searches only in provided object. 
 * @param attributeTypeUri {string} uri of attribute. 
 * @param parentTypeUri {string} parent object URI, if defined then search going only in this object.
 * @returns {*} 
 */
Metadata.prototype.getAttributeByUri = function(attributeTypeUri, parentTypeUri) {
    var type = null, list = {};
    if (this.entityTypes && this.relationTypes) {
        var objectType = this.entityTypes[parentTypeUri] || this.relationTypes[parentTypeUri]
        objectType 
            ? list.parentTypeUri = objectType 
            : Object.assign(list, this.entityTypes, this.relationTypes);

        Object.keys(list).some(function(uri) {
            type = Metadata.findAttributeByUri(list[uri], attributeTypeUri);
            return type != null;
        }, this);    
    }
    return type;
};

/**
 * Finds attribute definition by URI.
 * 
 * @param parentObj {object} entity or relation definition.
 * @param attributeTypeUri {string} uri of attribute.
 * @returns {*}
 */
Metadata.findAttributeByUri = function(parentObj, attributeTypeUri) {
    function getAllAttributes(parentObj) {
        return []
            .concat(parentObj.attributes || [])
            .concat(parentObj.analyticsAttributes || [])
    }

    var type = null;
    getAllAttributes(parentObj).some(function loop(attributeDef) {
        if (attributeDef.uri === attributeTypeUri) {
            type = attributeDef;
        } else if (attributeDef.attributes || attributeDef.analyticsAttributes) {
            getAllAttributes(attributeDef).some(function(child) {
                type = loop(child);
                return type != null;
            }, this);
        }
        return type;
    }, this);
    return type;
};

/**
 * Checks that second entity with {uri} is a ancestor of {uriOrParent} or they are equal.
 * @param metadata
 * @param uriOrParent
 * @param uri
 * @returns {boolean}
 */
Metadata.isInHierarchy = function(metadata, uriOrParent, uri) {
    var value = false;
    var typeUri = uri;
    while(uriOrParent && typeUri) {
        value = uriOrParent === typeUri;
        if (value) {
            break;
        } else {
            typeUri = (metadata.entityTypes[typeUri] || {}).extendsTypeURI;
        }
    }
    return value;
};

Metadata.isAnalyticAttribute = function(attrType) {
    if (_.isObject(attrType))
        attrType = attrType.uri;

    return typeof attrType === 'string' && attrType.indexOf('/analyticsAttributes/') !== -1;
};

Metadata.isSubAttributeAllowed = function(attrType, referencedUris) {
    if (!referencedUris) {
        return true;
    } else {
        return referencedUris.some(function(uri) {
            return uri === attrType.uri;
        });
    }
};

Metadata.isComplexAttribute = function(attrType) {
    return attrType &&
        (!!attrType.analyticsAttributes ||
            'Reference' === attrType.type ||
            'Nested' === attrType.type ||
            'Image' === attrType.type);
};

module.exports = Metadata;
