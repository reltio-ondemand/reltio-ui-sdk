jest.disableAutomock();
require('jasmine-ajax');
require('whatwg-fetch');
var _ = require('lodash');
var Metadata = require('../Metadata.js');

describe('Metadata tests', function() {
    beforeAll(function() {
        jest.useRealTimers();
    });
    describe('init metadata', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });

        it('should add defaults fields to raw metadata', function() {
            metadata.init();
            var rowConfig = metadata.getRawConfiguration();
            expect(rowConfig.entityTypes).not.toBeNull();
            expect(rowConfig.relationTypes).not.toBeNull();
            expect(rowConfig.groupTypes).not.toBeNull();
            expect(rowConfig.categoryTypes).not.toBeNull();
            expect(rowConfig.interactionTypes).not.toBeNull();
            expect(rowConfig.xwalkTypes).not.toBeNull();
            expect(rowConfig.survivorshipStrategies).not.toBeNull();
        });
    });
    describe('process attribute types', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });
        it('should process one entity type with attributes correctly', function() {
            metadata.init({
                attributeTypes: [{
                    uri: 'configuration/attributeTypes/Email',
                    valueType: 'String',
                    matchTokenClass: {
                        class: 'com.reltio.match.token.ComplexPhoneticNameToken'
                    }
                }],
                entityTypes: [{
                    uri: 'configuration/entityTypes/Party',
                    label: 'Party',
                    attributes: [{
                        uri: 'configuration/entityTypes/Party/attributes/Email',
                        type: 'configuration/attributeTypes/Email'
                    }]
                }]
            });
            var emailAttribute = metadata.getEntityType('configuration/entityTypes/Party').attributes[0];
            expect(emailAttribute.uri).toBe('configuration/entityTypes/Party/attributes/Email');
            expect(emailAttribute.type).toBe('String');
        });
        it('should use string as defaul type', function() {
            metadata.init({
                attributeTypes: [{
                    uri: 'configuration/attributeTypes/Email',
                    matchTokenClass: {
                        class: 'com.reltio.match.token.ComplexPhoneticNameToken'
                    }
                }],
                entityTypes: [{
                    uri: 'configuration/entityTypes/Party',
                    label: 'Party',
                    attributes: [{
                        uri: 'configuration/entityTypes/Party/attributes/Email',
                        type: 'configuration/attributeTypes/Email'
                    }]
                }]
            });
            var emailAttribute = metadata.getEntityType('configuration/entityTypes/Party').attributes[0];
            expect(emailAttribute.uri).toBe('configuration/entityTypes/Party/attributes/Email');
            expect(emailAttribute.type).toBe('String');
        });
        it('should process one entity type with inhereted attributes correctly', function() {
            metadata.init({
                attributeTypes: [{
                        uri: 'configuration/attributeTypes/Text',
                        valueType: 'Text',
                        matchTokenClass: {
                            class: 'com.reltio.match.token.ComplexPhoneticNameToken'
                        }
                    },
                    {
                        uri: 'configuration/attributeTypes/Email',
                        extendsTypeURI: 'configuration/attributeTypes/Text'
                    }
                ],
                entityTypes: [{
                    uri: 'configuration/entityTypes/Party',
                    label: 'Party',
                    attributes: [{
                        uri: 'configuration/entityTypes/Party/attributes/Email',
                        type: 'configuration/attributeTypes/Email'
                    }]
                }]
            });
            var emailAttribute = metadata.getEntityType('configuration/entityTypes/Party').attributes[0];
            expect(emailAttribute.uri).toBe('configuration/entityTypes/Party/attributes/Email');
            expect(emailAttribute.type).toBe('Text');
        });
        it('should process nested attribute in entity type and attribute types correctly', function() {
            metadata.init({
                attributeTypes: [{
                    uri: 'configuration/attributeTypes/Number',
                    valueType: 'Number',
                    matchTokenClass: {
                        class: 'com.reltio.match.token.ComplexPhoneticNameToken'
                    }
                }],
                entityTypes: [{
                    uri: 'configuration/entityTypes/Party',
                    label: 'Party',
                    attributes: [{
                        uri: 'configuration/entityTypes/Party/attributes/Nested',
                        type: 'Nested',
                        attributes: [{
                            uri: 'configuration/entityTypes/Party/attributes/Nested/attributes/Simple',
                            type: 'configuration/attributeTypes/Number'
                        }]
                    }]
                }]
            });
            var simpleAttribute = metadata.getEntityType('configuration/entityTypes/Party').attributes[0].attributes[0];
            expect(simpleAttribute.uri).toBe('configuration/entityTypes/Party/attributes/Nested/attributes/Simple');
            expect(simpleAttribute.type).toBe('Number');
        });
    });
    describe('process entities types', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });

        it('should process one entity type correctly', function() {
            metadata.init({
                entityTypes: [{
                    uri: 'configuration/entityTypes/Party',
                    label: 'Party',
                    attributes: [{
                        uri: 'configuration/entityTypes/Party/attributes/Name',
                        type: 'String'
                    }]
                }]
            });
            expect(Object.keys(metadata.getEntityTypes()).length).toBe(1);
            expect(Object.keys(metadata.getEntityTypes())[0]).toBe('configuration/entityTypes/Party');
            expect(metadata.getEntityType('configuration/entityTypes/Party').label).toBe('Party');
            expect(metadata.getEntityType('configuration/entityTypes/Party').attributes[0].uri).toBe('configuration/entityTypes/Party/attributes/Name');
        });
        it('should process type inheritance correctly', function() {
            metadata.init({
                entityTypes: [{
                        uri: 'configuration/entityTypes/Party',
                        abstract: true,
                        label: 'Party',
                        attributes: [{
                            uri: 'configuration/entityTypes/Party/attributes/Name',
                            type: 'String'
                        }]
                    },
                    {
                        uri: 'configuration/entityTypes/HCO',
                        abstract: false,
                        extendsTypeURI: 'configuration/entityTypes/Party',
                        label: 'HCO',
                        attributes: [{
                            uri: 'configuration/entityTypes/HCO/attributes/Name',
                            type: 'String'
                        }]
                    }
                ]
            });
            expect(Object.keys(metadata.getEntityTypes()).length).toBe(2);
            var partyEntityType = metadata.getEntityType('configuration/entityTypes/Party');
            expect(partyEntityType.uri).toBe('configuration/entityTypes/Party');
            expect(partyEntityType.abstract).toBeTruthy();
            expect(partyEntityType.label).toBe('Party');
            var hcoEntityType = metadata.getEntityType('configuration/entityTypes/HCO');
            expect(hcoEntityType.attributes[0].uri).toBe('configuration/entityTypes/HCO/attributes/Name');
            expect(hcoEntityType.abstract).toBeFalsy();
            expect(hcoEntityType.label).toBe('HCO');
        });
        it('should process dependent typescorrectly', function() {
            metadata.init({
                entityTypes: [{
                    uri: 'configuration/entityTypes/Product',
                    abstract: false,
                    dependentAttributes: [{
                        attributeUri: 'configuration/entityTypes/Product/attributes/EC_Attributing_Category',
                        default: [
                            'configuration/entityTypes/Product/attributes/EC_Item_Validation_Method'
                        ],
                        values: [{
                            valuesList: [
                                'pizza cheese'
                            ],
                            visibleAttributes: [
                                'configuration/entityTypes/Product/attributes/SYYN'
                            ]
                        }]
                    }],
                    label: 'Product'
                }]
            });
            expect(Object.keys(metadata.getEntityTypes()).length).toBe(1);
            var productType = metadata.getEntityType('configuration/entityTypes/Product');
            expect(productType.dependentAttributes[0].values[0].valuesList).toEqual(['PIZZA CHEESE']);
        });
    });

    describe('process relations types', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });
        it('should process simple relation type correctly', function() {
            metadata.init({
                relationTypes: [{
                    uri: 'configuration/relationTypes/Allied',
                    startObject: {
                        uri: 'configuration/relationTypes/Allied/startObject'
                    }
                }]
            });
            expect(Object.keys(metadata.getRelationTypes()).length).toBe(1);
            var relationType = metadata.getRelationType('configuration/relationTypes/Allied');
            expect(relationType.uri).toBe('configuration/relationTypes/Allied');
            expect(relationType.startObject.uri).toBe('configuration/relationTypes/Allied/startObject');
        });
        it('should process relation type and attributes correctly', function() {
            metadata.init({
                attributeTypes: [{
                    uri: 'configuration/attributeTypes/Number',
                    valueType: 'Number',
                    matchTokenClass: {
                        class: 'com.reltio.match.token.ComplexPhoneticNameToken'
                    }
                }],
                relationTypes: [{
                    uri: 'configuration/relationTypes/Allied',
                    attributes: [{
                        uri: 'configuration/relationTypes/Allied/attributes/Name',
                        type: 'configuration/attributeTypes/Number'
                    }]
                }]
            });
            expect(Object.keys(metadata.getRelationTypes()).length).toBe(1);
            var relationType = metadata.getRelationType('configuration/relationTypes/Allied');
            expect(relationType.uri).toBe('configuration/relationTypes/Allied');
            expect(relationType.attributes[0].type).toBe('Number');
        });
    });

    describe('process group types', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });
        it('should process group types correctly', function() {
            metadata.init({
                attributeTypes: [{
                    uri: 'configuration/attributeTypes/Number',
                    valueType: 'Number'
                }],
                groupTypes: [{
                    attributes: [{
                        uri: 'configuration/groupTypes/Test/attributes/Name',
                        type: 'configuration/attributeTypes/Number'
                    }],
                    uri: 'configuration/groupTypes/Test'
                }]
            });
            expect(Object.keys(metadata.getGroupTypes()).length).toBe(1);
            var groupType = metadata.getGroupType('configuration/groupTypes/Test');
            expect(groupType.uri).toBe('configuration/groupTypes/Test');
            expect(groupType.attributes[0].type).toBe('Number');
        });
    });

    describe('process graph types', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });
        it('should process graph types correctly', function() {
            metadata.init({
                attributeTypes: [{
                    uri: 'configuration/attributeTypes/Number',
                    valueType: 'Number'
                }],
                graphTypes: [{
                    attributes: [{
                        uri: 'configuration/graphTypes/FamilyGraph/attributes/Name',
                        type: 'configuration/attributeTypes/Number'
                    }],
                    uri: 'configuration/graphTypes/FamilyGraph'
                }]
            });
            expect(Object.keys(metadata.getGraphTypes()).length).toBe(1);
            var graphType = metadata.getGraphType('configuration/graphTypes/FamilyGraph');
            expect(graphType.uri).toBe('configuration/graphTypes/FamilyGraph');
            expect(graphType.attributes[0].type).toBe('Number');
        });
    });

    describe('process category types', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });
        it('should process category types correctly', function() {
            metadata.init({
                attributeTypes: [{
                    uri: 'configuration/attributeTypes/Number',
                    valueType: 'Number'
                }],
                categoryTypes: [{
                    attributes: [{
                        uri: 'configuration/categoryTypes/GgHierarchy/attributes/Name',
                        type: 'configuration/attributeTypes/Number'
                    }],
                    uri: 'configuration/categoryTypes/GgHierarchy'
                }]
            });
            expect(Object.keys(metadata.getCategoryTypes()).length).toBe(1);
            var categoryType = metadata.getCategoryTypes()['configuration/categoryTypes/GgHierarchy'];
            expect(categoryType.uri).toBe('configuration/categoryTypes/GgHierarchy');
            expect(categoryType.attributes[0].type).toBe('Number');
        });
    });

    describe('process interaction types', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });
        it('should process interaction types correctly', function() {
            metadata.init({
                attributeTypes: [{
                    uri: 'configuration/attributeTypes/Number',
                    valueType: 'Number'
                }],
                interactionTypes: [{
                    attributes: [{
                        uri: 'configuration/interactionTypes/Phone/attributes/Name',
                        type: 'configuration/attributeTypes/Number'
                    }],
                    uri: 'configuration/interactionTypes/Phone'
                }]
            });
            expect(Object.keys(metadata.getInteractionTypes()).length).toBe(1);
            var interactionType = metadata.getInteractionType('configuration/interactionTypes/Phone');
            expect(interactionType.uri).toBe('configuration/interactionTypes/Phone');
            expect(interactionType.attributes[0].type).toBe('Number');
        });
    });

    describe('process roles', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });
        it('should process roles correctly', function() {
            metadata.init({
                roles: [{
                    label: 'Test',
                    uri: 'configuration/roles/Test'
                }]
            });
            expect(Object.keys(metadata.getRoles()).length).toBe(1);
            var role = metadata.getRole('configuration/roles/Test');
            expect(role.uri).toBe('configuration/roles/Test');
            expect(role.label).toBe('Test');
        });
    });

    describe('process xwalk types', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });
        it('should process xwalks with reltio type correctly', function() {
            metadata.init({
                sources: [{
                        label: 'AMA',
                        uri: 'configuration/sources/AMA'
                    },
                    {
                        label: 'LReltio',
                        uri: 'configuration/sources/Reltio'
                    }
                ]
            });
            expect(metadata.getXwalkTypes().length).toBe(2);
            expect(_.isArray(metadata.getXwalkTypes())).toBeTruthy();
            var uri = 'configuration/sources/Reltio';
            var shortUri = 'Reltio';
            var label = 'LReltio';
            expect(metadata.getXwalkType(uri)).toBeDefined();
            expect(metadata.getXwalkType(shortUri)).toBeDefined();
            expect(metadata.getXwalkType(label)).toBeDefined();
        });
        it('should add reltio xwalk if it is not defined', function() {
            metadata.init({
                sources: [{
                    label: 'AMA',
                    uri: 'configuration/sources/AMA'
                }]
            });
            expect(metadata.getXwalkTypes().length).toBe(2);
            var uri = 'configuration/sources/Reltio';
            var shortUri = 'Reltio';
            expect(metadata.getXwalkType(uri)).toBeDefined();
            expect(metadata.getXwalkType(shortUri)).toBeDefined();
            expect(metadata.getXwalkType('AMA')).toBeDefined();
        });
        it('should return sorted by label sources if it is necessary', function() {
            metadata.init({
                sources: [{
                        label: 'Reltio',
                        uri: 'configuration/sources/Reltio'
                    },
                    {
                        label: 'AMA',
                        uri: 'configuration/sources/AMA'
                    }
                ]
            });
            var sorted = metadata.getXwalkTypes(true);
            expect(sorted[0].label).toBe('AMA');
            expect(sorted[1].label).toBe('Reltio');
        });
    });
    describe('process survivorship strategies', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
        });
        it('should process survivorship strategies correctly', function() {
            metadata.init({
                survivorshipStrategies: [{
                    label: 'Reltio Cleanser or Nothing',
                    uri: 'configuration/survivorshipStrategies/CleanserWinsStrategy'
                }]
            });
            var strategies = metadata.getSurvivorshipStrategies();
            expect(Object.keys(strategies).length).toBe(1);
            var strategy = metadata.getSurvivorshipStrategy('configuration/survivorshipStrategies/CleanserWinsStrategy');
            expect(strategy.uri).toBe('configuration/survivorshipStrategies/CleanserWinsStrategy');
            expect(strategy.label).toBe('Reltio Cleanser or Nothing');
            expect(Object.keys(strategies)).toEqual(['CleanserWinsStrategy']);
        });
    });
    describe('getAttributesByUri', function() {
        var metadata;
        beforeAll(function() {
            metadata = new Metadata();
            metadata.init({
                'entityTypes': [{
                    'uri': 'configuration/entityTypes/HCP',
                    'attributes': [
                        {
                            'uri': 'configuration/entityTypes/HCP/attributes/Identifiers',
                            'attributes': [
                                {'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/Type'},
                                {'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/ID'}
                            ]
                        },
                        {'uri': 'configuration/entityTypes/HCP/attributes/FirstName'}
                    ]
                }],
                'relationTypes': [{
                    'uri': 'configuration/relationTypes/HasBeer',
                    'attributes': [
                        {
                            'uri': 'configuration/relationTypes/HasBeer/attributes/Identifiers',
                            'attributes': [
                                {'uri': 'configuration/relationTypes/HasBeer/attributes/Identifiers/attributes/Type'},
                                {'uri': 'configuration/relationTypes/HasBeer/attributes/Identifiers/attributes/ID'}
                            ]
                        },
                        {'uri': 'configuration/relationTypes/HasBeer/attributes/AttributeName'}
                    ]
                },{'uri': 'configuration/relationTypes/Empty'}]
            });
        });
        it('should find entity attributes everywhere', function() { 
            expect(metadata.getAttributeByUri('configuration/entityTypes/HCP/attributes/Identifiers/attributes/Type')).not.toBe(null);
        });
        it('should find relation attributes everywhere', function() { 
            expect(metadata.getAttributeByUri('configuration/relationTypes/HasBeer/attributes/Identifiers')).not.toBe(null)
        });
        it('should find attributes in relation', function() { 
            expect(metadata.getAttributeByUri('configuration/relationTypes/HasBeer/attributes/Identifiers/attributes/ID', 'configuration/relationTypes/HasBeer')).not.toBe(null);
        });
        it('should not find attributes in relation', function() { 
            expect(metadata.getAttributeByUri('configuration/relationTypes/HasBeer/attributes/Identifiers/attributes/ID', 'configuration/relationTypes/Empty')).toBe(null);
        });
    });
    afterAll(function() {
        jest.enableAutomock();
        jest.useFakeTimers();
    });

    describe('findAttributesByUri', function() {
        var entityType = {
            'uri': 'configuration/entityTypes/HCP',
            'analyticsAttributes': [
                {
                    'uri': 'configuration/entityTypes/HCP/analyticsAttributes/DQ',
                    'attributes': [
                        {'uri': 'configuration/entityTypes/HCP/analyticsAttributes/DQ/analyticsAttributes/score'},
                        {'uri': 'configuration/entityTypes/HCP/analyticsAttributes/DQs/analyticsAttributes/date'}
                    ]
                }
            ],
            'attributes': [
            {
                'uri': 'configuration/entityTypes/HCP/attributes/Identifiers',
                'attributes': [
                    {'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/Type'},
                    {'uri': 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/ID'}
                ]
            },
            {'uri': 'configuration/entityTypes/HCP/attributes/FirstName'}
        ]};
        var relationType = {
            'uri': 'configuration/relationTypes/HasBeer',
            'attributes': [
                {
                    'uri': 'configuration/relationTypes/HasBeer/attributes/Identifiers',
                    'attributes': [
                        {'uri': 'configuration/relationTypes/HasBeer/attributes/Identifiers/attributes/Type'},
                        {'uri': 'configuration/relationTypes/HasBeer/attributes/Identifiers/attributes/ID'}
                    ]
                },
                {'uri': 'configuration/relationTypes/HasBeer/attributes/AttributeName'}
            ]
        };
        
        it('should find entity attributes', function() {
            expect(Metadata.findAttributeByUri(entityType, 'configuration/entityTypes/HCP/attributes/Identifiers/attributes/Type')).not.toBe(null);
        });
        it('should find relation attributes', function() {
            expect(Metadata.findAttributeByUri(relationType, 'configuration/relationTypes/HasBeer/attributes/Identifiers')).not.toBe(null)
        });
        it('should find analytics attributes', function() {
            expect(Metadata.findAttributeByUri(entityType, 'configuration/entityTypes/HCP/analyticsAttributes/DQ/analyticsAttributes/score')).not.toBe(null);
        });
    });

    describe('isInHierarchy', function() {
        var entityType3 = {uri : '3', extendsTypeURI : '0'};
        var entityType2 = {uri : '2', extendsTypeURI : '1'};
        var entityType1 = {uri : '1', extendsTypeURI : '0'};
        var entityType0 = {uri : '0'};

        var metadata = {
            entityTypes : [entityType0, entityType1, entityType2, entityType3] 
        };
        
        
        it('types should be are relaties', function() {
            expect(Metadata.isInHierarchy(metadata, entityType0.uri , entityType2.uri)).toBe(true);
        });

        it('types should not intersect', function() {
            expect(Metadata.isInHierarchy(metadata, entityType2.uri , entityType3.uri)).toBe(false);
        });
        
    });

    describe('findAttributeValueByTypeUri behavior', function() {
        var metadata = new Metadata();
        metadata.init({
            entityTypes: [{
                attributes: [{
                    uri: 'configuration/entityTypes/HCP/attributes/FirstName',
                    label: 'First Name',
                    name: 'FirstName',
                    type: 'String'
                }, {
                    label: 'Address',
                    name: 'Address',
                    type: 'Reference',
                    uri: 'configuration/entityTypes/HCP/attributes/Address',
                    referencedAttributeURIs: ['configuration/relationTypes/HasAddress/attributes/RefString'],
                    referencedEntityTypeURI: 'configuration/entityTypes/Location',
                    relationshipTypeURI: 'configuration/relationTypes/HasAddress'

                }],
                analyticsAttributes: [{
                    uri: 'configuration/entityTypes/HCP/analyticsAttributes/IQ',
                    label: 'IQ',
                    name: 'IQ',
                    type: 'Nested',
                    analyticsAttributes: [{
                        uri: 'configuration/entityTypes/HCP/analyticsAttributes/IQ/analyticsAttributes/score',
                        label: 'Reltio Score',
                        name: 'score',
                        type: 'Int'
                    }, {
                        uri: 'configuration/entityTypes/HCP/analyticsAttributes/IQ/analyticsAttributes/updatedDate',
                        label: 'Updated date',
                        name: 'updatedDate',
                        type: 'Timestamp'
                    }]
                }],
                uri: 'configuration/entityTypes/HCP'
            }],
            relationTypes: [{
                uri: 'configuration/relationTypes/HasAddress',
                label: 'has address',
                startObject: {},
                endObject: {},
                attributes: [
                    {
                        label: 'RefString',
                        name: 'RefString',
                        type: 'String',
                        uri: 'configuration/relationTypes/HasAddress/attributes/RefString'
                    }
                ]
            }]
        });

        var entity = {
            attributes: {
                FirstName: [{
                    value: 'Jonh Smith',
                    uri: 'entities/PCboJz2/attributes/FirstName/1'
                }],
                Address: [{
                    value: {
                        RefString: [{
                            value: 'some string',
                            uri: 'entities/PCboJz2/attributes/Address/1/RefString/2'
                        }]
                    },
                    uri: 'entities/PCboJz2/attributes/Address/1'
                }]
            },
            analyticsAttributes: {
                IQ: [{
                    value: {
                        score: [{
                            value: '95',
                            uri: 'entities/PCboJz2/analyticsAttributes/IQ/0/score/0'
                        }],
                        updatedDate: [{
                            value: '1524735950115',
                            uri: 'entities/PCboJz2/analyticsAttributes/IQ/0/updatedDate/0'
                        }]
                    },
                    uri: 'entities/PCboJz2/analyticsAttributes/IQ/0'
                }]
            },
            type: 'configuration/entityTypes/HCP'
        };

        it('should find first level attributes values', function() {
            expect(metadata.findAttributeValueByTypeUri(entity, 'configuration/entityTypes/HCP/attributes/FirstName'))
                .toEqual(entity.attributes.FirstName);
        });

        it('should find first level analytics attributes values', function() {
            expect(metadata.findAttributeValueByTypeUri(entity, 'configuration/entityTypes/HCP/analyticsAttributes/IQ'))
                .toEqual(entity.analyticsAttributes.IQ);
        });

        it('should find second level analytics attributes values', function() {
            expect(metadata.findAttributeValueByTypeUri(entity, 'configuration/entityTypes/HCP/analyticsAttributes/IQ/analyticsAttributes/score'))
                .toEqual(entity.analyticsAttributes.IQ[0].value.score);
        });

        it('should return null for wrong uri', function() {
            expect(metadata.findAttributeValueByTypeUri(entity, 'configuration'))
                .toBe(null);
        });

        it('should find referenced subattributes values by "entityTypes" uri', function() {
            expect(metadata.findAttributeValueByTypeUri(entity, 'configuration/entityTypes/HCP/attributes/Address/attributes/RefString'))
                .toEqual(entity.attributes.Address[0].value.RefString);
        });

        it('should find referenced subattributes values by "relationTypes" uri', function() {
            expect(metadata.findAttributeValueByTypeUri(entity, 'configuration/relationTypes/HasAddress/attributes/RefString'))
                .toEqual(entity.attributes.Address[0].value.RefString);
        });
    });

    describe('isComplexAttribute behavior', function() {
        it('should return false for simple attributes', function() {
            var attrType = {type: 'String'};
            expect(Metadata.isComplexAttribute(attrType)).toBe(false);
        });

        it('should return true for Nested attributes', function() {
            var attrType = {type: 'Nested'};
            expect(Metadata.isComplexAttribute(attrType)).toBe(true);
        });

        it('should return true for Image attributes', function() {
            var attrType = {type: 'Image'};
            expect(Metadata.isComplexAttribute(attrType)).toBe(true);
        });

        it('should return true for Reference attributes', function() {
            var attrType = {type: 'Reference'};
            expect(Metadata.isComplexAttribute(attrType)).toBe(true);
        });
    });

    describe('isAnalyticAttribute behavior', function() {
        it('should return false for attribute', function() {
            var attrType = {uri: 'configuration/entityTypes/HCP/attributes/Address'};
            expect(Metadata.isAnalyticAttribute(attrType)).toBe(false);
        });
        it('should return true for analytic attribute', function() {
            var attrType = {uri: 'configuration/entityTypes/HCP/analyticsAttributes/Address'};
            expect(Metadata.isAnalyticAttribute(attrType)).toBe(true);
        });
        it('should return true for analytic attribute uri', function() {
            expect(Metadata.isAnalyticAttribute('configuration/entityTypes/HCP/analyticsAttributes/Address')).toBe(true);
        });
        it('should return true for attribute uri', function() {
            expect(Metadata.isAnalyticAttribute('configuration/entityTypes/HCP/attributes/Address')).toBe(false);
        });
    });

    describe('getSubAttributes behavior', function() {
        var metadata = new Metadata();
        var referenceAttrType = {
            label: 'Address',
            name: 'Address',
            type: 'Reference',
            uri: 'configuration/entityTypes/HCP/attributes/Address',
            referencedAttributeURIs: ['configuration/relationTypes/HasAddress/attributes/RefString'],
            referencedEntityTypeURI: 'configuration/entityTypes/Location',
            relationshipTypeURI: 'configuration/relationTypes/HasAddress'

        };
        var nestedAttrType = {
            uri: 'configuration/entityTypes/HCP/attributes/Nested',
            type: 'Nested',
            attributes: [{
                uri: 'configuration/entityTypes/HCP/attributes/Nested/attributes/Simple',
                type: 'Simple'
            }]
        };
        var simpleAttrType = {
            uri: 'configuration/entityTypes/HCP/attributes/FirstName',
            label: 'First Name',
            name: 'FirstName',
            type: 'String'
        };
        var iQAttrType = {
            uri: 'configuration/entityTypes/HCP/analyticsAttributes/IQ',
            label: 'IQ',
            name: 'IQ',
            type: 'Nested',
            analyticsAttributes: [{
                uri: 'configuration/entityTypes/HCP/analyticsAttributes/IQ/analyticsAttributes/score',
                label: 'Reltio Score',
                name: 'score',
                type: 'Int'
            }, {
                uri: 'configuration/entityTypes/HCP/analyticsAttributes/IQ/analyticsAttributes/updatedDate',
                label: 'Updated date',
                name: 'updatedDate',
                type: 'Timestamp'
            }]
        };
        var hasAddressRelType = {
            uri: 'configuration/relationTypes/HasAddress',
            label: 'has address',
            startObject: {},
            endObject: {},
            attributes: [
                {
                    label: 'RefString',
                    name: 'RefString',
                    type: 'String',
                    uri: 'configuration/relationTypes/HasAddress/attributes/RefString'
                }
            ]
        };
        metadata.init({
            entityTypes: [{
                attributes: [referenceAttrType, nestedAttrType, simpleAttrType],
                analyticsAttributes: [iQAttrType],
                uri: 'configuration/entityTypes/HCP'
            }],
            relationTypes: [hasAddressRelType]
        });

        it('should find subattributes of Reference', function() {
            expect(metadata.getSubAttributes(referenceAttrType))
                .toEqual(hasAddressRelType.attributes);
        });

        it('should find subattributes of analytic nested', function() {
            expect(metadata.getSubAttributes(iQAttrType))
                .toEqual(iQAttrType.analyticsAttributes);
        });

        it('should find subattributes of nested', function() {
            expect(metadata.getSubAttributes(nestedAttrType))
                .toEqual(nestedAttrType.attributes);
        });

        it('should return null for simple attribute', function() {
            expect(metadata.getSubAttributes(simpleAttrType)).toBe(null);
        });
    });
});
