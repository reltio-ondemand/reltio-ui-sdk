var _ = require('lodash');
module.exports = {
    /**
     * Method which is return hash of the string
     * @param {String} str - input string
     * @returns {String} hash
     * @static
     */
    getHash: function (str) {
        var i,
            chr,
            hash = 0;
        if (!str || str.length == 0) {
            return String(hash);
        }
        for (i = 0; i < str.length; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return String(hash);
    },

    /**
     * Method merges two objects
     * @param {Object} parent - parent object
     * @param {Object} child - child object
     * @param {Array} [ignoreProperties] - ignored properties
     * @param {Boolean} [replace] - replace property or not
     * @static
     */
    merge: function (parent, child, ignoreProperties, replace) {
        var me = this;
        _.forEach(parent, function (value, key) {
            if (ignoreProperties && _.includes(ignoreProperties, key)) {
                return;
            }

            if (replace || child[key] === undefined) {
                child[key] = _.clone(value);
            }
            else if (_.isPlainObject(value) && _.isPlainObject(child[key])) {
                me.merge(value, child[key]);
            }
        });
    },
    /**
     * Method returns all duplicate values in array
     * @param {Array} [array] - source array
     * @returns {Array}
     * @static
     */
    getDuplicates: function(array) {
        return array.reduce(function(acc, el, i, arr) {
            if (arr.indexOf(el) !== i && acc.indexOf(el) < 0) {
                acc.push(el);
            }
            return acc;
        }, []);
    },
    /**
     * Method removes the unnecessary symbols from the query value
     * @param {String} [value] - query value
     * @returns {string}
     * @static
     */
    escapeQueryValue: function (value) {
        value = value || '';
        /* it is an appropriate way to use encodeURIComponent(value) here, but it can lead to unpredictable behaviour on API side -
         * so stay using own regexp replace */
        return value
            .replace(/\\/g, '\\\\')
            .replace(/'/g, '\\\'')
            .replace(/"/g, '\\\"')
            .replace(/%/g, '%25')
            .replace(/#/g, '%23')
            .replace(/&/g, '%26')
            .replace(/\^/g, '%5E')
            .replace(/=/g, '%3D')
            .replace(/"/g, '%22')
            .replace(/</g, '%3C')
            .replace(/>/g, '%3E')
            .replace(/\+/g, '%2B');
    }
};
