jest.unmock('../Colors');
var Colors = require('../Colors');
describe('Color transform tests', function () {
    it('White must be transformed to black', function () {
        expect(Colors.getColor('#ffffff')).toEqual(0);
    });
    it('White must not be transformed to black', function () {
        expect(Colors.getColor('#ffffff')).not.toEqual(1);
    });
    it('Black must be transformed to white', function () {
        expect(Colors.getColor('#000000')).toEqual(1);
    });
});
