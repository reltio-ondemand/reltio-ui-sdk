jest.unmock('../DependencyManager');
var DependencyManager = require('../DependencyManager');

describe('dependency manager', function () {
    var a;
    var b;
    var c;
    var d;
    var e;
    var f;
    var g;
    var h;
    beforeEach(function () {
        a = DependencyManager.Node('Rickard Stark');
        b = DependencyManager.Node('Brandon Stark');
        c = DependencyManager.Node('Eddard Stark');
        d = DependencyManager.Node('Robb Stark');
        e = DependencyManager.Node('Sansa Stark');
        f = DependencyManager.Node('Arya Stark');
        g = DependencyManager.Node('Rickon Stark');
        h = DependencyManager.Node('John Snow');
        a.addEdge(b);
        a.addEdge(c);
        c.addEdge(d);
        c.addEdge(e);
        c.addEdge(f);
        c.addEdge(g);
        c.addEdge(h);
    });

    it('should apply action to each method in order from parent node to all child nodes', function () {
        var collection = [];
        var collector = function (item) {
            collection.push(item);
        };
        DependencyManager.resolve(a, collector);
        expect(collection.map(function (item) {
            return item.getInfo();
        })).toEqual(['Rickard Stark', 'Brandon Stark', 'Eddard Stark', 'Robb Stark', 'Sansa Stark', 'Arya Stark', 'Rickon Stark', 'John Snow']);
    });

    it('should show correct dependency order', function () {
        expect(DependencyManager.getDependencyOrder(a).map(function (item) {
            return item.getInfo();
        })).toEqual(['Brandon Stark', 'Robb Stark', 'Sansa Stark', 'Arya Stark', 'Rickon Stark', 'John Snow', 'Eddard Stark', 'Rickard Stark']);
    });

    it('should find nodes correctly', function () {
        expect(DependencyManager.findNode(a, 'John Snow')).toBe(h);
    });

    it('should return null if node is not found', function () {
        expect(DependencyManager.findNode(c, 'Rickard Stark')).toBeNull();
    });

    it('should remove edges correctly', function () {
        a.removeEdge(c);
        expect(DependencyManager.getDependencyOrder(a).map(function (item) {
            return item.getInfo();
        })).toEqual(['Brandon Stark', 'Rickard Stark']);
    });

    it('should find parent nodes correctly', function () {
        expect(DependencyManager.findParentNode(a, 'John Snow')).toBe(c);
    });


});