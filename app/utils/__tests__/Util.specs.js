jest.unmock('../Util');
var Util = require('../Util.js');

describe('Util', function () {

    describe('getHash method verification', function () {

        it('Should be defined', function () {
            expect(Util.getHash).toBeDefined();
        });
        it('Should return 0 in case of empty string', function () {
            expect(Util.getHash('')).toBe('0');
        });
        it('Should return hash in case of non-empty string', function () {
            expect(Util.getHash('aaa')).toBe('96321');
        });
    });

    describe('merge method verification', function () {
        var child, parent;

        beforeEach(function () {
            child = {a: 'test', b: true};
            parent = {c: 'c', d: 1};
        });
        it('Should be defined', function () {
            expect(Util.merge).toBeDefined();
        });
        it('Should merge parent to child', function () {
            var expected = {a: 'test', b: true, c: 'c', d: 1};
            Util.merge(parent, child);
            expect(child).toEqual(expected);
            expect(parent).toEqual(parent);
        });
        it('Should deep merge parent to child', function () {
            parent.inner = {test: 'test'};
            child.inner = {test2: 'test'};
            var expected = {a: 'test', b: true, c: 'c', d: 1, inner: {test: 'test', test2: 'test'}};
            Util.merge(parent, child);
            expect(child).toEqual(expected);
        });
        it('Shouldn\'t replace existing options', function () {
            parent.a = 'a';
            var expected = {a: 'test', b: true, c: 'c', d: 1};
            Util.merge(parent, child);
            expect(child).toEqual(expected);
        });
        it('Should replace existing options if the replace option is true', function () {
            parent.a = 'a';
            var expected = {a: 'a', b: true, c: 'c', d: 1};
            Util.merge(parent, child, null, true);
            expect(child).toEqual(expected);
        });
        it('Should ignores options which are described in ignore properties', function () {
            var expected = {a: 'test', b: true, d: 1};
            Util.merge(parent, child, ['c']);
            expect(child).toEqual(expected);
        });
        it('Shouldn\'t replace existing options', function () {
            parent.a = 'a';
            var expected = {a: 'test', b: true, c: 'c', d: 1};
            Util.merge(parent, child);
            expect(child).toEqual(expected);
        });

        it('Shouldn\'t merge arrays', function () {
            parent.e = ['a'];
            child.e = ['b'];
            var expected = {a: 'test', b: true, c: 'c', d: 1, e: ['b']};
            Util.merge(parent, child);
            expect(child).toEqual(expected);
        });
    });
    describe('escape query value method verification', function () {

        it('Should be defined', function () {
            expect(Util.escapeQueryValue).toBeDefined();
        });
        it('Should return empty string in case of null', function () {
            expect(Util.escapeQueryValue()).toBe('');
        });
        it('Should return hash in case of non-empty string', function () {
            expect(Util.escapeQueryValue('\\ \'%#&\^="<>\+')).toBe('\\\\ \\\'%25%23%26%5E%3D\\%22%3C%3E%2B');
        });
    });
});
