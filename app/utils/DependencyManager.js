
module.exports = {
    Node: Node,
    resolve: resolve,
    findNode: findNode,
    findParentNode: findParentNode,
    getDependencyOrder: getDependencyOrder
};

function Node(info) {
    var edges = [];
    return {
        getInfo: function () {
            return info;
        },
        getEdges: function () {
            return edges;
        },
        removeEdge: function (edge) {
            var index = edges.indexOf(edge);
            edges.splice(index, 1);
        },
        addEdge: function (edge) {
            edges.push(edge);
        }
    }
}
function resolve(node, action) {
    action(node);
    node.getEdges().forEach(function (edge) {
        resolve(edge, action);
    });
}

function getDependencyOrder(node) {
    var results = [];
    node.getEdges().forEach(function (edge) {
        results.push.apply(results, getDependencyOrder(edge));
    });
    results.push(node);
    return results;
}

function findNode(startNode, find) {
    var res = null;
    if (startNode.getInfo() === find) {
        return startNode;
    }
    var edges = startNode.getEdges();
    for (var i = 0; i < edges.length; i++) {
        res = findNode(edges[i], find);
        if (res) {
            break;
        }
    }
    return res;
}
function findParentNode(startNode, find) {
    var res = null;
    var hasNode = startNode.getEdges().some(function (item) {
        return item.getInfo() === find;
    });
    if (hasNode) {
        return startNode;
    }
    var edges = startNode.getEdges();
    for (var i = 0; i < edges.length; i++) {
        res = findParentNode(edges[i], find);
        if (res) {
            break;
        }
    }
    return res;
}

