jest.unmock('../../editMode/AttributesDiff');
jest.unmock('../HistoryManager');
var AttributesDiff = require('../../editMode/AttributesDiff');
var HistoryManager = require('../HistoryManager');

var referenceAttribute = {
    Address: [
        {
            'label': 'DataTenant4255497916300 Dr AMA1 alpharetta GA 3005497916300',
            'relationshipLabel': 'rank -',
            value: {
                'AddressLine1': [
                    {
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'ov': true,
                        'value': 'DataTenant4255497916300 Dr AMA1',
                        'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/AddressLine1/9arjp2th'
                    }
                ],
                'AddressLine2': [
                    {
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine2',
                        'ov': true,
                        'value': 'Dr27',
                        'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/AddressLine2/9arjp79x'
                    }
                ],
                'City': [
                    {
                        'type': 'configuration/entityTypes/Location/attributes/City',
                        'ov': true,
                        'value': 'alpharetta',
                        'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/City/9arjpBQD'
                    }
                ],
                'StateProvince': [
                    {
                        'type': 'configuration/entityTypes/Location/attributes/StateProvince',
                        'ov': true,
                        'value': 'GA',
                        'lookupCode': 'GA',
                        'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/StateProvince/9arjpFgT'
                    }
                ],
                'Zip': [
                    {
                        'label': '3005497916300-',
                        'value': {
                            'Zip5': [
                                {
                                    'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                                    'ov': true,
                                    'value': '3005497916300',
                                    'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/Zip/9arjpOCz/Zip5/9arjpSTF'
                                }
                            ]
                        },
                        'ov': true,
                        'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/Zip/9arjpOCz'
                    }
                ],
                'Country': [
                    {
                        'type': 'configuration/entityTypes/Location/attributes/Country',
                        'ov': true,
                        'value': 'USA',
                        'lookupCode': 'us',
                        'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/Country/9arjpJwj'
                    }
                ],
                'DEA': [
                    {
                        'label': '(Appt required - )',
                        'value': {
                            'Number': [
                                {
                                    'type': 'configuration/relationTypes/HasAddress/attributes/DEA/attributes/Number',
                                    'ov': true,
                                    'value': '123',
                                    'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC/Number/JxoxI91S'
                                }
                            ]
                        },
                        'ov': true,
                        'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC'
                    }
                ]
            },
            'ov': true,
            'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby',
            'refEntity': {
                'type': 'configuration/entityTypes/Location',
                'crosswalks': [
                    {
                        'uri': 'entities/Cpk0VAs/crosswalks/3V8cqZZ.901XbDL6',
                        'type': 'configuration/sources/DT_TEST',
                        'value': 'entities/GABZ6g3',
                        'createDate': '2016-05-05T09:34:06.375Z',
                        'updateDate': '2016-05-05T09:34:06.375Z',
                        'attributeURIs': [
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/StateProvince/9arjpFgT',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/City/9arjpBQD',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/Zip/9arjpOCz/Zip5/9arjpSTF',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/AddressLine1/9arjp2th',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/AddressLine2/9arjp79x',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/Zip/9arjpOCz',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/Country/9arjpJwj'
                        ],
                        'crosswalkExternalInfo': {
                            'dtssInfo': {
                                'tenantId': 'olegdt',
                                'operationId': '2c914e2a-cceb-4d28-8d42-9dfa9f9820ce',
                                'updatedBy': 'dtss',
                                'updatedTime': 1462443389390,
                                'status': 'IMPORTED_BY_TASK'
                            }
                        }
                    },
                    {
                        'uri': 'entities/Cpk0VAs/crosswalks/3V8cqZZ.9arj9f2L',
                        'type': 'configuration/sources/AMA',
                        'sourceTable': 'DataTenant4255497916300_ST01',
                        'value': 'DataTenant4255497916300 Dr.Dr27.alpharetta.GA.3005497916300..us.',
                        'createDate': '2013-06-22T12:12:12.000Z',
                        'updateDate': '2013-06-24T12:12:12.000Z',
                        'attributeURIs': [
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/StateProvince/9arjpFgT',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/City/9arjpBQD',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/Zip/9arjpOCz/Zip5/9arjpSTF',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/AddressLine1/9arjp2th',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/AddressLine2/9arjp79x',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/Zip/9arjpOCz',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/Country/9arjpJwj'
                        ],
                        'crosswalkExternalInfo': {
                            'dtssInfo': {
                                'tenantId': 'olegdt',
                                'operationId': '04c82ec0-2329-4845-905d-2a3f3f4a1afd',
                                'updatedBy': 'dtss',
                                'updatedTime': 1462442838074,
                                'status': 'IMPORTED_BY_TASK'
                            }
                        }
                    }
                ],
                'objectURI': 'entities/3V8cqZZ'
            },
            'refRelation': {
                'type': 'configuration/relationTypes/HasAddress',
                'crosswalks': [
                    {
                        'uri': 'entities/Cpk0VAs/crosswalks/DPn6Rby.JxoxHfBe',
                        'type': 'configuration/sources/Reltio',
                        'value': 'DPn6Rby',
                        'createDate': '2016-05-12T13:41:29.337Z',
                        'updateDate': '2016-05-12T13:41:29.337Z',
                        'attributeURIs': [
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC/Number/JxoxI91S',
                            'entities/Cpk0VAs/attributes/Address/DPn6Rby'
                        ],
                        'crosswalkExternalInfo': {}
                    }
                ],
                'startRefPinned': false,
                'endRefPinned': false,
                'startRefIgnored': false,
                'endRefIgnored': false,
                'objectURI': 'relations/DPn6Rby'
            },
            'startObjectCrosswalks': [
                {
                    'uri': 'entities/Cpk0VAs/crosswalks/IbvtP43C',
                    'type': 'configuration/sources/Reltio',
                    'value': 'Cpk0VAs',
                    'reltioLoadDate': '2016-05-12T10:28:45.271Z',
                    'createDate': '2016-05-12T10:28:45.271Z',
                    'updateDate': '2016-05-12T10:28:45.271Z',
                    'attributes': [
                        'entities/Cpk0VAs/attributes/LastName/IbvtOzmw',
                        'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S',
                        'entities/Cpk0VAs/attributes/BirthCity/IbvtPCZi',
                        'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi'
                    ],
                    'singleAttributeUpdateDates': {
                        'entities/Cpk0VAs/attributes/LastName/IbvtOzmw': '2016-05-12T10:29:42.016Z',
                        'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S': '2016-05-12T13:00:10.103Z',
                        'entities/Cpk0VAs/attributes/BirthCity/IbvtPCZi': '2016-05-12T10:29:12.991Z',
                        'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi': '2016-05-12T13:00:10.103Z'
                    }
                }
            ]
        }
    ]
};
describe('history manager test', function () {
    var historyManager = HistoryManager.createHistoryManager(AttributesDiff);
    describe('simple attributes changes test', function () {
        it('should return entity with added attribute and return added appearance', function () {
            var oldState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }]
            };
            var newState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }],
                LastName: [{
                    type: 'configuration/entityTypes/HCP/attributes/LastName',
                    ov: true,
                    value: 'Snow1',
                    uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzmw'
                }]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(Object.keys(result.appearance)[0]).toBe('entities/Cpk0VAs/attributes/LastName/IbvtOzmw');
            expect(result.appearance['entities/Cpk0VAs/attributes/LastName/IbvtOzmw']).toBe('INSERT_ATTRIBUTE');
            expect(result.attributes['LastName']).toBeDefined();

        });
        it('should return entity with ignored attribute and return ignored appearance', function () {
            var oldState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }]
            };
            var newState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        ignored: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(Object.keys(result.appearance)[0]).toBe('entities/Cpk0VAs/attributes/FirstName/IbvtOvWg');
            expect(result.attributes['FirstName'][0].ignored).toBeFalsy();
            expect(result.appearance['entities/Cpk0VAs/attributes/FirstName/IbvtOvWg']).toBe('IGNORE_ATTRIBUTE');
        });
        it('should return entity with unignored attribute and return ignored false appearance', function () {
            var oldState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        ignored: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }]
            };
            var newState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        ignored: false,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(Object.keys(result.appearance)[0]).toBe('entities/Cpk0VAs/attributes/FirstName/IbvtOvWg');
            expect(result.attributes['FirstName'][0].ignored).toBeFalsy();
            expect(result.appearance['entities/Cpk0VAs/attributes/FirstName/IbvtOvWg']).toBe('UNDO_IGNORE_ATTRIBUTE');
        });
        it('shouldn\'t produce event in case of already ignored item and attribute should be ignored', function () {
            var oldState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        ignored: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }]
            };
            var newState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        ignored: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(0);
            expect(result.attributes['FirstName'][0].ignored).toBeTruthy();
        });
        it('should produce update event in case of batch ignored attribute and add new attribute', function () {
            var oldState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }]
            };
            var newState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        ignored: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    },
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        value: 'Jhon1',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg1'
                    }]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(2);
            expect(result.attributes['FirstName'][0].ignored).toBeTruthy();
            expect(result.attributes['FirstName'][1].ignored).toBeFalsy();
        });
        it('should return entity with removed attribute and return removed appearance', function () {
            var oldState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }],
                LastName: [{
                    type: 'configuration/entityTypes/HCP/attributes/LastName',
                    ov: true,
                    value: 'Snow1',
                    uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzmw'
                }]
            };
            var newState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(Object.keys(result.appearance)[0]).toBe('entities/Cpk0VAs/attributes/LastName/IbvtOzmw');
            expect(result.appearance['entities/Cpk0VAs/attributes/LastName/IbvtOzmw']).toBe('DELETE_ATTRIBUTE');
            expect(result.attributes['LastName']).toBeDefined();
        });
        it('should return entity with removed attribute and return removed appearance', function () {
            var oldState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }],
                LastName: [{
                    type: 'configuration/entityTypes/HCP/attributes/LastName',
                    ov: true,
                    value: 'Snow1',
                    uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzmw'
                }]
            };
            var newState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }],
                LastName: [{
                    type: 'configuration/entityTypes/HCP/attributes/LastName',
                    ov: true,
                    value: 'Snow12',
                    uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzmw'
                }]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(Object.keys(result.appearance)[0]).toBe('entities/Cpk0VAs/attributes/LastName/IbvtOzmw');
            expect(result.appearance['entities/Cpk0VAs/attributes/LastName/IbvtOzmw']).toBe('UPDATE_ATTRIBUTE');
            expect(result.attributes['LastName'][0].value).toBe('Snow12');
        });
        it('should return entity with changed attribute and return appearancee', function () {
            var oldState = {
                LastName: [{
                    type: 'configuration/entityTypes/HCP/attributes/LastName',
                    ov: true,
                    value: 'Snow',
                    uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzmw'
                }],
                BirthCity: [{
                    type: 'configuration/entityTypes/HCP/attributes/BirthCity',
                    ov: true,
                    value: 'Winterfell',
                    uri: 'entities/Cpk0VAs/attributes/BirthCity/IbvtPCZi'
                }]
            };
            var newState = {
                FirstName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/FirstName',
                        ov: true,
                        value: 'Jhon',
                        uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
                    }],
                LastName: [{
                    type: 'configuration/entityTypes/HCP/attributes/LastName',
                    ov: true,
                    value: 'Snow12',
                    uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzmw'
                }]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(3);
            expect(result.appearance['entities/Cpk0VAs/attributes/LastName/IbvtOzmw']).toBe('UPDATE_ATTRIBUTE');
            expect(result.appearance['entities/Cpk0VAs/attributes/FirstName/IbvtOvWg']).toBe('INSERT_ATTRIBUTE');
            expect(result.appearance['entities/Cpk0VAs/attributes/BirthCity/IbvtPCZi']).toBe('DELETE_ATTRIBUTE');
        });
        it('should works well for entity with multivalues', function () {
            var oldState = {
                LastName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/LastName',
                        ov: true,
                        value: 'Snow5',
                        uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzm5'
                    },
                    {
                        type: 'configuration/entityTypes/HCP/attributes/LastName',
                        ov: true,
                        value: 'Snow',
                        uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzmw'
                    }]
            };
            var newState = {
                LastName: [
                    {
                        type: 'configuration/entityTypes/HCP/attributes/LastName',
                        ov: true,
                        value: 'Snow12',
                        uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzmw'
                    },
                    {
                        type: 'configuration/entityTypes/HCP/attributes/LastName',
                        ov: true,
                        value: 'Snow1',
                        uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzm1'
                    }]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(3);
            expect(result.attributes['LastName'].length).toBe(3);
            expect(result.appearance['entities/Cpk0VAs/attributes/LastName/IbvtOzmw']).toBe('UPDATE_ATTRIBUTE');
            expect(result.appearance['entities/Cpk0VAs/attributes/LastName/IbvtOzm1']).toBe('INSERT_ATTRIBUTE');
            expect(result.appearance['entities/Cpk0VAs/attributes/LastName/IbvtOzm5']).toBe('DELETE_ATTRIBUTE');
        });
    });
    describe('nested attributes changes test', function () {
        it('should return entity with added nested attribute and return added appearance', function () {
            var oldState = {};
            var newState = {
                CRMSON: [
                    {
                        label: '',
                        value: {
                            SentDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/SentDate',
                                    ov: true,
                                    value: '2012-12-12',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi'
                                }
                            ],
                            UpdateDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/UpdateDate',
                                    ov: true,
                                    value: '2012-12-15',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/UpdateDate/IbvwoBLi1'
                                }
                            ]
                        },
                        ov: true,
                        uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S'
                    }
                ]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(result.appearance['entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S']).toBe('INSERT_ATTRIBUTE');
            expect(Object.keys(result.attributes).length).toBe(1);
            expect(Object.keys(result.attributes['CRMSON'][0].value).length).toBe(2);
        });

        it('should return entity with removed nested attribute and return removed appearance', function () {
            var oldState = {
                CRMSON: [
                    {
                        label: '',
                        value: {
                            SentDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/SentDate',
                                    ov: true,
                                    value: '2012-12-12',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi'
                                }
                            ],
                            UpdateDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/UpdateDate',
                                    ov: true,
                                    value: '2012-12-15',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/UpdateDate/IbvwoBLi1'
                                }
                            ]
                        },
                        ov: true,
                        uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S'
                    }
                ]
            };
            var newState = {};
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(result.appearance['entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S']).toBe('DELETE_ATTRIBUTE');
            expect(Object.keys(result.attributes).length).toBe(1);
            expect(Object.keys(result.attributes['CRMSON'][0].value).length).toBe(2);
        });

        it('should return entity with nested attribute and return changed attribute appearance', function () {
            var oldState = {
                CRMSON: [
                    {
                        label: '',
                        value: {
                            SentDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/SentDate',
                                    ov: true,
                                    value: '2012-12-12',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi'
                                }
                            ],
                            UpdateDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/UpdateDate',
                                    ov: true,
                                    value: '2012-12-15',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/UpdateDate/IbvwoBLi1'
                                }
                            ]
                        },
                        ov: true,
                        uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S'
                    }
                ]
            };
            var newState = {
                CRMSON: [
                    {
                        label: '',
                        value: {
                            SentDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/SentDate',
                                    ov: true,
                                    value: '2012-12-15',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi'
                                }
                            ],
                            UpdateDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/UpdateDate',
                                    ov: true,
                                    value: '2012-12-17',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/UpdateDate/IbvwoBLi1'
                                }
                            ]
                        },
                        ov: true,
                        uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S'
                    }
                ]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(2);
            expect(result.appearance['entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi']).toBe('UPDATE_ATTRIBUTE');
            expect(result.appearance['entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/UpdateDate/IbvwoBLi1']).toBe('UPDATE_ATTRIBUTE');
            expect(Object.keys(result.attributes).length).toBe(1);
            expect(Object.keys(result.attributes['CRMSON'][0].value).length).toBe(2);
        });

        it('should return entity with nested attribute and return changed attribute appearance', function () {
            var oldState = {
                CRMSON: [
                    {
                        label: '',
                        value: {
                            UpdateDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/UpdateDate',
                                    ov: true,
                                    value: '2012-12-15',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/UpdateDate/IbvwoBLi1'
                                }
                            ]
                        },
                        ov: true,
                        uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S'
                    }
                ]
            };
            var newState = {
                CRMSON: [
                    {
                        label: '',
                        value: {
                            SentDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/SentDate',
                                    ov: true,
                                    value: '2012-12-15',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi'
                                }
                            ]
                        },
                        ov: true,
                        uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S'
                    }
                ]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(2);
            expect(result.appearance['entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi']).toBe('INSERT_ATTRIBUTE');
            expect(result.appearance['entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/UpdateDate/IbvwoBLi1']).toBe('DELETE_ATTRIBUTE');
            expect(Object.keys(result.attributes).length).toBe(1);
            expect(Object.keys(result.attributes['CRMSON'][0].value).length).toBe(2);
        });

        it('should work correctly if whole nested is ignored', function () {
            var oldState = {
                CRMSON: [
                    {
                        label: '',
                        value: {
                            UpdateDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/UpdateDate',
                                    ov: true,
                                    value: '2012-12-15',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/UpdateDate/IbvwoBLi1'
                                }
                            ]
                        },
                        ov: true,
                        uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S'
                    }
                ]
            };
            var newState = {
                CRMSON: [
                    {
                        label: '',
                        ignored: true,
                        value: {
                            UpdateDate: [
                                {
                                    type: 'configuration/entityTypes/HCP/attributes/CRMSON/attributes/UpdateDate',
                                    ov: true,
                                    value: '2012-12-15',
                                    uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/UpdateDate/IbvwoBLi1'
                                }
                            ]
                        },
                        ov: false,
                        uri: 'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S'
                    }
                ]
            };
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(result.appearance['entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S']).toBe('IGNORE_ATTRIBUTE');

            expect(Object.keys(result.attributes).length).toBe(1);
            expect(Object.keys(result.attributes['CRMSON'][0].value).length).toBe(1);
        });
    });
    describe('reference attributes changes test', function () {
        it('should return entity with added reference attribute and return added appearance', function () {
            var oldState = {};
            var result = historyManager.computeHistoryDiff(oldState, referenceAttribute);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(result.appearance['entities/Cpk0VAs/attributes/Address/DPn6Rby']).toBe('INSERT_ATTRIBUTE');
            expect(Object.keys(result.attributes).length).toBe(1);
            expect(Object.keys(result.attributes['Address'][0].value).length).toBe(7);
        });
        it('should return entity with removed reference attribute and return removed appearance', function () {
            var oldState = referenceAttribute;
            var newState = {};
            var result = historyManager.computeHistoryDiff(oldState, newState);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(result.appearance['entities/Cpk0VAs/attributes/Address/DPn6Rby']).toBe('DELETE_ATTRIBUTE');
            expect(Object.keys(result.attributes).length).toBe(1);
            expect(Object.keys(result.attributes['Address'][0].value).length).toBe(7);
        });
        it('should return entity with updated reference attribute and return update appearance', function () {
            var referenceAttribute = {
                Address: [
                    {
                        label: 'DataTenant4255497916300 Dr AMA1 alpharetta GA 3005497916300',
                        value: {
                            'DEA': [
                                {
                                    'label': '(Appt required - )',
                                    'value': {
                                        'Number': [
                                            {
                                                'type': 'configuration/relationTypes/HasAddress/attributes/DEA/attributes/Number',
                                                'ov': true,
                                                'value': '123',
                                                'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC/Number/JxoxI91S'
                                            }
                                        ]
                                    },
                                    'ov': true,
                                    'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC'
                                }
                            ]
                        },
                        'ov': true,
                        'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby',
                        'refEntity': {
                            'type': 'configuration/entityTypes/Location',
                            'crosswalks': [],
                            'objectURI': 'entities/3V8cqZZ'
                        },
                        'refRelation': {
                            'type': 'configuration/relationTypes/HasAddress',
                            'crosswalks': [
                                {
                                    'uri': 'entities/Cpk0VAs/crosswalks/DPn6Rby.JxoxHfBe',
                                    'type': 'configuration/sources/Reltio',
                                    'value': 'DPn6Rby',
                                    'createDate': '2016-05-12T13:41:29.337Z',
                                    'updateDate': '2016-05-12T13:41:29.337Z',
                                    'attributeURIs': [
                                        'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC',
                                        'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC/Number/JxoxI91S',
                                        'entities/Cpk0VAs/attributes/Address/DPn6Rby'
                                    ],
                                    'crosswalkExternalInfo': {}
                                }
                            ],
                            'objectURI': 'relations/DPn6Rby'
                        },
                        'startObjectCrosswalks': [
                            {
                                'uri': 'entities/Cpk0VAs/crosswalks/IbvtP43C',
                                'type': 'configuration/sources/Reltio',
                                'value': 'Cpk0VAs',
                                'reltioLoadDate': '2016-05-12T10:28:45.271Z',
                                'createDate': '2016-05-12T10:28:45.271Z',
                                'updateDate': '2016-05-12T10:28:45.271Z',
                                'attributes': [
                                    'entities/Cpk0VAs/attributes/LastName/IbvtOzmw',
                                    'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S',
                                    'entities/Cpk0VAs/attributes/BirthCity/IbvtPCZi',
                                    'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi'
                                ],
                                'singleAttributeUpdateDates': {
                                    'entities/Cpk0VAs/attributes/LastName/IbvtOzmw': '2016-05-12T10:29:42.016Z',
                                    'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S': '2016-05-12T13:00:10.103Z',
                                    'entities/Cpk0VAs/attributes/BirthCity/IbvtPCZi': '2016-05-12T10:29:12.991Z',
                                    'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi': '2016-05-12T13:00:10.103Z'
                                }
                            }
                        ]
                    }
                ]
            };
            var referenceAttributeChanged = {
                Address: [
                    {
                        label: 'DataTenant4255497916300 Dr AMA1 alpharetta GA 3005497916300',
                        value: {
                            'DEA': [
                                {
                                    'label': '(Appt required - )',
                                    'value': {
                                        'Number': [
                                            {
                                                'type': 'configuration/relationTypes/HasAddress/attributes/DEA/attributes/Number',
                                                'ov': true,
                                                //nuber changed here
                                                'value': '123111',
                                                'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC/Number/JxoxI91S'
                                            }
                                        ]
                                    },
                                    'ov': true,
                                    'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC'
                                }
                            ]
                        },
                        'ov': true,
                        'uri': 'entities/Cpk0VAs/attributes/Address/DPn6Rby',
                        'refEntity': {
                            'type': 'configuration/entityTypes/Location',
                            'crosswalks': [],
                            'objectURI': 'entities/3V8cqZZ'
                        },
                        'refRelation': {
                            'type': 'configuration/relationTypes/HasAddress',
                            'crosswalks': [
                                {
                                    'uri': 'entities/Cpk0VAs/crosswalks/DPn6Rby.JxoxHfBe',
                                    'type': 'configuration/sources/Reltio',
                                    'value': 'DPn6Rby',
                                    'createDate': '2016-05-12T13:41:29.337Z',
                                    'updateDate': '2016-05-12T13:41:29.337Z',
                                    'attributeURIs': [
                                        'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC',
                                        'entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC/Number/JxoxI91S',
                                        'entities/Cpk0VAs/attributes/Address/DPn6Rby'
                                    ],
                                    'crosswalkExternalInfo': {}
                                }
                            ],
                            'objectURI': 'relations/DPn6Rby'
                        },
                        'startObjectCrosswalks': [
                            {
                                'uri': 'entities/Cpk0VAs/crosswalks/IbvtP43C',
                                'type': 'configuration/sources/Reltio',
                                'value': 'Cpk0VAs',
                                'reltioLoadDate': '2016-05-12T10:28:45.271Z',
                                'createDate': '2016-05-12T10:28:45.271Z',
                                'updateDate': '2016-05-12T10:28:45.271Z',
                                'attributes': [
                                    'entities/Cpk0VAs/attributes/LastName/IbvtOzmw',
                                    'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S',
                                    'entities/Cpk0VAs/attributes/BirthCity/IbvtPCZi',
                                    'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi'
                                ],
                                'singleAttributeUpdateDates': {
                                    'entities/Cpk0VAs/attributes/LastName/IbvtOzmw': '2016-05-12T10:29:42.016Z',
                                    'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S': '2016-05-12T13:00:10.103Z',
                                    'entities/Cpk0VAs/attributes/BirthCity/IbvtPCZi': '2016-05-12T10:29:12.991Z',
                                    'entities/Cpk0VAs/attributes/CRMSON/Ibvwo75S/SentDate/IbvwoBLi': '2016-05-12T13:00:10.103Z'
                                }
                            }
                        ]
                    }
                ]
            };
            var result = historyManager.computeHistoryDiff(referenceAttribute, referenceAttributeChanged);
            expect(Object.keys(result.appearance).length).toBe(1);
            expect(result.appearance['entities/Cpk0VAs/attributes/Address/DPn6Rby/DEA/JxoxI4lC/Number/JxoxI91S']).toBe('UPDATE_ATTRIBUTE');
            expect(Object.keys(result.attributes).length).toBe(1);
            expect(Object.keys(result.attributes['Address'][0].value).length).toBe(1);
        });
    });
});
