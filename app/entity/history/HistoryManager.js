var _ = require('lodash');

/**
 * This module provides History Manager. Which allows to compare two objects and returns the diff as array of changes.
 */
/**
 * Use this class to get object with computeHistoryDiff
 * @param {AttributesDiff} attributeDiff - module which provides compare function
 * @returns {{computeHistoryDiff: computeHistoryDiff}} object with computeHistoryDiff function
 * @example
 * ```
 * var AttributesDiff = require('../editMode/AttributesDiff');
 * var historyManager = HistoryManager.createHistoryManager(AttributesDiff);
 * var oldState = {
 *     FirstName: [
 *         {
 *             type: 'configuration/entityTypes/HCP/attributes/FirstName',
 *             ov: true,
 *             value: 'Jhon',
 *             uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
 *         }]
 * };
 * var newState = {
 *     FirstName: [
 *         {
 *             type: 'configuration/entityTypes/HCP/attributes/FirstName',
 *             ov: true,
 *             value: 'Jhon',
 *             uri: 'entities/Cpk0VAs/attributes/FirstName/IbvtOvWg'
 *         }],
 *     LastName: [{
 *         type: 'configuration/entityTypes/HCP/attributes/LastName',
 *         ov: true,
 *         value: 'Snow1',
 *         uri: 'entities/Cpk0VAs/attributes/LastName/IbvtOzmw'
 *     }]
 * };
 * var result = historyManager.computeHistoryDiff(oldState, newState);
 * ```
 */
var Manager = function (attributeDiff) {
    var unIgnore = function (attrA, attrB) {
        if (attrA && attrB && attrA.ignored !== attrB.ignored) {
            delete attrA.ignored;
            delete attrB.ignored;
            attrA.ov = true;
            attrB.ov = true;
        }
        return {a: attrA, b: attrB};
    };
    //todo: extract to utils in sdk
    var forEach = function (o, func, self, deep) {
        if (!o || !func) {
            return;
        }
        if (!self) {
            self = null;
        }
        deep = !!deep;

        if (Array.isArray(o)) {
            for (var i = 0; i < o.length; i++) {
                func.call(self, i, o[i], o);
                if (deep) {
                    forEach(o[i], func, self, deep);
                }
            }
        }
        else if (_.isObject(o)) {
            for (var key in o) {
                func.call(self, key, o[key], o);
                if (deep) {
                    forEach(o[key], func, self, deep);
                }
            }
        }
    };

    var mergeWithUri = function (oldState, newState) {
        var existingUris = oldState.map(function (item) {
            return item.uri;
        });
        var dst = oldState.map(function (oldStateItem) {
            var newStateItem = newState.filter(function (item) {
                    return item.uri === oldStateItem.uri
                })[0] || {};
            var unIgnored = unIgnore(oldStateItem, newStateItem);
            return mergeAttributes(unIgnored.a, unIgnored.b);
        });
        var newItems = newState.filter(function (item) {
            return existingUris.indexOf(item.uri) === -1;
        });
        dst = dst.concat(newItems);
        return dst;
    };
    var createUpdateAppearance = function (attributeAppearance, valueAppearance, ignoredValues) {
        return function (item) {
            var attributeUri = item.uri.lastIndexOf('/') !== -1 ? item.uri.substring(0, item.uri.lastIndexOf('/')) : '';
            if (!attributeAppearance[attributeUri]) {
                attributeAppearance[attributeUri] = [];
            }
            attributeAppearance[attributeUri].push(item.type);
            valueAppearance[item.uri] = item.type;
            if (item.type === 'IGNORE_ATTRIBUTE') {
                ignoredValues.push(item.uri);
            }
        }
    };
    var mergeEvents = function (attributeAppearance, valueAppearance, ignoredAttributes) {
        var toIgnore = [];
        var updatedAttributes = Object.keys(attributeAppearance).filter(function (key) {
            return attributeAppearance[key].indexOf('IGNORE_ATTRIBUTE') !== -1 && attributeAppearance[key].indexOf('INSERT_ATTRIBUTE') !== -1;
        });
        Object.keys(valueAppearance).filter(function (key) {
            var attributeUri = key.lastIndexOf('/') !== -1 ? key.substring(0, key.lastIndexOf('/')) : null;
            return updatedAttributes.indexOf(attributeUri) !== -1;
        }).forEach(function (key) {
            if (ignoredAttributes.indexOf(key) !== -1) {
                toIgnore.push(key);
            }
            valueAppearance[key] = 'UPDATE_ATTRIBUTE';
        });
        return {appearance: valueAppearance, ignored: toIgnore};
    };
    var mergeAttributes = function (oldState, newState) {
        var array = Array.isArray(newState);
        var dst = array ? [] : {};

        if (array) {
            oldState = oldState || [];
            var hasUris = oldState.some(function (item) {
                return item.uri;
            });
            if (hasUris) {
                dst = mergeWithUri(oldState, newState);
            } else {
                dst = dst.concat(oldState);
                newState.forEach(function (e, i) {
                    if (typeof dst[i] === 'undefined') {
                        dst[i] = e;
                    } else if (_.isObject(e)) {
                        dst[i] = mergeAttributes(oldState[i], e);
                    } else {
                        if (oldState.indexOf(e) === -1) {
                            dst.push(e);
                        }
                    }
                });
            }
        } else {
            if (_.isObject(oldState)) {
                Object.keys(oldState).forEach(function (key) {
                    dst[key] = oldState[key];
                })
            }
            Object.keys(newState).forEach(function (key) {
                if (!_.isObject(newState[key])) {
                    dst[key] = newState[key];
                }
                else {
                    if (!oldState[key]) {
                        dst[key] = newState[key];
                    } else {
                        dst[key] = mergeAttributes(oldState[key], newState[key]);
                    }
                }
            });
        }

        return dst;
    };
    var computeHistoryDiff = function (oldState, newState) {
        var clonedOldState = JSON.parse(JSON.stringify(oldState));
        var clonedNewState = JSON.parse(JSON.stringify(newState));

        var attributes = mergeAttributes(oldState, newState);
        var valueAppearance = {};
        var attributeAppearance = {};
        var ignoredAttributes = [];
        var updateAppearance = createUpdateAppearance(attributeAppearance, valueAppearance, ignoredAttributes);
        attributeDiff.compareAttributes(attributes, clonedNewState)
            .map(function (item) {
                if (item.type === 'IGNORE_ATTRIBUTE' && item.newValue.value === false) {
                    item.type = 'UNDO_IGNORE_ATTRIBUTE';
                }
                return item;
            })
            .forEach(function (item) {
                updateAppearance(item);
            });

        attributeDiff.compareAttributes(attributes, clonedOldState)
            .map(function (item) {
                if (item.type === 'DELETE_ATTRIBUTE') {
                    item.type = 'INSERT_ATTRIBUTE';
                }
                if (item.type === 'IGNORE_ATTRIBUTE') {
                    item.type = item.newValue.value ? 'UNDO_IGNORE_ATTRIBUTE' : 'IGNORE_ATTRIBUTE';
                }
                return item;
            })
            .forEach(function (item) {
                updateAppearance(item);
            });
        var result = mergeEvents(attributeAppearance, valueAppearance, ignoredAttributes);
        forEach(attributes, function (key, item) {
            var ignored = this;
            if (item && ignored.indexOf(item.uri) !== -1) {
                item.ignored = true;
                delete item.ov;
                item.hidden = true;
            }
        }, result.ignored, true);
        return {attributes: attributes, appearance: result.appearance};
    };
    return {
        computeHistoryDiff: computeHistoryDiff
    }
};
module.exports = {
    createHistoryManager: Manager
};
