/**
 * Use this module to implement hierarchical (tree) data structures. where nodes can have multiple parents.
 * @module entity/editMode/MultiparentTree
 * @type {{Node: Node, find: find, findParentNodes: findParentNodes}}
 */

module.exports = {
    Node: Node,
    find: find,
    findParentNodes: findParentNodes
};
/**
 * Use this class to instantiate tree nodes.
 * @internal
 * @param {{}} info - Descriptive or useful information
 * @returns {MultiparentTree.Node}
 * @static
 */
function Node(info) {
    var children = [];
    /**
     * @alias MultiparentTree.Node
     * @namespace
     */
    var internal = {
        /**
         * Use this method to return data from the current node.
         * @returns {{}}
         * @inner
         */
        getInfo: function () {
            return info;
        },
        /**
         * Use this method to return an array of node children.
         * @returns {MultiparentTree.Node[]}
         * @inner
         */
        getChildren: function () {
            return children;
        },
        /**
         * Use this method to remove a child from the current node.
         * @param {MultiparentTree.Node} child - children
         * @inner
         */
        removeChild: function (child) {
            var index = children.indexOf(child);
            children.splice(index, 1);
        },
        /**
         * Use this method to add children to a node.
         * @param {MultiparentTree.Node} child - children
         * @inner
         */
        addChild: function (child) {
            children.push(child);
        }
    };

    return internal;
}
/**
 * Function allows to find node in all children of startNode by predicate
 * @param {MultiparentTree.Node} startNode - initial node
 * @param {Function} predicate - predicate function
 * @returns {MultiparentTree.Node}
 * @static
 */
function find(startNode, predicate) {
    var res = null;
    if (predicate(startNode.getInfo())) {
        return startNode;
    }
    var children = startNode.getChildren();
    for (var i = 0; i < children.length; i++) {
        res = find(children[i], predicate);
        if (res) {
            break;
        }
    }
    return res;
}
/**
 * Use this method to find all nodes by predicate and returns an array of their parent nodes.
 * @param {MultiparentTree.Node} startNode - initial node
 * @param {Function} predicate - predicate function
 * @returns {Array}
 * @static
 */
function findParentNodes(startNode, predicate) {
    var res = [];
    var hasNode = startNode.getChildren().some(function (item) {
        return predicate(item.getInfo());
    });
    if (hasNode) {
        res.push(startNode);
    }
    var edges = startNode.getChildren();
    for (var i = 0; i < edges.length; i++) {
        res.push.apply(res, findParentNodes(edges[i], predicate));
    }
    return res;
}
