jest.unmock('../AttributesDiff');
var AttributesDiff = require('../AttributesDiff');
describe('compareAttributes', function () {

    it('can detect UPDATE_ATTRIBUTE for simple attributes', function () {
        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }]
        };
        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Stark'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: 'Stark'
            }
        });
    });

    it('can detect UPDATE_ATTRIBUTE for attribute inside nested attributes', function () {
        var a = {
            Sword: [{
                uri: '/attributes/Sword/1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        uri: '/attributes/Sword/1/Name/1',
                        value: 'Longclaw'
                    }],
                    Material: [{
                        type: '/attributes/Sword/attributes/Material',
                        uri: '/attributes/Sword/1/Material/1',
                        value: 'Steel'
                    }]
                }
            }]
        };
        var b = {
            Sword: [{
                uri: '/attributes/Sword/1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        uri: '/attributes/Sword/1/Name/1',
                        value: 'Longclaw'
                    }],
                    Material: [{
                        type: '/attributes/Sword/attributes/Material',
                        uri: '/attributes/Sword/1/Material/1',
                        value: 'Valyrian steel'
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/Sword/1/Material/1',
            newValue: {
                value: 'Valyrian steel'
            }
        });
    });

    it('can apply crosswalk to reference attribute', function () {
        var a = {
            Address: [{
                uri: '/attributes/Address/1',
                value: {
                    AddressLine1: [{
                        type: '/attributes/Address/attributes/AddressLine1',
                        uri: '/attributes/Address/1/AddressLine1/2',
                        value: 'Winterfell'
                    }]
                }
            }]
        };
        var b = {
            Address: [{
                uri: '/attributes/Address/1',
                value: {
                    AddressLine1: [{
                        type: '/attributes/Address/attributes/AddressLine1',
                        uri: '/attributes/Address/1/AddressLine1/2',
                        value: 'The Wall'
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);
        var crosswalks = [{
            type: 'configuration/sources/Reltio',
            value: '42',
            attributeURIs: [
                '/attributes/Address/1/AddressLine1/2',
                '/attributes/Address/1']
        }];
        result = AttributesDiff.applyCrosswalks(result, crosswalks);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/Address/1/AddressLine1/2',
            newValue: {
                value: 'The Wall'
            },
            crosswalk: {
                type: 'configuration/sources/Reltio',
                value: '42'
            }
        });
    });

    it('update image using UPDATE_PROFILE_PIC_BY_URI (different)', function () {
        var a = {
            defaultProfilePic : 'entities/uri/attributes/ImageLinks/1'
        };
        var b = {
            defaultProfilePic : 'entities/uri/attributes/ImageLinks/2'
        };

        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_PROFILE_PIC_BY_URI',
            newValue: {
                value: 'entities/uri/attributes/ImageLinks/2'
            }
        });
    });

    it('update image using UPDATE_PROFILE_PIC_BY_URI (equal)', function () {
        var a = {
            defaultProfilePic : 'entities/uri/attributes/ImageLinks/1'
        };
        var b = {
            defaultProfilePic : 'entities/uri/attributes/ImageLinks/1'
        };

        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(0);
    });

    it('update image using UPDATE_PROFILE_PIC_BY_URL (string, different)', function () {
        var a = {
            defaultProfilePicValue : 'http://url.com/image1.png'
        };
        var b = {
            defaultProfilePicValue : 'http://url.com/image2.png'
        };

        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_PROFILE_PIC_BY_URL',
            newValue: {
                value: 'http://url.com/image2.png'
            }
        });
    });

    it('update image using UPDATE_PROFILE_PIC_BY_URL (string, equal)', function () {
        var a = {
            defaultProfilePicValue : 'http://url.com/image1.png'
        };
        var b = {
            defaultProfilePicValue : 'http://url.com/image1.png'
        };

        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(0);
    });

    it('update image using UPDATE_PROFILE_PIC_BY_URL (object, equal)', function () {
        var a = {
            defaultProfilePicValue : {
                uri: '/1/',
                url: 'http://url.com/image1.png'
            }
        };
        var b = {
            defaultProfilePicValue : {
                uri: '/1/',
                url: 'http://url.com/image2.png'
            }
        };

        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(0);
    });

    it('update image using UPDATE_PROFILE_PIC_BY_URL (object, different)', function () {
        var a = {
            defaultProfilePicValue : {
                uri: '/1/',
                url: 'http://url.com/image1.png'
            }
        };
        var b = {
            defaultProfilePicValue : {
                uri: '/2/',
                url: 'http://url.com/image1.png'
            }
        };

        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_PROFILE_PIC_BY_URL',
            newValue: {
                value: {
                    uri: '/2/',
                    url: 'http://url.com/image1.png'
                }
            }
        });
    });

    it('update image using UPDATE_PROFILE_PIC_BY_URL (mixed, different)', function () {
        var a = {
            defaultProfilePicValue : ''
        };
        var b = {
            defaultProfilePicValue : {
                uri: '/2/',
                url: 'http://url.com/image1.png'
            }
        };

        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_PROFILE_PIC_BY_URL',
            newValue: {
                value: {
                    uri: '/2/',
                    url: 'http://url.com/image1.png'
                }
            }
        });
    });

    it('Ignore attribute should produce ignore true event', function () {
        
        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                ignored: false,
                value: 'Jon'
            }]
        };
        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon',
                ignored: true
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: '/attributes/FirstName/1',
            newValue: {
                value: true
            }
        });
    });

    it('Ignore attribute should produce ignore true event for attributes with lookups', function () {

        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                ignored: false,
                value: 'Jon',
                lookupCode: 'J'
            }]
        };
        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon',
                ignored: true,
                lookupCode: 'J'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: '/attributes/FirstName/1',
            newValue: {
                value: true
            }
        });
    });

    it('Unignore attribute should produce ignore false event', function () {

        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                ignored: true,
                value: 'Jon'
            }]
        };
        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: '/attributes/FirstName/1',
            newValue: {
                value: false
            }
        });
    });

    it('should not produce diff if only labels are different', function () {
        var a = {
            Sword: [{
                uri: '/attributes/Sword/1',
                label: 'Longclaw',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        uri: '/attributes/Sword/1/Name/1',
                        value: 'Longclaw'
                    }]
                }
            }]
        };
        var b = {
            Sword: [{
                uri: '/attributes/Sword/1',
                label: 'Longclaw was changed',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        uri: '/attributes/Sword/1/Name/1',
                        value: 'Longclaw'
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(0);
    });

    it('should delete empty values', function () {
        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }]
        };
        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: ''
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/1'
        });
    });

    it('should delete null values', function () {
        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }]
        };
        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: null
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/1'
        });
    });

    it('should not produce diff if lookup codes are equals', function () {
        var a = {
            CountryCode: [{
                type: '/attributes/CountryCode',
                uri: '/attributes/CountryCode/1',
                value: 'Westeros',
                lookupCode: 'WS'
            }]
        };
        var b = {
            CountryCode: [{
                type: '/attributes/CountryCode',
                uri: '/attributes/CountryCode/1',
                value: 'Westeros (WS)',
                lookupCode: 'WS'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(0);
    });

    it('should detect UPDATE_ATTRIBUTE for different lookup codes', function () {
        var a = {
            CountryCode: [{
                type: '/attributes/CountryCode',
                uri: '/attributes/CountryCode/1',
                value: 'Westeros',
                lookupCode: 'WS'
            }]
        };
        var b = {
            CountryCode: [{
                type: '/attributes/CountryCode',
                uri: '/attributes/CountryCode/1',
                value: 'Meereen',
                lookupCode: 'ME'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result[0]).toEqual({
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/CountryCode/1',
            newValue: {
                value: 'ME'
            }
        });
    });

});
