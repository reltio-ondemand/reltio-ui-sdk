jest.unmock('../AttributesDiff');
var AttributesDiff = require('../AttributesDiff');
describe('compareAttributes', function () {

    it('can insert startDate/endDate', function () {
        var a = {
            attributes: {}
        };
        var b = {
            attributes: {},
            startDate: 518727600000,
            endDate: 518727600000
        };

        var result = AttributesDiff.compareActivenessAttributes(a, b);

        expect(result.length).toBe(2);
        expect(result[0]).toEqual({
            type: 'UPDATE_START_DATE',
            newValue: {
                value: '518727600000'
            }
        });
        expect(result[1]).toEqual({
            type: 'UPDATE_END_DATE',
            newValue: {
                value: '518727600000'
            }
        });
    });

    it('can delete startDate/endDate', function () {
        var a = {
            attributes: {},
            startDate: 518727600000,
            endDate: 518727600000
        };
        var b = {
            attributes: {},
            startDate: 518727600000
        };

        var result = AttributesDiff.compareActivenessAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_END_DATE',
            newValue: {
                value: ''
            }
        });
    });

    it('can update startDate/endDate', function () {
        var a = {
            attributes: {},
            startDate: 518727600000,
            endDate: 518727600000
        };
        var b = {
            attributes: {},
            startDate: 518727600000,
            endDate: 518727600001
        };

        var result = AttributesDiff.compareActivenessAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_END_DATE',
            newValue: {
                value: '518727600001'
            }
        });
    });

});
