jest.unmock('../AttributesDiff');
var AttributesDiff = require('../AttributesDiff');
describe('ApplyCrosswalks', function () {

    it('can just apply Reltio crosswalk', function () {
        var diff = [{
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: 'Snow'
            }
        }, {
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/LastName',
            newValue: [
                {value: 'Stark'},
                {value: 'Targaryen'}
            ]
        }];
        var crosswalks = [{
            type: 'configuration/sources/Reltio',
            value: '123',
            attributes: [
                '/attributes/LastName/1'
            ]
        }];

        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        expect(diff[0].crosswalk).toEqual({
            type: 'configuration/sources/Reltio',
            value: '123'
        });
        expect(diff[1].crosswalk).toBeUndefined();
    });

    it('shouldn\'t apply Reltio crosswalk even there is no Reltio in crosswalks in case of insert', function () {
        //We could apply reltio crosswalk if and only if it exists and has value
        var diff = [{
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/LastName',
            newValue: [
                {value: 'Stark'},
                {value: 'Targaryen'}
            ]
        }];
        var crosswalks = [{
            type: 'configuration/sources/HMS',
            value: '123',
            attributes: [
                '/attributes/LastName/1'
            ]
        }];

        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        expect(diff[0].crosswalk).toBeUndefined();
    });

    it('should replace UPDATE_ATTRIBUTE with INSERT_ATTRIBUTE + IGNORE_ATTRIBUTE for non-Reltio crosswalk', function () {
        var diff = [{
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: 'Snow'
            }
        }];
        var crosswalks = [{
            type: 'configuration/sources/HMS',
            value: '123',
            attributes: [
                '/attributes/LastName/1'
            ]
        }];

        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        expect(diff[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/LastName',
            newValue: [
                {value: 'Snow'}
            ]
        });
        expect(diff[1]).toEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: true
            }
        });
    });

    it('should replace DELETE_ATTRIBUTE with IGNORE_ATTRIBUTE for non-Reltio crosswalk', function () {
        var diff = [{
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/1'
        }];
        var crosswalks = [{
            type: 'configuration/sources/HMS',
            value: '123',
            attributes: [
                '/attributes/LastName/1'
            ]
        }];

        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        expect(diff[0]).toEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: true
            }
        });
    });

    it('should apply crosswalk for DELETE_ATTRIBUTE for Reltio crosswalk', function () {
        var diff = [{
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/1'
        }];
        var crosswalks = [{
            type: 'configuration/sources/Reltio',
            value: '123',
            attributes: [
                '/attributes/LastName/1'
            ]
        }];

        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        expect(diff[0]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            crosswalk: {
                type: 'configuration/sources/Reltio',
                value: '123'
            }
        });
    });

    it('should handle DELETE_ATTRIBUTE=IGNORE_ATTRIBUTE+DELETE_ATTRIBUTE for value in multiple crosswalks', function () {
        var diff = [{
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/1'
        }];
        var crosswalks = [{
            type: 'configuration/sources/Reltio',
            value: '1',
            attributes: [
                '/attributes/LastName/1'
            ]
        }, {
            type: 'configuration/sources/Reltio',
            value: '2',
            attributes: [
                '/attributes/LastName/1'
            ]
        }, {
            type: 'configuration/sources/HMS',
            value: '3',
            attributes: [
                '/attributes/LastName/1'
            ]
        }, {
            type: 'configuration/sources/HMS',
            value: '4',
            attributes: [
                '/attributes/LastName/1'
            ]
        }];

        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        expect(diff[0]).toEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: true
            }
        });
        expect(diff[1]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            crosswalk: {
                type: 'configuration/sources/Reltio',
                value: '1'
            }
        });
        expect(diff[2]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            crosswalk: {
                type: 'configuration/sources/Reltio',
                value: '2'
            }
        });
    });

    it('should handle UPDATE_ATTRIBUTE=IGNORE_ATTRIBUTE+UPDATE_ATTRIBUTE for value in multiple crosswalks', function () {
        var diff = [{
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: 'Snow'
            }
        }];
        var crosswalks = [{
            type: 'configuration/sources/HMS',
            value: '1',
            attributes: [
                '/attributes/LastName/1'
            ]
        }, {
            type: 'configuration/sources/WB',
            value: '2',
            attributes: [
                '/attributes/LastName/1'
            ]
        }, {
            type: 'configuration/sources/Reltio',
            value: '3',
            attributes: [
                '/attributes/LastName/1'
            ]
        }, {
            type: 'configuration/sources/Reltio',
            value: '4',
            attributes: [
                '/attributes/LastName/1'
            ]
        }];

        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);


        expect(diff[0]).toEqual({
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {value: 'Snow'},
            crosswalk: {
                type: 'configuration/sources/Reltio',
                value: '3'
            }
        });
        expect(diff[1]).toEqual({
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {value: 'Snow'},
            crosswalk: {
                type: 'configuration/sources/Reltio',
                value: '4'
            }
        });
        expect(diff[2]).toEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: true
            }
        });
    });

    it('should handle UPDATE_ATTRIBUTE=IGNORE_ATTRIBUTE+INSERT_ATTRIBUTE for value in multiple crosswalks', function () {
        var diff = [{
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: 'Snow'
            }
        }];
        var crosswalks = [{
            type: 'configuration/sources/HMS',
            value: '1',
            attributes: [
                '/attributes/LastName/1'
            ]
        }, {
            type: 'configuration/sources/WB',
            value: '2',
            attributes: [
                '/attributes/LastName/1'
            ]
        }, {
            type: 'configuration/sources/Reltio',
            value: '3',
            attributes: []
        }, {
            type: 'configuration/sources/Reltio',
            value: '4',
            attributes: []
        }];

        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        expect(diff[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/LastName',
            newValue: [
                {value: 'Snow'}
            ]
        });
        expect(diff[1]).toEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: true
            }
        });
    });

    it('should replace DELETE_ATTRIBUTE with IGNORE_ATTRIBUTE for reference attribute with no crosswalks', function () {
        var diff = [{
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/Address/1'
        }];
        var crosswalks = [{
            type: 'configuration/sources/Reltio',
            value: '123',
            attributes: []
        }];

        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        expect(diff[0]).toEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: '/attributes/Address/1',
            newValue: {
                value: true
            }
        });
    });

    it('should return change as is if it already has crosswalk', function () {
        var change = {
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/LastName/1',
            newValue: {
                value: 'Snow'
            },
            crosswalk: {
                type: 'configuration/sources/Reltio',
                value: '123'
            }
        };
        var diff = [change];
        var crosswalks = [];

        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        expect(diff[0]).toEqual(change);
    });

});
