jest.unmock('../AttributesDiff');
jest.unmock('./ReferenceAttribute.res.js');
var AttributesDiff = require('../AttributesDiff');
var ReferenceAttributeResources = require('./ReferenceAttribute.res');
describe('reference attribute tests', function () {

    it('shouldn\'t produce any events in case of the same reference attribute', function () {

        //we have no Object.assign :(
        var clone = (JSON.parse(JSON.stringify(ReferenceAttributeResources.initAttribute)));
        var results = AttributesDiff.compareAttributes(ReferenceAttributeResources.initAttribute, clone);

        expect(results.length).toBe(0);
    });

    it('should produce ignore and add events for updated attribute without reltio crosswalk', function () {
        var results = AttributesDiff.compareAttributes(ReferenceAttributeResources.initAttribute, ReferenceAttributeResources.modifiedRelationAttribute);
        var crosswalks = AttributesDiff.collectCrosswalksFromAttributes(ReferenceAttributeResources.initAttribute);
        results = AttributesDiff.applyCrosswalks(results, crosswalks);

        expect(results.length).toBe(2);
        expect(results[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType',
            newValue: [
                {
                    value: 'Mailing (Mailing_vod__c)'
                }
            ]
        });
        expect(results[1]).toEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn',
            newValue: {value: true}
        });
    });

    it('should produce remove/ignore event for reference attribute with changed entity', function () {
        var results = AttributesDiff.compareAttributes(ReferenceAttributeResources.initAttribute, ReferenceAttributeResources.modifiedEntity);
        expect(results.length).toBe(2);
        expect(results[0]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT'
        });
        expect(results[1].type).toBe('INSERT_ATTRIBUTE');
        expect(results[1].uri).toBe('entities/1xkCLdTZ/attributes/Address');
    });

    it('should generate delete event without any CW for ref attribute with reltio cw', function () {
        //the cw will be added in the apply cw method 
        var initAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: [{}],
                refRelation: [{
                    'type': 'configuration/relationTypes/HasAddress',
                    'crosswalks': [{
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
                        'type': 'configuration/sources/Reltio',
                        'value': 'R|1001104040|1000|10|10|SH',
                        'createDate': '2015-09-07T11:54:25.021Z',
                        'updateDate': '2015-09-07T11:54:25.021Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                        'crosswalkExternalInfo': {}
                    }]
                }]
            }]
        };
        var modified = {Address: []};

        var results = AttributesDiff.compareAttributes(initAttr, modified);
        expect(results.length).toBe(1);
        expect(results[0]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT'
        });
    });

    it('should generate correct events in case of change entity (entity is array)', function () {
        var initAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: [{
                    'type': 'configuration/entityTypes/Location',
                    'objectURI': '/1234'

                }],
                refRelation: [{
                    'type': 'configuration/relationTypes/HasAddress',
                    'crosswalks': [{
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
                        'type': 'configuration/sources/ECP-400',
                        'value': 'R|1001104040|1000|10|10|SH',
                        'createDate': '2015-09-07T11:54:25.021Z',
                        'updateDate': '2015-09-07T11:54:25.021Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                        'crosswalkExternalInfo': {}
                    }]
                }]
            }]
        };
        var modifiedAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: [{
                    'type': 'configuration/entityTypes/Location',
                    'objectURI': '/new1234'

                }],
                refRelation: [{
                    'type': 'configuration/relationTypes/HasAddress',
                    'crosswalks': [{
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
                        'type': 'configuration/sources/ECP-400',
                        'value': 'R|1001104040|1000|10|10|SH',
                        'createDate': '2015-09-07T11:54:25.021Z',
                        'updateDate': '2015-09-07T11:54:25.021Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                        'crosswalkExternalInfo': {}
                    }]
                }]
            }]
        };
        var results = AttributesDiff.compareAttributes(initAttr, modifiedAttr);
        expect(results.length).toBe(2);
        expect(Object.prototype.toString.call(results[1].newValue[0].refEntity)).toBe('[object Object]');
        expect(Object.prototype.toString.call(results[1].newValue[0].refRelation)).toBe('[object Object]')
    });

    it('should generate correct events in case of refEntity clearing (entity is array)', function () {
        var initAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: [{
                    'type': 'configuration/entityTypes/Location',
                    'objectURI': '/1234'

                }]
            }]
        };
        var modifiedAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: []
            }]
        };
        var results = AttributesDiff.compareAttributes(initAttr, modifiedAttr);
        expect(results.length).toBe(2);
        expect(results[1].newValue[0].refEntity).toBeUndefined();
    });

    it('should generate correct events in case of change entity (entity is object)', function () {
        var initAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: {
                    'type': 'configuration/entityTypes/Location',
                    'objectURI': '/1234'

                },
                refRelation: {
                    'type': 'configuration/relationTypes/HasAddress',
                    'crosswalks': [{
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
                        'type': 'configuration/sources/ECP-400',
                        'value': 'R|1001104040|1000|10|10|SH',
                        'createDate': '2015-09-07T11:54:25.021Z',
                        'updateDate': '2015-09-07T11:54:25.021Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                        'crosswalkExternalInfo': {}
                    }]
                }
            }]
        };
        var modifiedAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: {
                    'type': 'configuration/entityTypes/Location',
                    'objectURI': '/new1234'

                },
                refRelation: {
                    'type': 'configuration/relationTypes/HasAddress',
                    'crosswalks': [{
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
                        'type': 'configuration/sources/ECP-400',
                        'value': 'R|1001104040|1000|10|10|SH',
                        'createDate': '2015-09-07T11:54:25.021Z',
                        'updateDate': '2015-09-07T11:54:25.021Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                        'crosswalkExternalInfo': {}
                    }]
                }
            }]
        };
        var results = AttributesDiff.compareAttributes(initAttr, modifiedAttr);
        expect(results.length).toBe(2);
        expect(Object.prototype.toString.call(results[1].newValue[0].refEntity)).toBe('[object Object]');
        expect(Object.prototype.toString.call(results[1].newValue[0].refRelation)).toBe('[object Object]')
    });

    it('should collect correct cw from attributes', function () {
        var initAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: [{
                    'type': 'configuration/entityTypes/Location',
                    'objectURI': '/1234'

                }],
                refRelation: [{
                    'type': 'configuration/relationTypes/HasAddress',
                    'crosswalks': [{
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
                        'type': 'configuration/sources/ECP-400',
                        'value': 'R|1001104040|1000|10|10|SH',
                        'createDate': '2015-09-07T11:54:25.021Z',
                        'updateDate': '2015-09-07T11:54:25.021Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                        'crosswalkExternalInfo': {}
                    }]
                }]
            }]
        };

        var results = AttributesDiff.collectCrosswalksFromAttributes(initAttr);
        expect(results[0].attributeURIs.length).toBe(2);
    });
    it('should collect correct cw from ref attribute and add it to the existing cw', function () {
        var initAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: [{
                    'type': 'configuration/entityTypes/Location',
                    'objectURI': '/1234'

                }],
                refRelation: [{
                    'type': 'configuration/relationTypes/HasAddress',
                    'crosswalks': [{
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
                        'type': 'configuration/sources/ECP-400',
                        'value': 'R|1001104040|1000|10|10|SH',
                        'createDate': '2015-09-07T11:54:25.021Z',
                        'updateDate': '2015-09-07T11:54:25.021Z',
                        'attributeURIs': ['1', '2'],
                        'crosswalkExternalInfo': {}
                    }]
                }]
            }]
        };
        var initCW =[{
            'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
            'type': 'configuration/sources/ECP-400',
            'value': 'R|1001104040|1000|10|10|SH',
            'attributeURIs': ['3', '4'],
            'crosswalkExternalInfo': {}
        }];
        var results = AttributesDiff.collectCrosswalksFromAttributes(initAttr, initCW);
        expect(results[0].attributes.length).toBe(4);
    });

    it('shoudn\'t add the tmp refRelation object URI to the INSERT_ATTRIBUTE event by default', function () {
        var initAttr = {
            Address: []
        };
        var modifiedAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: [{
                    'type': 'configuration/entityTypes/Location',
                    'objectURI': '/new1234'

                }],
                refRelation: [{
                    'type': 'configuration/relationTypes/HasAddress',
                    'objectURI': '/relation/uri$$test/123'
                }]
            }]
        };
        var results = AttributesDiff.compareAttributes(initAttr, modifiedAttr);
        expect(results.length).toBe(1);
        expect(results[0].newValue[0].refEntity.objectURI).toBe('/new1234');
        expect(results[0].newValue[0].refRelation.objectURI).toBeUndefined()
    });

    it('shoud add the tmp refRelation object URI to the INSERT_ATTRIBUTE event if keep uri is true', function () {
        var initAttr = {
            Address: []
        };
        var modifiedAttr = {
            Address: [{
                value: {},
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: [{
                    'type': 'configuration/entityTypes/Location',
                    'objectURI': '/new1234'

                }],
                refRelation: [{
                    'type': 'configuration/relationTypes/HasAddress',
                    'objectURI': '/relation/uri$$test/123'
                }]
            }]
        };
        var results = AttributesDiff.compareAttributes(initAttr, modifiedAttr, true);
        expect(results.length).toBe(1);
        expect(results[0].newValue[0].refEntity.objectURI).toBe('/new1234');
        expect(results[0].newValue[0].refRelation.objectURI).toBe('/relation/uri$$test/123')
    });

    it('should not fail when there is paging in reference attribute', function () {
        var initAttr = {
            Address: [{
                value: {
                    AddressLine1: [{
                        type: 'configuration/entityTypes/Location/attributes/AddressLine1',
                        ov: true,
                        value: '50 Charlton Ave E',
                        uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WUNGSYsN'
                    }],
                    paging: {}
                },
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: [{
                    'type': 'configuration/entityTypes/Location',
                    'objectURI': '/1234'
                }]
            }]
        };
        var modifiedAttr = {
            Address: [{
                value: {
                    AddressLine1: [{
                        type: 'configuration/entityTypes/Location/attributes/AddressLine1',
                        ov: true,
                        value: '50 Charlton Ave E',
                        uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WUNGSYsN'
                    }],
                    paging: {}
                },
                uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                refEntity: []
            }]
        };
        var results = AttributesDiff.compareAttributes(initAttr, modifiedAttr);
        expect(results.length).toBe(2);
    });

});
