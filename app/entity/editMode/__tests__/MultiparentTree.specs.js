jest.unmock('../MultiparentTree');
var MultiparentTree = require('../MultiparentTree');

describe('Multiparent tree', function () {
    var all;
    var rickardStark;
    var brandonStark;
    var eddartStark;
    var robbStark;
    var catelynTully;
    var sansaStark;
    var aryaStark;
    var rickonStark;
    var unknown;
    var johnSnow;
    beforeEach(function () {
        all = MultiparentTree.Node('Сharacters');
        rickardStark = MultiparentTree.Node('Rickard Stark');
        brandonStark = MultiparentTree.Node('Brandon Stark');
        eddartStark = MultiparentTree.Node('Eddard Stark');
        robbStark = MultiparentTree.Node('Robb Stark');
        catelynTully = MultiparentTree.Node('Catelyn Tully');
        sansaStark = MultiparentTree.Node('Sansa Stark');
        aryaStark = MultiparentTree.Node('Arya Stark');
        rickonStark = MultiparentTree.Node('Rickon Stark');
        unknown = MultiparentTree.Node('Unknown');
        johnSnow = MultiparentTree.Node('John Snow');
        all.addChild(rickardStark);
        all.addChild(catelynTully);
        all.addChild(unknown);

        rickardStark.addChild(brandonStark);
        rickardStark.addChild(eddartStark);
        eddartStark.addChild(robbStark);
        eddartStark.addChild(sansaStark);
        eddartStark.addChild(aryaStark);
        eddartStark.addChild(rickonStark);
        eddartStark.addChild(johnSnow);

        catelynTully.addChild(robbStark);
        catelynTully.addChild(sansaStark);
        catelynTully.addChild(aryaStark);
        catelynTully.addChild(rickonStark);

        unknown.addChild(johnSnow);
    });

    it('should find nodes correctly', function () {
        expect(MultiparentTree.find(rickardStark, function (item) {
            return item === 'John Snow'
        })).toBe(johnSnow);
        expect(MultiparentTree.find(unknown, function (item) {
            return item === 'John Snow'
        })).toBe(johnSnow);
    });

    it('should find parent nodes correctly', function () {
        var parents = MultiparentTree.findParentNodes(all, function (item) {
            return item === 'John Snow'
        });
        expect(parents.length).toBe(2);
        expect(parents.indexOf(unknown)).not.toBe(-1);
        expect(parents.indexOf(eddartStark)).not.toBe(-1);
        expect(parents.indexOf(rickardStark)).toBe(-1);

        var parentsForRob = MultiparentTree.findParentNodes(all, function (item) {
            return item === 'Robb Stark';
        });
        expect(parentsForRob.length).toBe(2);
        expect(parentsForRob.indexOf(catelynTully)).not.toBe(-1);
        expect(parentsForRob.indexOf(rickardStark)).toBe(-1);
        expect(parentsForRob.indexOf(eddartStark)).not.toBe(-1);
    });

    it('should remove nodes correctly', function () {
        rickardStark.removeChild(eddartStark);
        expect(MultiparentTree.find(rickardStark, function (item) {
            return item === 'Eddard Stark';
        })).toBeNull();
    });
});

describe('LookupsTest', function () {
    it('should process complex lookups well', function () {
        var all = MultiparentTree.Node({uri: 'configuration/entityTypes/HCA'});
        var country = MultiparentTree.Node({
            dependentLookupCode: 'Test Country',
            uri: 'configuration/entityTypes/HCA/attributes/TestCountry'
        });
        var state = MultiparentTree.Node({
            dependentLookupCode: 'Test State',
            uri: 'configuration/entityTypes/HCA/attributes/TestState'
        });
        var city = MultiparentTree.Node({
            dependentLookupCode: 'Test City',
            uri: 'configuration/entityTypes/HCA/attributes/TestCity'
        });
        var anotherCity = MultiparentTree.Node({
            dependentLookupCode: 'Test City',
            uri: 'configuration/entityTypes/HCA/attributes/TestNested/attributes/TestCitySubAttribute'
        });

        all.addChild(country);
        country.addChild(state);
        country.addChild(city);
        state.addChild(city);
        state.addChild(anotherCity);
        var parentsForCity = MultiparentTree.findParentNodes(all, function (item) {
            return item.dependentLookupCode === 'Test City';
        });
        expect(parentsForCity.length).toBe(2);
    });
});
