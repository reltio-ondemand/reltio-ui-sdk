jest.unmock('../AttributesDiff');
var AttributesDiff = require('../AttributesDiff');

describe('roles and tags', function () {

    it('can insert role/tag', function () {

        var tags = AttributesDiff.compareTags(null, ['Lannisters', 'Baratheon']);
        var roles = AttributesDiff.compareRoles(null, ['Lann', 'Azor Ahai']);

        expect(tags.length).toBe(1);
        expect(roles.length).toBe(1);
        expect(tags[0]).toEqual({
            type: 'UPDATE_TAGS',
            newValue: ['Lannisters', 'Baratheon']
        });
        expect(roles[0]).toEqual({
            type: 'UPDATE_ROLES',
            newValue: ['Lann', 'Azor Ahai']
        });
    });

    it('can delete role/tag', function () {

        var tags = AttributesDiff.compareTags(['Lannisters', 'Baratheon'], []);
        var roles = AttributesDiff.compareRoles(['Lann', 'Azor Ahai'], []);

        expect(tags.length).toBe(1);
        expect(roles.length).toBe(1);
        expect(tags[0]).toEqual({
            type: 'UPDATE_TAGS',
            newValue: []
        });
        expect(roles[0]).toEqual({
            type: 'UPDATE_ROLES',
            newValue: []
        });
    });

    it('can update role/tag', function () {

        var tags = AttributesDiff.compareTags(['Lannisters', 'Baratheon'], ['Lannisters', 'Baratheon', 'Frey']);
        var roles = AttributesDiff.compareRoles(['Lann', 'Azor Ahai'], ['Azor Ahai', 'Nissa Nissa']);

        expect(tags.length).toBe(1);
        expect(roles.length).toBe(1);
        expect(tags[0]).toEqual({
            type: 'UPDATE_TAGS',
            newValue: ['Lannisters', 'Baratheon', 'Frey']
        });
        expect(roles[0]).toEqual({
            type: 'UPDATE_ROLES',
            newValue: ['Azor Ahai', 'Nissa Nissa']
        });
    });

    it('can doesn\'t produce not necessary event', function () {

        var tags = AttributesDiff.compareTags(['Lannisters', 'Baratheon'], ['Lannisters', 'Baratheon']);
        var roles = AttributesDiff.compareRoles(['Lann', 'Azor Ahai'], ['Lann', 'Azor Ahai']);

        expect(tags.length).toBe(0);
        expect(roles.length).toBe(0);
    });


});
