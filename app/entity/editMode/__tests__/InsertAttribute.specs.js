jest.unmock('../AttributesDiff');
var AttributesDiff = require('../AttributesDiff');
describe('compareAttributes', function () {

    it('can detect INSERT_ATTRIBUTE for simple attributes', function () {
        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }]
        };
        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }, {
                type: '/attributes/LastName',
                uri: 'uri/attributes/LastName/$uri23',
                value: 'Stark'
            }, {
                type: '/attributes/LastName',
                uri: 'uri/attributes/LastName/$uri24',
                value: 'Targaryen'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: 'uri/attributes/LastName',
            newValue: [
                {value: 'Stark'},
                {value: 'Targaryen'}
            ]
        });
    });

    it('can detect INSERT_ATTRIBUTE for whole nested attributes', function () {
        var a = {
            FullName: [{
                type: '/attributes/FullName',
                uri: '/attributes/FullName/1',
                value: 'Jon Snow'
            }]
        };
        var b = {
            FullName: [{
                type: '/attributes/FullName',
                uri: '/attributes/FullName/1',
                value: 'Jon Snow'
            }],
            Sword: [{
                uri: '/attributes/Sword/$uri_1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        value: 'Longclaw',
                        uri: '$uri_2'
                    }],
                    Material: [{
                        type: '/attributes/Sword/attributes/Material',
                        value: 'Valyrian steel',
                        uri: '$uri_3'
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Sword',
            newValue: [{
                value: {
                    Name: [{
                        value: 'Longclaw'
                    }],
                    Material: [{
                        value: 'Valyrian steel'
                    }]
                }
            }]
        });
    });

    it('can detect INSERT_ATTRIBUTE for simple attribute inside nested attribute', function () {
        var a = {
            Sword: [{
                uri: '/attributes/Sword/1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        uri: '/attributes/Sword/1/attributes/Name/1',
                        value: 'Longclaw'
                    }]
                }
            }]
        };
        var b = {
            Sword: [{
                uri: '/attributes/Sword/1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        uri: '/attributes/Sword/1/attributes/Name/1',
                        value: 'Longclaw'
                    }],
                    Material: [{
                        type: '/attributes/Sword/attributes/Material',
                        uri: '/attributes/Sword/1/attributes/Material/$uri_1',
                        value: 'Valyrian steel'
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Sword/1/attributes/Material',
            newValue: [
                {value: 'Valyrian steel'}
            ]
        });
    });

    it('should remove crosswalks for INSERT_ATTRIBUTE', function () {
        var a = {};
        var b = {
            Sword: [{
                uri: '/attributes/Sword/$uri_1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        value: 'Longclaw',
                        uri: '$uri_2',
                        crosswalks: [{
                            type: 'configuration/sources/Reltio'
                        }]
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Sword',
            newValue: [{
                value: {
                    Name: [{
                        value: 'Longclaw'
                    }]
                }
            }]
        });

        expect(b.Sword[0].value.Name[0].uri).toBe('$uri_2');
    });


    it('insert image using UPDATE_PROFILE_PIC_BY_URI', function () {
        var a = {};
        var b = {
            defaultProfilePic: 'entities/uri/attributes/ImageLinks/1'
        };


        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_PROFILE_PIC_BY_URI',
            newValue: {
                value: 'entities/uri/attributes/ImageLinks/1'
            }
        });
    });

    it('insert image using UPDATE_PROFILE_PIC_BY_URL', function () {
        var a = {};
        var b = {
            defaultProfilePicValue: 'http://url.com/image1.png'
        };

        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_PROFILE_PIC_BY_URL',
            newValue: {
                value: 'http://url.com/image1.png'
            }
        });
    });

    it('should skip empty values', function () {
        var a = {};
        var b = {
            Name: [{
                uri: '/attributes/Name/name1',
                value: ''
            }, {
                uri: '/attributes/Name/name2',
                value: 'Jon'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Name',
            newValue: [
                {value: 'Jon'}
            ]
        });
    });

    it('should skip null values', function () {
        var a = {};
        var b = {
            Name: [{
                uri: '/attributes/Name/name1',
                value: null
            }, {
                uri: '/attributes/Name/name2',
                value: 'Jon'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Name',
            newValue: [
                {value: 'Jon'}
            ]
        });
    });

    it('should skip empty nested attribute with empty values', function () {
        var a = {};
        var b = {
            Sword: [{
                uri: '/attributes/Sword/$uri_1',
                value: {
                    e1: [{
                        type: '/attributes/Sword/attributes/e1',
                        value: '',
                        uri: '$uri_2'
                    }],
                    e2: [{
                        type: '/attributes/Sword/attributes/e2',
                        value: '',
                        uri: '$uri_3'
                    }],
                    e3: [{
                        type: '/attributes/Sword/attributes/e3',
                        value: null,
                        uri: '$uri_3'
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(0);
    });

    it('should skip empty nested attribute values', function () {
        var a = {};
        var b = {
            Nested: [{
                uri: '/attributes/Nested/uri$000',
                value: {}
            },
                {
                    uri: '/attributes/Nested/uri$001',
                    value: {
                        SubAttr: [{
                            value: ''
                        }, {
                            value: null
                        }]
                    }
                },
                {
                    uri: '/attributes/Nested/uri$002',
                    value: {
                        SubAttr: [{
                            value: null
                        }, {
                            value: ''
                        }]
                    }
                },
                {
                    uri: '/attributes/Nested/uri$003',
                    value: {
                        SubAttr: [{
                            value: 'Alex'
                        }]
                    }
                }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Nested',
            newValue: [{
                value: {
                    SubAttr: [{
                        value: 'Alex'
                    }]
                }
            }]
        });
    });

    it('should exclude broken nested attributes', function () {
        var a = {};
        var b = {
            Nested: [{
                uri: '/attributes/Nested/uri$001',
                value: {
                    SubAttr1: [{}],
                    SubAttr2: undefined,
                    SubAttr3: null,
                    SubAttr4: [{
                        uri: '/attributes/Nested/uri$001/SubAttr5/uri0',
                        value : 0
                    },{
                        value : ''
                    }],
                    SubAttr5: [{
                        uri: '/attributes/Nested/uri$001/SubAttr5/uri1',
                        value : false
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result).toEqual([{
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Nested',
            newValue: [{ 
                value: {
                    SubAttr4: [{
                        value: 0
                    }],
                    SubAttr5: [{
                        value: false
                    }]
                }
            }]
        }]);
    });

    it('should exclude broken simple attributes', function () {
        var a = {};
        var b = {
            Simple: [
                undefined,
                null,
                { value : 0, uri: '/attributes/Simple/uri0'},
                { value : false, uri: '/attributes/Simple/uri1'},
                { value : '', uri: '/attributes/Simple/uri2' }
            ],
            Broken : undefined
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result).toEqual([{
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Simple',
            newValue: [
                { value : 0},
                { value : false }
            ]
        }]);
    });
    
    it('should exclude empty sub-attributes of nested attribute', function () {
        var a = {};
        var b = {
            Nested: [
            {
                uri: '/attributes/Nested/uri$001',
                value: {
                    SubAttr1: [{
                        value: ''
                    }],
                    SubAttr2: [{
                        value: 'value1'
                    }]
                }
            },
            {
                uri: '/attributes/Nested/uri$001',
                value: {
                    SubAttr1: [{
                        value: null
                    }],
                    SubAttr2: [{
                        value: 'value2'
                    }]
                }
            },
            {
                uri: '/attributes/Nested/uri$001',
                value: {
                    SubAttr1: [{
                        value: '1'
                    }, {
                        value: null
                    }]
                }
            },
            {
                uri: '/attributes/Nested/uri$002',
                value: {
                    SubAttr1: [{
                        value: '2'
                    }, {
                        value: ''
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Nested',
            newValue: [
                {value: {
                    SubAttr2: [{
                        value: 'value1'
                    }]
                }},
                {value: {
                    SubAttr2: [{
                        value: 'value2'
                    }]
                }},
                {value: {
                    SubAttr1: [{
                        value: '1'
                    }]
                }},
                {value: {
                    SubAttr1: [{
                        value: '2'
                    }]
                }}
            ]
        });
    });
    
    it('should create INSERT_ATTRIBUTE with lookup codes', function () {
        var a = {};
        var b = {
            CountryCode: [{
                type: '/attributes/CountryCode',
                uri: '/attributes/CountryCode/1',
                value: 'Meereen',
                lookupCode: 'ME'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/CountryCode',
            newValue: [
                {value: 'ME'}
            ]
        });
    });

    it('should create INSERT_ATTRIBUTE in case of Reference Attribute with entity only', function () {
        var a = {};
        var b = {
            Children: [{
                value: {},
                type: '/attributes/Children',
                uri: '/attributes/Children/Rob123',
                refEntity: [
                    {objectURI: 'entities/Jhon345'}]
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Children',
            newValue: [{value: {}, refEntity: {objectURI: 'entities/Jhon345'}}]
        });
    });

    it('should keep uris if needed for simple attributes', function () {
        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }]
        };
        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }, {
                type: '/attributes/LastName',
                uri: 'uri/attributes/LastName/$uri23',
                value: 'Stark'
            }, {
                type: '/attributes/LastName',
                uri: 'uri/attributes/LastName/$uri24',
                value: 'Targaryen'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b, true);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: 'uri/attributes/LastName',
            newValue: [
                {value: 'Stark', uri: 'uri/attributes/LastName/$uri23'},
                {value: 'Targaryen', uri: 'uri/attributes/LastName/$uri24'}
            ]
        });
    });

    it('should keep uris if needed for nested attributes', function () {
        var a = {
            FullName: [{
                type: '/attributes/FullName',
                uri: '/attributes/FullName/1',
                value: 'Jon Snow'
            }]
        };
        var b = {
            FullName: [{
                type: '/attributes/FullName',
                uri: '/attributes/FullName/1',
                value: 'Jon Snow'
            }],
            Sword: [{
                uri: '/attributes/Sword/$uri_1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        value: 'Longclaw',
                        uri: '$uri_2'
                    }],
                    Material: [{
                        type: '/attributes/Sword/attributes/Material',
                        value: 'Valyrian steel',
                        uri: '$uri_3'
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b, true);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Sword',
            newValue: [{
                uri: '/attributes/Sword/$uri_1',
                value: {
                    Name: [{
                        value: 'Longclaw',
                        uri: '$uri_2'
                    }],
                    Material: [{
                        value: 'Valyrian steel',
                        uri: '$uri_3'
                    }]
                }
            }]
        });
    });

    it('should keep uris if needed for existing nested attributes', function () {
        var a = {
            Sword: [{
                uri: '/attributes/Sword/uri_1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        value: 'Longclaw',
                        uri: '/attributes/Sword/uri_1/Name/uri_2'
                    }]
                }
            }]
        };
        var b = {
            Sword: [{
                uri: '/attributes/Sword/uri_1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        value: 'Longclaw',
                        uri: '/attributes/Sword/uri_1/Name/uri_2'
                    }],
                    Material: [{
                        type: '/attributes/Sword/attributes/Material',
                        value: 'Valyrian steel',
                        uri: '/attributes/Sword/uri_1/Material/$uri_3'
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b, true);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Sword/uri_1/Material',
            newValue: [{
                uri: '/attributes/Sword/uri_1/Material/$uri_3',
                value: 'Valyrian steel'
            }]
        });
    });

    it('should work correctly if one sub-attribute is null, but the second is not null', function() {
        var a = {
            ProfessionCode: [
                {
                    type: 'configuration/entityTypes/HCP/attributes/Profession/attributes/ProfessionCode',
                    ov: false,
                    value: null,
                    lookupError: '1003: RDM canonical value mapping not found in tenant [rTi8OUA6GWxvLPI]',
                    uri: 'entities/3gvulWE/attributes/Profession/i4E9962D/ProfessionCode/i4E99EYj'
                }
            ]
        };
        var b = {
            ProfessionCode: [
                {
                    type: 'configuration/entityTypes/HCP/attributes/Profession/attributes/ProfessionCode',
                    ov: false,
                    value: '7',
                    lookupError: '1003: RDM canonical value mapping not found in tenant [rTi8OUA6GWxvLPI]',
                    uri: 'entities/3gvulWE/attributes/Profession/i4E9962D/ProfessionCode/i4E99EYj'
                }
            ]
        };

        var result = AttributesDiff.compareAttributes(a, b, true);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: '_UPDATE_NON_OV_ATTRIBUTE',
            typeUri: 'entities/3gvulWE/attributes/Profession/i4E9962D/ProfessionCode',
            uri: 'entities/3gvulWE/attributes/Profession/i4E9962D/ProfessionCode/i4E99EYj',
            value: '7'
        });
    });


});
