module.exports = {
    initAttribute: {
        Address: [{
            label: '50 Charlton Ave E Hamilton ON L8N 4A6',
            relationshipLabel: 'Ship To',
            value: {
                'AddressLine1': [
                    {
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'ov': true,
                        'value': '50 Charlton Ave E',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WUNGSYsN'
                    }, {
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'ov': false,
                        'value': '50 Charleton Ave E',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WYNVZQR9'
                    }],
                'City': [
                    {
                        'type': 'configuration/entityTypes/Location/attributes/City',
                        'ov': true,
                        'value': 'Hamilton',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/City/WUNGLcOJ'
                    }],
                'StateProvince': [
                    {
                        'type': 'configuration/entityTypes/Location/attributes/StateProvince',
                        'ov': true,
                        'value': 'ON',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/StateProvince/WUNGLgeZ'
                    }],
                'Country': [
                    {
                        'type': 'configuration/entityTypes/Location/attributes/Country',
                        'ov': true,
                        'value': 'Canada',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/20UEtorKy'
                    }, {
                        'type': 'configuration/entityTypes/Location/attributes/Country',
                        'ov': false,
                        'value': 'CA',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/WUNGLkup'
                    }],
                'ISO3166-2': [{
                    'type': 'configuration/entityTypes/Location/attributes/ISO3166-2',
                    'ov': true,
                    'value': 'CA',
                    'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/ISO3166-2/WUNGLxhb'
                }],
                'Zip': [
                    {
                        'label': 'L8N 4A6-',
                        'value': {
                            'Zip5': [{
                                'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                                'ov': true,
                                'value': 'L8N 4A6',
                                'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/20UEtpkkK/Zip5/20UEtpp0a'
                            }]
                        },
                        'ov': true,
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/20UEtpkkK'
                    },
                    {
                        'label': '-',
                        'value': {
                            'PostalCode': [{
                                'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/PostalCode',
                                'ov': false,
                                'value': 'L8N 4A6',
                                'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5/PostalCode/WUNGLtRL'
                            }]
                        },
                        'ov': false,
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5'
                    }],
                'VerificationStatus': [{
                    'type': 'configuration/entityTypes/Location/attributes/VerificationStatus',
                    'ov': true,
                    'value': 'Verified',
                    'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/VerificationStatus/20UEtoAiO'
                }],
                'AddressType': [{
                    'type': 'configuration/relationTypes/HasAddress/attributes/AddressType',
                    'ov': true,
                    'value': 'Ship To',
                    'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'
                }]
            },
            ov: true,
            uri: 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
            refEntity: [{
                'type': 'configuration/entityTypes/Location',
                'crosswalks': [{
                    'uri': 'entities/1xkCLdTZ/crosswalks/FmiGI8x.WYNQvj19',
                    'type': 'configuration/sources/ECP-400',
                    'value': '125894123b96146259517339950aed8f',
                    'createDate': '2015-09-07T11:54:25.021Z',
                    'updateDate': '2015-09-07T11:54:25.021Z',
                    'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/City/WUNGLcOJ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5/PostalCode/WUNGLtRL', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WYNVZQR9', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/StateProvince/WUNGLgeZ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/WUNGLkup'],
                    'crosswalkExternalInfo': {}
                }, {
                    'uri': 'entities/1xkCLdTZ/crosswalks/FmiGI8x.20UEtlHhY',
                    'type': 'configuration/sources/ReltioCleanser',
                    'value': 'FmiGI8x',
                    'createDate': '2015-10-01T08:50:07.391Z',
                    'updateDate': '2015-10-01T08:50:07.391Z',
                    'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/VerificationStatus/20UEtoAiO', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/ISO3166-2/WUNGLxhb', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/City/WUNGLcOJ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WUNGSYsN', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/StateProvince/WUNGLgeZ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/20UEtpkkK/Zip5/20UEtpp0a', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/20UEtorKy', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/20UEtpkkK'],
                    'crosswalkExternalInfo': {}
                }, {
                    'uri': 'entities/1xkCLdTZ/crosswalks/FmiGI8x.WUNGKiyx',
                    'type': 'configuration/sources/REP-200',
                    'value': '125894123b96146259517339950aed8f',
                    'createDate': '2015-09-07T10:53:28.882Z',
                    'updateDate': '2015-09-07T10:53:28.882Z',
                    'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/ISO3166-2/WUNGLxhb', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/City/WUNGLcOJ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5/PostalCode/WUNGLtRL', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WUNGSYsN', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/StateProvince/WUNGLgeZ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/WUNGLkup'],
                    'crosswalkExternalInfo': {}
                }, {
                    'uri': 'entities/1xkCLdTZ/crosswalks/FmiGI8x.WfKJOCGp',
                    'type': 'configuration/sources/BDXGEN',
                    'value': '125894123b96146259517339950aed8f',
                    'createDate': '2015-09-07T13:58:40.232Z',
                    'updateDate': '2015-09-07T13:58:40.232Z',
                    'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/City/WUNGLcOJ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5/PostalCode/WUNGLtRL', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WUNGSYsN', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/StateProvince/WUNGLgeZ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/WUNGLkup'],
                    'crosswalkExternalInfo': {}
                }],
                'objectURI': 'entities/FmiGI8x',
                'label': '50 Charlton Ave E Hamilton ON L8N 4A6'
            }],
            refRelation: [{
                'type': 'configuration/relationTypes/HasAddress',
                'crosswalks': [{
                    'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
                    'type': 'configuration/sources/ECP-400',
                    'value': 'R|1001104040|1000|10|10|SH',
                    'createDate': '2015-09-07T11:54:25.021Z',
                    'updateDate': '2015-09-07T11:54:25.021Z',
                    'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                    'crosswalkExternalInfo': {}
                }, {
                    'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.20UEtNMRk',
                    'type': 'configuration/sources/BDXGEN',
                    'value': 'R|1001129717|1023|10|10|SH',
                    'createDate': '2015-09-30T08:03:50.371Z',
                    'updateDate': '2015-09-30T08:03:50.371Z',
                    'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                    'crosswalkExternalInfo': {}
                }, {
                    'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.20UEtNUyG',
                    'type': 'configuration/sources/BDXGEN',
                    'value': 'R|1001205497|1023|10|10|SH',
                    'createDate': '2015-09-30T08:10:03.112Z',
                    'updateDate': '2015-09-30T08:10:03.112Z',
                    'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                    'crosswalkExternalInfo': {}
                }, {
                    'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.20UEtNdUm',
                    'type': 'configuration/sources/BDXGEN',
                    'value': 'R|1001118652|1023|10|10|SH',
                    'createDate': '2015-09-30T08:01:48.074Z',
                    'updateDate': '2015-09-30T08:01:48.074Z',
                    'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                    'crosswalkExternalInfo': {}
                }],
                'startRefPinned': false,
                'endRefPinned': false,
                'startRefIgnored': false,
                'endRefIgnored': false,
                'objectURI': 'relations/7GoRYzT'
            }],
            'startObjectCrosswalks': [
                {
                    'uri': 'entities/1xkCLdTZ/crosswalks/20TbkZFn6',
                    'type': 'configuration/sources/BDXGEN',
                    'value': '1001118652|1023|10|10|SH',
                    'reltioLoadDate': '2015-09-30T08:01:48.074Z',
                    'createDate': '2015-09-25T13:46:45.135Z',
                    'updateDate': '2015-09-25T13:46:45.135Z',
                    'attributes': ['entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx', 'entities/1xkCLdTZ/attributes/Department/20Tbka4wC', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/Suc2fPnP', 'entities/1xkCLdTZ/attributes/Name1/Suc2fxtT', 'entities/1xkCLdTZ/attributes/CustomerNumber/20TbkZfMe', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/DataSource/20TbkZSZs', 'entities/1xkCLdTZ/attributes/CustomerClassification/1jfoJ5V1R'],
                    'singleAttributeUpdateDates': {}
                }, {
                    'uri': 'entities/1xkCLdTZ/crosswalks/20TbkbNv6',
                    'type': 'configuration/sources/BDXGEN',
                    'value': '1001129717|1023|10|10|SH',
                    'reltioLoadDate': '2015-09-30T08:03:50.371Z',
                    'createDate': '2015-09-25T13:46:45.758Z',
                    'updateDate': '2015-09-25T13:46:45.758Z',
                    'attributes': ['entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx', 'entities/1xkCLdTZ/attributes/Department/20Tbka4wC', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/Suc2fPnP', 'entities/1xkCLdTZ/attributes/Name1/Suc2fxtT', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/DataSource/20TbkZSZs', 'entities/1xkCLdTZ/attributes/CustomerClassification/1jfoJ5V1R', 'entities/1xkCLdTZ/attributes/CustomerNumber/20TbkbjEO'],
                    'singleAttributeUpdateDates': {}
                },
                {
                    'uri': 'entities/1xkCLdTZ/crosswalks/1jfoJ6Be1',
                    'type': 'configuration/sources/ECP-400',
                    'value': '1001104040|1000|10|10|SH',
                    'reltioLoadDate': '2015-09-07T11:54:25.021Z',
                    'createDate': '2015-09-03T20:10:55.225Z',
                    'updateDate': '2015-08-01T10:59:35.613Z',
                    'attributes': ['entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx', 'entities/1xkCLdTZ/attributes/Name1/1jfoJ67Nl', 'entities/1xkCLdTZ/attributes/Department/1jfoJ637V', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/CustomerNumber/1jfoJ5m4T', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/1jfoJ5yrF', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/CustomerClassification/1jfoJ5V1R', 'entities/1xkCLdTZ/attributes/DataSource/1jfoJ4k8b'],
                    'singleAttributeUpdateDates': {
                        'entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx': '2015-08-27T19:20:19.345Z',
                        'entities/1xkCLdTZ/attributes/DataSource/1jfoJ4k8b': '2015-09-03T20:10:55.225Z'
                    }
                },
                {
                    'uri': 'entities/1xkCLdTZ/crosswalks/20TbkaHiy',
                    'type': 'configuration/sources/BDXGEN',
                    'value': '1001205497|1023|10|10|SH',
                    'reltioLoadDate': '2015-09-30T08:10:03.112Z',
                    'createDate': '2015-09-25T13:46:47.253Z',
                    'updateDate': '2015-09-25T13:46:47.253Z',
                    'attributes': ['entities/1xkCLdTZ/attributes/CustomerClassification/20Tbkb2bo', 'entities/1xkCLdTZ/attributes/Name/20TbkaQFU', 'entities/1xkCLdTZ/attributes/Department/20Tbka4wC', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/Suc2fPnP', 'entities/1xkCLdTZ/attributes/CustomerNumber/20TbkahIW', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/DataSource/20TbkZSZs', 'entities/1xkCLdTZ/attributes/Name1/20TbkbJeq'],
                    'singleAttributeUpdateDates': {}
                }]
        }]
    }
    ,
    modifiedRelationAttribute: {
        Address: [
            {
                'label': '50 Charlton Ave E Hamilton ON L8N 4A6',
                'relationshipLabel': 'Ship To',
                'value': {
                    'AddressLine1': [{
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'ov': true,
                        'value': '50 Charlton Ave E',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WUNGSYsN'
                    }, {
                        'type': 'configuration/entityTypes/Location/attributes/AddressLine1',
                        'ov': false,
                        'value': '50 Charleton Ave E',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WYNVZQR9'
                    }],
                    'City': [{
                        'type': 'configuration/entityTypes/Location/attributes/City',
                        'ov': true,
                        'value': 'Hamilton',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/City/WUNGLcOJ'
                    }],
                    'StateProvince': [{
                        'type': 'configuration/entityTypes/Location/attributes/StateProvince',
                        'ov': true,
                        'value': 'ON',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/StateProvince/WUNGLgeZ'
                    }],
                    'Country': [{
                        'type': 'configuration/entityTypes/Location/attributes/Country',
                        'ov': true,
                        'value': 'Canada',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/20UEtorKy'
                    }, {
                        'type': 'configuration/entityTypes/Location/attributes/Country',
                        'ov': false,
                        'value': 'CA',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/WUNGLkup'
                    }],
                    'ISO3166-2': [{
                        'type': 'configuration/entityTypes/Location/attributes/ISO3166-2',
                        'ov': true,
                        'value': 'CA',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/ISO3166-2/WUNGLxhb'
                    }],
                    'Zip': [{
                        'label': 'L8N 4A6-',
                        'value': {
                            'Zip5': [{
                                'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/Zip5',
                                'ov': true,
                                'value': 'L8N 4A6',
                                'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/20UEtpkkK/Zip5/20UEtpp0a'
                            }]
                        },
                        'ov': true,
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/20UEtpkkK'
                    }, {
                        'label': '-',
                        'value': {
                            'PostalCode': [{
                                'type': 'configuration/entityTypes/Location/attributes/Zip/attributes/PostalCode',
                                'ov': false,
                                'value': 'L8N 4A6',
                                'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5/PostalCode/WUNGLtRL'
                            }]
                        },
                        'ov': false,
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5'
                    }],
                    'VerificationStatus': [{
                        'type': 'configuration/entityTypes/Location/attributes/VerificationStatus',
                        'ov': true,
                        'value': 'Verified',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/VerificationStatus/20UEtoAiO'
                    }],
                    'AddressType': [{
                        'type': 'configuration/relationTypes/HasAddress/attributes/AddressType',
                        'ov': true,
                        'value': 'Mailing (Mailing_vod__c)',
                        'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn',
                        'crosswalks': [{
                            'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
                            'type': 'configuration/sources/ECP-400',
                            'value': 'R|1001104040|1000|10|10|SH',
                            'createDate': '2015-09-07T11:54:25.021Z',
                            'updateDate': '2015-09-07T11:54:25.021Z',
                            'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                            'crosswalkExternalInfo': {}
                        }, {
                            'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.20UEtNMRk',
                            'type': 'configuration/sources/BDXGEN',
                            'value': 'R|1001129717|1023|10|10|SH',
                            'createDate': '2015-09-30T08:03:50.371Z',
                            'updateDate': '2015-09-30T08:03:50.371Z',
                            'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                            'crosswalkExternalInfo': {}
                        }, {
                            'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.20UEtNUyG',
                            'type': 'configuration/sources/BDXGEN',
                            'value': 'R|1001205497|1023|10|10|SH',
                            'createDate': '2015-09-30T08:10:03.112Z',
                            'updateDate': '2015-09-30T08:10:03.112Z',
                            'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                            'crosswalkExternalInfo': {}
                        }, {
                            'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.20UEtNdUm',
                            'type': 'configuration/sources/BDXGEN',
                            'value': 'R|1001118652|1023|10|10|SH',
                            'createDate': '2015-09-30T08:01:48.074Z',
                            'updateDate': '2015-09-30T08:01:48.074Z',
                            'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                            'crosswalkExternalInfo': {}
                        }]
                    }]
                },
                'ov': true,
                'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
                'refEntity': [{
                    'type': 'configuration/entityTypes/Location',
                    'crosswalks': [{
                        'uri': 'entities/1xkCLdTZ/crosswalks/FmiGI8x.WYNQvj19',
                        'type': 'configuration/sources/ECP-400',
                        'value': '125894123b96146259517339950aed8f',
                        'createDate': '2015-09-07T11:54:25.021Z',
                        'updateDate': '2015-09-07T11:54:25.021Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/City/WUNGLcOJ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5/PostalCode/WUNGLtRL', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WYNVZQR9', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/StateProvince/WUNGLgeZ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/WUNGLkup'],
                        'crosswalkExternalInfo': {}
                    }, {
                        'uri': 'entities/1xkCLdTZ/crosswalks/FmiGI8x.20UEtlHhY',
                        'type': 'configuration/sources/ReltioCleanser',
                        'value': 'FmiGI8x',
                        'createDate': '2015-10-01T08:50:07.391Z',
                        'updateDate': '2015-10-01T08:50:07.391Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/VerificationStatus/20UEtoAiO', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/ISO3166-2/WUNGLxhb', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/City/WUNGLcOJ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WUNGSYsN', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/StateProvince/WUNGLgeZ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/20UEtpkkK/Zip5/20UEtpp0a', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/20UEtorKy', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/20UEtpkkK'],
                        'crosswalkExternalInfo': {}
                    }, {
                        'uri': 'entities/1xkCLdTZ/crosswalks/FmiGI8x.WUNGKiyx',
                        'type': 'configuration/sources/REP-200',
                        'value': '125894123b96146259517339950aed8f',
                        'createDate': '2015-09-07T10:53:28.882Z',
                        'updateDate': '2015-09-07T10:53:28.882Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/ISO3166-2/WUNGLxhb', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/City/WUNGLcOJ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5/PostalCode/WUNGLtRL', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WUNGSYsN', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/StateProvince/WUNGLgeZ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/WUNGLkup'],
                        'crosswalkExternalInfo': {}
                    }, {
                        'uri': 'entities/1xkCLdTZ/crosswalks/FmiGI8x.WfKJOCGp',
                        'type': 'configuration/sources/BDXGEN',
                        'value': '125894123b96146259517339950aed8f',
                        'createDate': '2015-09-07T13:58:40.232Z',
                        'updateDate': '2015-09-07T13:58:40.232Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/City/WUNGLcOJ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Zip/WUNGLpB5/PostalCode/WUNGLtRL', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressLine1/WUNGSYsN', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/StateProvince/WUNGLgeZ', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/Country/WUNGLkup'],
                        'crosswalkExternalInfo': {}
                    }],
                    'objectURI': 'entities/FmiGI8x'
                }],
                'refRelation': [{
                    'type': 'configuration/relationTypes/HasAddress',
                    'crosswalks': [{
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.WYNQvnHP',
                        'type': 'configuration/sources/ECP-400',
                        'value': 'R|1001104040|1000|10|10|SH',
                        'createDate': '2015-09-07T11:54:25.021Z',
                        'updateDate': '2015-09-07T11:54:25.021Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                        'crosswalkExternalInfo': {}
                    }, {
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.20UEtNMRk',
                        'type': 'configuration/sources/BDXGEN',
                        'value': 'R|1001129717|1023|10|10|SH',
                        'createDate': '2015-09-30T08:03:50.371Z',
                        'updateDate': '2015-09-30T08:03:50.371Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                        'crosswalkExternalInfo': {}
                    }, {
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.20UEtNUyG',
                        'type': 'configuration/sources/BDXGEN',
                        'value': 'R|1001205497|1023|10|10|SH',
                        'createDate': '2015-09-30T08:10:03.112Z',
                        'updateDate': '2015-09-30T08:10:03.112Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                        'crosswalkExternalInfo': {}
                    }, {
                        'uri': 'entities/1xkCLdTZ/crosswalks/7GoRYzT.20UEtNdUm',
                        'type': 'configuration/sources/BDXGEN',
                        'value': 'R|1001118652|1023|10|10|SH',
                        'createDate': '2015-09-30T08:01:48.074Z',
                        'updateDate': '2015-09-30T08:01:48.074Z',
                        'attributeURIs': ['entities/1xkCLdTZ/attributes/Address/7GoRYzT', 'entities/1xkCLdTZ/attributes/Address/7GoRYzT/AddressType/WYNQwxjn'],
                        'crosswalkExternalInfo': {}
                    }],
                    'startRefPinned': false,
                    'endRefPinned': false,
                    'startRefIgnored': false,
                    'endRefIgnored': false,
                    'objectURI': 'relations/7GoRYzT'
                }],
                'startObjectCrosswalks': [{
                    'uri': 'entities/1xkCLdTZ/crosswalks/20TbkZFn6',
                    'type': 'configuration/sources/BDXGEN',
                    'value': '1001118652|1023|10|10|SH',
                    'reltioLoadDate': '2015-09-30T08:01:48.074Z',
                    'createDate': '2015-09-25T13:46:45.135Z',
                    'updateDate': '2015-09-25T13:46:45.135Z',
                    'attributes': ['entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx', 'entities/1xkCLdTZ/attributes/Department/20Tbka4wC', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/Suc2fPnP', 'entities/1xkCLdTZ/attributes/Name1/Suc2fxtT', 'entities/1xkCLdTZ/attributes/CustomerNumber/20TbkZfMe', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/DataSource/20TbkZSZs', 'entities/1xkCLdTZ/attributes/CustomerClassification/1jfoJ5V1R'],
                    'singleAttributeUpdateDates': {}
                }, {
                    'uri': 'entities/1xkCLdTZ/crosswalks/20TbkbNv6',
                    'type': 'configuration/sources/BDXGEN',
                    'value': '1001129717|1023|10|10|SH',
                    'reltioLoadDate': '2015-09-30T08:03:50.371Z',
                    'createDate': '2015-09-25T13:46:45.758Z',
                    'updateDate': '2015-09-25T13:46:45.758Z',
                    'attributes': ['entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx', 'entities/1xkCLdTZ/attributes/Department/20Tbka4wC', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/Suc2fPnP', 'entities/1xkCLdTZ/attributes/Name1/Suc2fxtT', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/DataSource/20TbkZSZs', 'entities/1xkCLdTZ/attributes/CustomerClassification/1jfoJ5V1R', 'entities/1xkCLdTZ/attributes/CustomerNumber/20TbkbjEO'],
                    'singleAttributeUpdateDates': {}
                }, {
                    'uri': 'entities/1xkCLdTZ/crosswalks/1jfoJ6Be1',
                    'type': 'configuration/sources/ECP-400',
                    'value': '1001104040|1000|10|10|SH',
                    'reltioLoadDate': '2015-09-07T11:54:25.021Z',
                    'createDate': '2015-09-03T20:10:55.225Z',
                    'updateDate': '2015-08-01T10:59:35.613Z',
                    'attributes': ['entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx', 'entities/1xkCLdTZ/attributes/Name1/1jfoJ67Nl', 'entities/1xkCLdTZ/attributes/Department/1jfoJ637V', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/CustomerNumber/1jfoJ5m4T', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/1jfoJ5yrF', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/CustomerClassification/1jfoJ5V1R', 'entities/1xkCLdTZ/attributes/DataSource/1jfoJ4k8b'],
                    'singleAttributeUpdateDates': {
                        'entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx': '2015-08-27T19:20:19.345Z',
                        'entities/1xkCLdTZ/attributes/DataSource/1jfoJ4k8b': '2015-09-03T20:10:55.225Z'
                    }
                }, {
                    'uri': 'entities/1xkCLdTZ/crosswalks/20TbkaHiy',
                    'type': 'configuration/sources/BDXGEN',
                    'value': '1001205497|1023|10|10|SH',
                    'reltioLoadDate': '2015-09-30T08:10:03.112Z',
                    'createDate': '2015-09-25T13:46:47.253Z',
                    'updateDate': '2015-09-25T13:46:47.253Z',
                    'attributes': ['entities/1xkCLdTZ/attributes/CustomerClassification/20Tbkb2bo', 'entities/1xkCLdTZ/attributes/Name/20TbkaQFU', 'entities/1xkCLdTZ/attributes/Department/20Tbka4wC', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/Suc2fPnP', 'entities/1xkCLdTZ/attributes/CustomerNumber/20TbkahIW', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/DataSource/20TbkZSZs', 'entities/1xkCLdTZ/attributes/Name1/20TbkbJeq'],
                    'singleAttributeUpdateDates': {}
                }]
            }
        ]
    },
    modifiedEntity: {
        Address: [{
            'label': '50 Charlton Ave E Hamilton ON L8N 4A6',
            'relationshipLabel': 'Ship To',
            'value': {},
            'ov': true,
            'uri': 'entities/1xkCLdTZ/attributes/Address/7GoRYzT',
            'refEntity': [{
                'objectURI': 'entities/1yR9SWrt',
                'crosswalks': [{'type': 'configuration/sources/Reltio'}]
            }],
            'refRelation': [{'crosswalks': [{'type': 'configuration/sources/Reltio'}]}],
            'startObjectCrosswalks': [{
                'uri': 'entities/1xkCLdTZ/crosswalks/20TbkZFn6',
                'type': 'configuration/sources/BDXGEN',
                'value': '1001118652|1023|10|10|SH',
                'reltioLoadDate': '2015-09-30T08:01:48.074Z',
                'createDate': '2015-09-25T13:46:45.135Z',
                'updateDate': '2015-09-25T13:46:45.135Z',
                'attributes': ['entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx', 'entities/1xkCLdTZ/attributes/Department/20Tbka4wC', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/Suc2fPnP', 'entities/1xkCLdTZ/attributes/Name1/Suc2fxtT', 'entities/1xkCLdTZ/attributes/CustomerNumber/20TbkZfMe', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/DataSource/20TbkZSZs', 'entities/1xkCLdTZ/attributes/CustomerClassification/1jfoJ5V1R'],
                'singleAttributeUpdateDates': {}
            }, {
                'uri': 'entities/1xkCLdTZ/crosswalks/20TbkbNv6',
                'type': 'configuration/sources/BDXGEN',
                'value': '1001129717|1023|10|10|SH',
                'reltioLoadDate': '2015-09-30T08:03:50.371Z',
                'createDate': '2015-09-25T13:46:45.758Z',
                'updateDate': '2015-09-25T13:46:45.758Z',
                'attributes': ['entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx', 'entities/1xkCLdTZ/attributes/Department/20Tbka4wC', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/Suc2fPnP', 'entities/1xkCLdTZ/attributes/Name1/Suc2fxtT', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/DataSource/20TbkZSZs', 'entities/1xkCLdTZ/attributes/CustomerClassification/1jfoJ5V1R', 'entities/1xkCLdTZ/attributes/CustomerNumber/20TbkbjEO'],
                'singleAttributeUpdateDates': {}
            }, {
                'uri': 'entities/1xkCLdTZ/crosswalks/1jfoJ6Be1',
                'type': 'configuration/sources/ECP-400',
                'value': '1001104040|1000|10|10|SH',
                'reltioLoadDate': '2015-09-07T11:54:25.021Z',
                'createDate': '2015-09-03T20:10:55.225Z',
                'updateDate': '2015-08-01T10:59:35.613Z',
                'attributes': ['entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx', 'entities/1xkCLdTZ/attributes/Name1/1jfoJ67Nl', 'entities/1xkCLdTZ/attributes/Department/1jfoJ637V', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/CustomerNumber/1jfoJ5m4T', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/1jfoJ5yrF', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/CustomerClassification/1jfoJ5V1R', 'entities/1xkCLdTZ/attributes/DataSource/1jfoJ4k8b'],
                'singleAttributeUpdateDates': {
                    'entities/1xkCLdTZ/attributes/Name/1jfoJ5dXx': '2015-08-27T19:20:19.345Z',
                    'entities/1xkCLdTZ/attributes/DataSource/1jfoJ4k8b': '2015-09-03T20:10:55.225Z'
                }
            }, {
                'uri': 'entities/1xkCLdTZ/crosswalks/20TbkaHiy',
                'type': 'configuration/sources/BDXGEN',
                'value': '1001205497|1023|10|10|SH',
                'reltioLoadDate': '2015-09-30T08:10:03.112Z',
                'createDate': '2015-09-25T13:46:47.253Z',
                'updateDate': '2015-09-25T13:46:47.253Z',
                'attributes': ['entities/1xkCLdTZ/attributes/CustomerClassification/20Tbkb2bo', 'entities/1xkCLdTZ/attributes/Name/20TbkaQFU', 'entities/1xkCLdTZ/attributes/Department/20Tbka4wC', 'entities/1xkCLdTZ/attributes/DistributionChannel/1jfoJ5qKj', 'entities/1xkCLdTZ/attributes/AccountGroup/1jfoJ5hoD', 'entities/1xkCLdTZ/attributes/SalesOrg/Suc2fPnP', 'entities/1xkCLdTZ/attributes/CustomerNumber/20TbkahIW', 'entities/1xkCLdTZ/attributes/Division/1jfoJ5uaz', 'entities/1xkCLdTZ/attributes/PartnerFunction/1jfoJ5ZHh', 'entities/1xkCLdTZ/attributes/DataSource/20TbkZSZs', 'entities/1xkCLdTZ/attributes/Name1/20TbkbJeq'],
                'singleAttributeUpdateDates': {}
            }]
        }]
    }
};