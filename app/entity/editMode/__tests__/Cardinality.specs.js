jest.unmock('../AttributesDiff');
jest.unmock('../AttributesEditor');
var AttributesDiff = require('../AttributesDiff');
var AttributesEditor = require('../AttributesEditor');

describe('CardinalityIntegrationTests', function () {

    it('should IGNORE non-Reltio ov=true value and UPDATE Reltio ov=false value for attribute with 1..1 cardinality', function () {
        // ARRANGE

        var attributes = {
            Name: [
                {
                    ov: true,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/1',
                    value: 'Jon Snow from WB'
                }, {
                    ov: false,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/2',
                    value: 'Jon Snow from WB 1'
                }, {
                    ov: false,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/3',
                    value: 'Jon Snow from Reltio'
                }, {
                    ov: false,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/4',
                    value: 'Jon Snow from Reltio 1'
                }
            ]
        };

        var originalAttributes = JSON.parse(JSON.stringify(attributes));

        var crosswalks = [{
            type: 'configuration/sources/WB',
            value: '1',
            attributes: [
                '/attributes/Name/1',
                '/attributes/Name/2'
            ]
        }, {
            type: 'configuration/sources/Reltio',
            value: '2',
            attributes: [
                '/attributes/Name/3',
                '/attributes/Name/4'
            ]
        }];

        var attrType = {
            name: 'Name',
            uri: '/attributes/Name',
            cardinality: {
                minValue: 1,
                maxValue: 1
            }
        };

        // ACT

        AttributesEditor.editAttribute(
            attributes,
            attrType,
            '/attributes/Name/1',
            'Jon Snow');

        var diff = AttributesDiff.compareAttributes(originalAttributes, attributes);
        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        // ASSERT

        expect(diff.length).toBe(1);
        expect(diff).toContainEqual({
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/Name/3',
            newValue: {
                value: 'Jon Snow'
            },
            crosswalk: {
                type: 'configuration/sources/Reltio',
                value: '2'
            }
        });
    });

    it('should just edit (IGNORE+INSERT) non-Reltio ov=true value for attribute with 1..2 cardinality', function () {
        // ARRANGE

        var attributes = {
            Name: [
                {
                    ov: true,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/1',
                    value: 'Jon Snow from WB'
                }, {
                    ov: false,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/2',
                    value: 'Jon Snow from WB 1'
                }, {
                    ov: false,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/3',
                    value: 'Jon Snow from Reltio'
                }, {
                    ov: false,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/4',
                    value: 'Jon Snow from Reltio 1'
                }
            ]
        };

        var originalAttributes = JSON.parse(JSON.stringify(attributes));

        var crosswalks = [{
            type: 'configuration/sources/WB',
            value: '1',
            attributes: [
                '/attributes/Name/1',
                '/attributes/Name/2'
            ]
        }, {
            type: 'configuration/sources/Reltio',
            value: '2',
            attributes: [
                '/attributes/Name/3',
                '/attributes/Name/4'
            ]
        }];

        var attrType = {
            name: 'Name',
            uri: '/attributes/Name',
            cardinality: {
                minValue: 1,
                maxValue: 2
            }
        };

        // ACT

        AttributesEditor.editAttribute(
            attributes,
            attrType,
            '/attributes/Name/1',
            'Jon Snow');

        var diff = AttributesDiff.compareAttributes(originalAttributes, attributes);
        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        // ASSERT

        expect(diff.length).toBe(2);
        expect(diff).toContainEqual({
            type: 'INSERT_ATTRIBUTE',
            uri: '/attributes/Name',
            newValue: [{
                value: 'Jon Snow'
            }]
        });
        expect(diff).toContainEqual({
            type: 'IGNORE_ATTRIBUTE',
            uri: '/attributes/Name/1',
            newValue: {
                value: true
            }
        });
    });

    it('should just edit Reltio ov=true value for attribute with 1..1 cardinality', function () {
        // ARRANGE

        var attributes = {
            Name: [
                {
                    ov: true,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/1',
                    value: 'Jon Snow from Reltio'
                }, {
                    ov: false,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/2',
                    value: 'Jon Snow from WB'
                }, {
                    ov: false,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/3',
                    value: 'Jon Snow from WB 1'
                }, {
                    ov: false,
                    type: '/attributes/Name',
                    uri: '/attributes/Name/4',
                    value: 'Jon Snow from Reltio 1'
                }
            ]
        };

        var originalAttributes = JSON.parse(JSON.stringify(attributes));

        var crosswalks = [{
            type: 'configuration/sources/WB',
            value: '1',
            attributes: [
                '/attributes/Name/2',
                '/attributes/Name/3'
            ]
        }, {
            type: 'configuration/sources/Reltio',
            value: '2',
            attributes: [
                '/attributes/Name/1',
                '/attributes/Name/4'
            ]
        }];

        var attrType = {
            name: 'Name',
            uri: '/attributes/Name',
            cardinality: {
                minValue: 1,
                maxValue: 1
            }
        };

        // ACT

        AttributesEditor.editAttribute(
            attributes,
            attrType,
            '/attributes/Name/1',
            'Jon Snow');

        var diff = AttributesDiff.compareAttributes(originalAttributes, attributes);
        diff = AttributesDiff.applyCrosswalks(diff, crosswalks);

        // ASSERT

        expect(diff.length).toBe(1);
        expect(diff).toContainEqual({
            type: 'UPDATE_ATTRIBUTE',
            uri: '/attributes/Name/1',
            newValue: {
                value: 'Jon Snow'
            },
            crosswalk: {
                type: 'configuration/sources/Reltio',
                value: '2'
            }
        });
    });

    it('should edit ov=false lookup code for attribute with 1..1 cardinality', function () {
        // ARRANGE

        var attributes = {
            CountryCode: [{
                type: '/attributes/CountryCode',
                uri: '/attributes/CountryCode/1',
                value: 'Meereen',
                lookupCode: 'ME',
                ov: true
            }, {
                type: '/attributes/CountryCode',
                uri: '/attributes/CountryCode/2',
                value: 'Westeros',
                lookupCode: 'WS',
                ov: false
            }]
        };

        var attrType = {
            name: 'CountryCode',
            uri: '/attributes/CountryCode',
            cardinality: {
                minValue: 1,
                maxValue: 1
            }
        };

        // ACT

        AttributesEditor.editAttribute(
            attributes,
            attrType,
            '/attributes/CountryCode/1',
            'BR');

        // ASSERT

        expect(attributes.CountryCode[0].lookupCode).toBe('BR');
        expect(attributes.CountryCode[1].lookupCode).toBe('BR');
    });

});
