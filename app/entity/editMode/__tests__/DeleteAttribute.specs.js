jest.unmock('../AttributesDiff');
var AttributesDiff = require('../AttributesDiff');
describe('compareAttributes', function () {

    it('can detect DELETE_ATTRIBUTE for simple attributes', function () {
        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }]
        };
        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/1'
        });
    });

    it('can detect DELETE_ATTRIBUTE for simple attributes with more than 1 value', function () {
        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }, {
                type: '/attributes/LastName',
                uri: '/attributes/LastName/2',
                value: 'Stark'
            }, {
                type: '/attributes/LastName',
                uri: '/attributes/LastName/3',
                value: 'Targaryen'
            }]
        };
        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(2);
        expect(result[0]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/2'
        });
        expect(result[1]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/LastName/3'
        });
    });

    it('can detect DELETE_ATTRIBUTE for attribute inside nested attributes', function () {
        var a = {
            Sword: [{
                uri: '/attributes/Sword/1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        uri: '/attributes/Sword/1/attributes/Name/1',
                        value: 'Longclaw'
                    }],
                    Material: [{
                        type: '/attributes/Sword/attributes/Material',
                        uri: '/attributes/Sword/1/attributes/Material/1',
                        value: 'Valyrian steel'
                    }]
                }
            }]
        };
        var b = {
            Sword: [{
                uri: '/attributes/Sword/1',
                value: {
                    Name: [{
                        type: '/attributes/Sword/attributes/Name',
                        uri: '/attributes/Sword/1/attributes/Name/1',
                        value: 'Longclaw'
                    }]
                }
            }]
        };

        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'DELETE_ATTRIBUTE',
            uri: '/attributes/Sword/1/attributes/Material/1'
        });
    });


    it('delete image using UPDATE_PROFILE_PIC_BY_URI', function () {
        var a = {
            defaultProfilePic : 'entities/uri/attributes/ImageLinks/1'
        };
        var b = {};

        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_PROFILE_PIC_BY_URI',
            newValue: {
                value: undefined
            }
        });
    });

    it('delete image using UPDATE_PROFILE_PIC_BY_URL', function () {
        var a = {
            defaultProfilePicValue : 'http://url.com/image1.png'
        };
        var b = {
        };

        var result = AttributesDiff.compareImages(a, b);

        expect(result.length).toBe(1);
        expect(result[0]).toEqual({
            type: 'UPDATE_PROFILE_PIC_BY_URL',
            newValue: {
                value: undefined
            }
        });
    });

    it('should not detect DELETE_ATTRIBUTE for empty value', function () {
        var a = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: ''
            }]
        };

        var b = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: ''
            }]
        };
        var result = AttributesDiff.compareAttributes(a, b);

        expect(result.length).toBe(0);
    });
});
