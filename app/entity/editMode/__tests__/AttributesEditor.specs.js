jest.unmock('../AttributesEditor');
var AttributesEditor = require('../AttributesEditor');
describe('AttributesEditor', function () {

    describe('editAttribute', function () {

        it('can edit simple attribute', function () {
            var attributes = {
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                LastName: [{
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/1',
                    value: 'Snow'
                }]
            };

            AttributesEditor.editAttribute(
                attributes,
                {name: 'LastName'},
                '/attributes/LastName/1',
                'Stark');

            expect(attributes).toEqual({
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                LastName: [{
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/1',
                    value: 'Stark'
                }]
            });
        });

        it('should completely update reference attribute', function () {
            var attributes = {
                Address: [{
                    refEntity: [{
                        objectURI: 'entities/2EpaVt0k',
                        type: 'configuration/sources/Reltio'
                    }],
                    refRelation: [{
                        type: 'configuration/sources/Reltio'
                    }],
                    uri: '/attributes/Address/1',
                    value: {
                        AddressLine1: [{
                            type: '/attributes/Address/attributes/AddressLine1',
                            uri: '/attributes/Address/1/AddressLine1/2',
                            value: 'Winterfell'
                        }]
                    }
                }]
            };
            var value = {
                value: {},
                label: 'label',
                refRelation: [{
                    type: 'configuration/sources/Reltio'
                }],
                refEntity: {
                    objectURI: 'entities/uri$1460992109168',
                    crosswalks: [
                        {
                            type: 'configuration/sources/Reltio'
                        }
                    ]
                }
            };
            AttributesEditor.editAttribute(
                attributes,
                {name: 'Address'},
                '/attributes/Address/1',
                value);

            expect(attributes).toEqual({
                Address: [{
                    value: {},
                    label: 'label',
                    uri: '/attributes/Address/1',
                    refRelation: [{
                        type: 'configuration/sources/Reltio'
                    }],
                    refEntity: {
                        objectURI: 'entities/uri$1460992109168',
                        crosswalks: [
                            {
                                type: 'configuration/sources/Reltio'
                            }
                        ]
                    }
                }]
            });
        });

        it('should add attribute if it\'s not exists', function () {
            var attributes = {
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }]
            };

            AttributesEditor.editAttribute(
                attributes,
                {name: 'LastName', uri: '/attributes/LastName'},
                '/attributes/LastName/$uri_123',
                'Stark');

            expect(attributes).toEqual({
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                LastName: [{
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/$uri_123',
                    value: 'Stark'
                }]
            });
        });

        it('should add nested attribute if it\'s not exists', function () {
            var attributes = {};

            AttributesEditor.editAttribute(
                attributes,
                {name: 'Material', uri: '/attributes/Sword/attributes/Material'},
                'entity-uri/attributes/Sword/$uri_123/Material/$uri_456',
                'Valyrian steel');

            expect(attributes.Sword[0].uri).toBe('entity-uri/attributes/Sword/$uri_123');
            expect(attributes.Sword[0].value.Material[0].uri).toBe('entity-uri/attributes/Sword/$uri_123/Material/$uri_456');
            expect(attributes.Sword[0].value.Material[0].value).toBe('Valyrian steel');
        });

        it('can operate with 2 or more levels of nesting', function () {
            var attributes = {};

            AttributesEditor.editAttribute(
                attributes,
                {name: 'Type', uri: '/attributes/Sword/attributes/Material/attributes/Type'},
                'entity-uri/attributes/Sword/$uri_123/Material/$uri_456/Type/$uri_789',
                'Valyrian steel');

            expect(attributes.Sword[0].value.Material[0].value.Type[0].uri).toBe('entity-uri/attributes/Sword/$uri_123/Material/$uri_456/Type/$uri_789');
            expect(attributes.Sword[0].value.Material[0].value.Type[0].value).toBe('Valyrian steel');

            expect(attributes.Sword[0].value.Material[0].value.Type[0].type).toBe('/attributes/Sword/attributes/Material/attributes/Type');
            expect(attributes.Sword[0].value.Material[0].type).toBe('/attributes/Sword/attributes/Material');
            expect(attributes.Sword[0].type).toBe('/attributes/Sword');
        });

        it('can edit simple attribute inside nested', function () {
            var attributes = {
                Sword: [{
                    uri: '/attributes/Sword/1',
                    value: {
                        Name: [{
                            type: '/attributes/Sword/attributes/Name',
                            uri: '/attributes/Sword/1/Name/2',
                            value: 'Longclaw'
                        }],
                        Material: [{
                            type: '/attributes/Sword/attributes/Material',
                            uri: '/attributes/Sword/1/Material/3',
                            value: 'Steel'
                        }]
                    }
                }]
            };

            AttributesEditor.editAttribute(
                attributes,
                {name: 'Material', uri: '/attributes/Sword/attributes/Material'},
                '/attributes/Sword/1/Material/3',
                'Valyrian steel');

            expect(attributes.Sword[0].value.Material[0].value).toBe('Valyrian steel');
        });

        it('should apply crosswalks to reference attributes', function () {
            var attributes = {
                Address: [{
                    uri: '/attributes/Address/1',
                    value: {
                        AddressLine1: [{
                            type: '/attributes/Address/attributes/AddressLine1',
                            uri: '/attributes/Address/1/AddressLine1/2',
                            value: 'Winterfell'
                        }]
                    }
                }]
            };

            var crosswalks = [{
                type: 'configuration/sources/Reltio',
                value: '42',
                attributeURIs: [
                    '/attributes/Address/1/AddressLine1/2',
                    '/attributes/Address/1']
            }];

            AttributesEditor.editAttribute(
                attributes,
                {name: 'AddressLine1', uri: '/attributes/Address/attributes/AddressLine1'},
                '/attributes/Address/1/AddressLine1/2',
                'The Wall',
                crosswalks);

            expect(attributes.Address[0].value.AddressLine1[0].value).toBe('The Wall');
            expect(attributes.Address[0].value.AddressLine1[0].crosswalks).toEqual(crosswalks);
        });

        it('should change lookup code', function () {
            var attributes = {
                CountryCode: [{
                    type: '/attributes/CountryCode',
                    uri: '/attributes/CountryCode/1',
                    value: 'Westeros',
                    lookupCode: 'WS'
                }]
            };

            AttributesEditor.editAttribute(
                attributes,
                {name: 'CountryCode', uri: '/attributes/CountryCode'},
                '/attributes/CountryCode/1',
                'ME');

            expect(attributes.CountryCode[0].value).toBe('ME');
            expect(attributes.CountryCode[0].lookupCode).toBe('ME');
        });

        it('should change lookup code and value (if passed)', function () {
            var attributes = {
                CountryCode: [{
                    type: '/attributes/CountryCode',
                    uri: '/attributes/CountryCode/1',
                    value: 'Westeros',
                    lookupCode: 'WS'
                }]
            };

            AttributesEditor.editAttribute(
                attributes,
                {name: 'CountryCode', uri: '/attributes/CountryCode'},
                '/attributes/CountryCode/1',
                {value: 'Meereen', lookupCode: 'ME'});

            expect(attributes.CountryCode[0].value).toBe('Meereen');
            expect(attributes.CountryCode[0].lookupCode).toBe('ME');
        });

        it('should apply null value correctly', function () {
            var attributes = {
                Bool: [{
                    type: '/attributes/Bool',
                    uri: '/attributes/Bool/1',
                    value: 'true'
                }]
            };

            AttributesEditor.editAttribute(
                attributes,
                {name: 'Bool', uri: '/attributes/Bool'},
                '/attributes/Bool/1',
                null);

            expect(attributes.Bool[0].value).toBe(null);
        });
    });

    describe('removeAttribute', function () {

        it('can remove simple attribute', function () {
            var attributes = {
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                LastName: [{
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/1',
                    value: 'Snow'
                }]
            };

            AttributesEditor.removeAttribute(
                attributes,
                {name: 'LastName'},
                '/attributes/LastName/1');

            expect(attributes).toEqual({
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }]
            });
        });

        it('can remove whole nested attribute', function () {
            var attributes = {
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                Sword: [{
                    uri: '/attributes/Sword/1',
                    value: {
                        Name: [{
                            type: '/attributes/Sword/attributes/Name',
                            uri: '/attributes/Sword/1/Name/2',
                            value: 'Longclaw'
                        }],
                        Material: [{
                            type: '/attributes/Sword/attributes/Material',
                            uri: '/attributes/Sword/1/Material/3',
                            value: 'Steel'
                        }]
                    }
                }]
            };

            AttributesEditor.removeAttribute(
                attributes,
                {name: 'Sword'},
                '/attributes/Sword/1');

            expect(attributes).toEqual({
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }]
            });
        });

        it('can remove simple attribute inside nested', function () {
            var attributes = {
                Sword: [{
                    uri: '/attributes/Sword/1',
                    value: {
                        Name: [{
                            type: '/attributes/Sword/attributes/Name',
                            uri: '/attributes/Sword/1/Name/2',
                            value: 'Longclaw'
                        }],
                        Material: [{
                            type: '/attributes/Sword/attributes/Material',
                            uri: '/attributes/Sword/1/Material/3',
                            value: 'Steel'
                        }]
                    }
                }]
            };

            AttributesEditor.removeAttribute(
                attributes,
                {name: 'Material'},
                '/attributes/Sword/1/Material/3');

            expect(attributes).toEqual({
                Sword: [{
                    uri: '/attributes/Sword/1',
                    value: {
                        Name: [{
                            type: '/attributes/Sword/attributes/Name',
                            uri: '/attributes/Sword/1/Name/2',
                            value: 'Longclaw'
                        }]
                    }
                }]
            });
        });
        it('remove non existing attribute should works well', function () {
            var attributes = {
                Sword: []
            };

            AttributesEditor.removeAttribute(
                attributes,
                {name: 'Material'},
                '/attributes/Material/3');

            expect(attributes).toEqual({
                Sword: []
            });
        });

        it('remove non existing subattribute should works well', function () {
            var attributes = {};

            AttributesEditor.removeAttribute(
                attributes,
                {name: 'Material'},
                'attributes/Material/uri$1461330989808/SubMaterial/uri$1461330990008');

            expect(attributes).toEqual({});
        });

    });

    describe('addAttribute', function () {

        it('can add simple attribute', function () {
            var attributes = {
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                LastName: [{
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/1',
                    value: 'Snow'
                }]
            };

            AttributesEditor.addAttribute(
                attributes,
                {name: 'LastName', uri: '/attributes/LastName'},
                '/attributes/LastName/temp',
                'Stark');

            expect(attributes).toEqual({
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                LastName: [{
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/temp',
                    value: 'Stark'
                }, {
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/1',
                    value: 'Snow'
                }]
            });
        });

        it('should add simple attribute to the end of array if index is Infinity', function () {
            var attributes = {
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                LastName: [{
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/1',
                    value: 'Snow'
                }]
            };

            var index = Infinity;

            AttributesEditor.addAttribute(
                attributes,
                {name: 'LastName', uri: '/attributes/LastName'},
                '/attributes/LastName/temp',
                'Stark',
                undefined,
                undefined,
                index);

            expect(attributes).toEqual({
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                LastName: [{
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/1',
                    value: 'Snow'
                }, {
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/temp',
                    value: 'Stark'
                }]
            });
        });


        it('can\'t add null value', function () {
            var attributes = {
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }]
            };

            AttributesEditor.addAttribute(
                attributes,
                {name: 'LastName', uri: '/attributes/LastName'},
                '/attributes/LastName/temp',
                null);

            expect(attributes['LastName'].length).toEqual(0);
        });

        it('can add simple attribute inside nested', function () {
            var attributes = {
                Sword: [{
                    uri: '/attributes/Sword/1',
                    value: {
                        Name: [{
                            type: '/attributes/Sword/attributes/Name',
                            uri: '/attributes/Sword/1/Name/2',
                            value: 'Longclaw'
                        }]
                    }
                }]
            };

            AttributesEditor.addAttribute(
                attributes,
                {name: 'Material', uri: '/attributes/Sword/attributes/Material'},
                '/attributes/Sword/1/Material/3',
                'Valyrian steel');

            expect(attributes.Sword[0].value.Material[0].value).toBe('Valyrian steel');
        });

        it('can add simple attribute inside nested with specified index', function () {
            var attributes = {
                Sword: [{
                    uri: '/attributes/Sword/1',
                    value: {
                        Name: [
                            {
                                type: '/attributes/Sword/attributes/Name',
                                uri: '/attributes/Sword/1/Name/2',
                                value: 'Longclaw'
                            },
                            {
                                type: '/attributes/Sword/attributes/Name',
                                uri: '/attributes/Sword/1/Name/3',
                                value: 'Oathkeeper'
                            }
                        ]
                    }
                }]
            };
            var index = 1;

            AttributesEditor.addAttribute(
                attributes,
                {name: 'Name', uri: '/attributes/Sword/attributes/Name'},
                '/attributes/Sword/1/Name/new',
                'New sword',
                undefined,
                undefined,
                index);

            expect(attributes).toEqual({
                Sword: [{
                    uri: '/attributes/Sword/1',
                    value: {
                        Name: [
                            {
                                type: '/attributes/Sword/attributes/Name',
                                uri: '/attributes/Sword/1/Name/2',
                                value: 'Longclaw'
                            },
                            {
                                type: '/attributes/Sword/attributes/Name',
                                uri: '/attributes/Sword/1/Name/new',
                                value: 'New sword'
                            },
                            {
                                type: '/attributes/Sword/attributes/Name',
                                uri: '/attributes/Sword/1/Name/3',
                                value: 'Oathkeeper'
                            }
                        ]
                    }
                }]
            });
        });

        it('should add lookup with value and code', function () {
            var attributes = {};

            AttributesEditor.addAttribute(
                attributes,
                {name: 'CountryCode', uri: '/attributes/CountryCode'},
                '/attributes/CountryCode/$123',
                {value: 'Meereen', lookupCode: 'ME'});

            expect(attributes.CountryCode[0].value).toBe('Meereen');
            expect(attributes.CountryCode[0].lookupCode).toBe('ME');
        });

    });

    describe('modify roles and tags', function () {

        it('can modify roles', function () {
            var entity = {
                roles: null
            };
            AttributesEditor.editRoles(entity, ['Rat Cook', 'Arson Iceaxe', 'First King']);
            expect(entity.roles).toEqual(['Rat Cook', 'Arson Iceaxe', 'First King']);
            AttributesEditor.editRoles(entity, null);
            expect(entity.roles).toEqual([]);
        });

        it('can modify tags', function () {
            var entity = {
                roles: null
            };
            AttributesEditor.editTags(entity, ['Bolton', 'Targaryen', 'Stark']);
            expect(entity.tags).toEqual(['Bolton', 'Targaryen', 'Stark']);
            AttributesEditor.editTags(entity, null);
            expect(entity.tags).toEqual([]);
        });

    });

    describe('modify start and end dates', function () {

        it('can modify start date', function () {
            var relation = {
                startDate: null
            };
            AttributesEditor.editStartDate(relation, 123);
            expect(relation.startDate).toEqual(123);
            AttributesEditor.editStartDate(relation, null);
            expect(relation.startDate).toEqual('');
        });

        it('can modify end date', function () {
            var relation = {
                endDate: null
            };
            AttributesEditor.editEndDate(relation, 345);
            expect(relation.endDate).toEqual(345);
            AttributesEditor.editEndDate(relation, null);
            expect(relation.endDate).toEqual('');
        });

    });

});
