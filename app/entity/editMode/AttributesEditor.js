var _ = require('lodash');

/**
 * Use this module to modify objects with attributes (in most cases, entities or relations).
 * Most functions in this module have a similar signature. All functions have the following required arguments:
 * <ol>
 * <li>attributes: entity.attributes or relation.attributes. In all the examples in this topic, the 'attributes' object directly skips entities and relations.</li>
 * <li>attrType: The attribute type from metadata.</li>
 * <li>uri: The URI of an attribute value.</li>
 * </ol>
 *
 * @module entity/editMode/AttributesEditor
 */
module.exports = {

    addAttribute: addAttribute,
    editAttribute: editAttribute,
    removeAttribute: removeAttribute,
    editRoles: editRoles,
    editTags: editTags,
    editStartDate: editStartDate,
    editEndDate: editEndDate
};

    function getPartInfo(name, uri, fullUri, attrType) {
        var result = {
            name: name,
            valueUri: fullUri.substr(0, fullUri.indexOf(uri) + uri.length)
        };

        if (attrType && attrType.uri) {
            result.uri = attrType.uri.substr(0, attrType.uri.indexOf(name) + name.length);
        }

        result.hasChildren = result.valueUri !== fullUri;

        return result;
    }

    function getInfoByUri(uri, attrType) {
        var s = uri.substr(uri.indexOf('/attributes/') + 12); // magic is everywhere, especially in strings and numbers
        var parts = s.split('/');

        var info = [];

        for (var i = 0; i < parts.length; i = i + 2) {
            info.push(getPartInfo(parts[i], parts[i + 1], uri, attrType));
        }

        return info;
    }

    /**
     * Use this function to define attribute values.
     * Alternatively, you can use editAttribute function for the same purpose.
     * If you define an attribute value with a passed URI that is not found, the URI is added.
     *
     * @param {Object} attributes - The attributes object for new value
     * @param {Object} attrType - Attribute type from metadata
     * @param {string} uri - The uri (temporary in this case) of added value
     * @param {string|Object} value - Added value
     * @param {Array} [crosswalks] - Optional crosswalks array. Used for working with reference attributes.
     * @param {number} [nestingLevel] - Optional. Current nesting level of processing.
     * @param {number} [index] - Optional. Used for adding value to specific place in array.
     * @static
     * @example
     * ```
     *  var attributes = {
     *      FirstName: [{
     *          type: '/attributes/FirstName',
     *          uri: '/attributes/FirstName/1',
     *          value: 'Jon'
     *      }],
     *      LastName: [{
     *          type: '/attributes/LastName',
     *          uri: '/attributes/LastName/1',
     *          value: 'Snow'
     *      }]
     *  };
     *
     *  AttributesEditor.addAttribute(
     *  attributes,
     *  {name: 'LastName', uri: '/attributes/LastName'},
     *  '/attributes/LastName/$uri_123',
     *  'Stark');
     *
     *  expect(attributes).toEqual({
     *      FirstName: [{
     *          type: '/attributes/FirstName',
     *          uri: '/attributes/FirstName/1',
     *          value: 'Jon'
     *      }],
     *      LastName: [{
     *          type: '/attributes/LastName',
     *          uri: '/attributes/LastName/1',
     *          value: 'Snow'
     *      }, {
     *          type: '/attributes/LastName',
     *          uri: '/attributes/LastName/$uri_123',
     *          value: 'Stark'
     *      }]
     *  });
     * ```
     */
    function addAttribute(attributes, attrType, uri, value, crosswalks, nestingLevel, index) {
        if (nestingLevel === undefined) {
            nestingLevel = 0;
        }

        var info = getInfoByUri(uri, attrType)[nestingLevel];

        if (info.hasChildren) {
            if (!attributes[info.name]) {
                attributes[info.name] = [{
                    uri: info.valueUri,
                    value: {}
                }];
            }

            var attrValue = attributes[info.name].filter(function (val) {
                return val.uri === info.valueUri;
            })[0];

            return addAttribute(attrValue.value, attrType, uri, value, crosswalks, nestingLevel + 1, index);
        } else {
            var values = attributes[attrType.name];
            if (!values) {
                values = attributes[attrType.name] = [];
            }
            if (value == null) {
                return;
            }
            // no tests for refEntity
            var val = {
                value: value.refEntity ? value.value : value,
                uri: uri,
                type: attrType.uri
            };

            if (value.lookupCode) {
                val.value = value.value;
                val.lookupCode = value.lookupCode;
            }

            if (value.refEntity) {
                val.refEntity = value.refEntity;
                val.label = value.label;
            }

            if (value.refRelation) {
                val.refRelation = value.refRelation;
            }

            if (crosswalks) {
                val.crosswalks = crosswalks;
            }

            values.splice(index || 0, 0, val);

            return value;
        }
    }

    /**
     * Use this function to edit attribute values.
     * If you edit an attribute value with a passed URI that is not be found, the URI is added.
     *
     * @param {Object} attributes - The attributes object for edited value
     * @param {Object} attrType - Attribute type from metadata
     * @param {string} uri - The uri of attribute value
     * @param {string|Object} value - New value
     * @param {Array} [crosswalks] - Optional crosswalks array. Used for working with reference attributes.
     * @static
     * @example
     * ```
     *  var attributes = {
     *      FirstName: [{
     *          type: '/attributes/FirstName',
     *          uri: '/attributes/FirstName/1',
     *          value: 'Jon'
     *      }],
     *      LastName: [{
     *          type: '/attributes/LastName',
     *          uri: '/attributes/LastName/1',
     *          value: 'Snow'
     *      }]
     *  };
     *
     *  AttributesEditor.editAttribute(
     *  attributes,
     *  {name: 'LastName', uri: '/attributes/LastName'},
     *  '/attributes/LastName/1',
     *  'Stark');
     *
     *  expect(attributes).toEqual({
     *      FirstName: [{
     *          type: '/attributes/FirstName',
     *          uri: '/attributes/FirstName/1',
     *          value: 'Jon'
     *      }],
     *      LastName: [{
     *          type: '/attributes/LastName',
     *          uri: '/attributes/LastName/1',
     *          value: 'Stark'
     *      }]
     *  });
     * ```
     */
    function editAttribute(attributes, attrType, uri, value, crosswalks, nestingLevel) {
        if (nestingLevel === undefined) {
            nestingLevel = 0;
        }

        var info = getInfoByUri(uri, attrType)[nestingLevel];

        if (info.hasChildren) {
            var parent = editAttribute(attributes, info, info.valueUri, {}, crosswalks, nestingLevel);
            editAttribute(parent, attrType, uri, value, crosswalks, nestingLevel + 1);
        } else {
            var values = attributes[attrType.name];
            var valueToEdit = values && values.filter(function (val) {
                    return val.uri === uri;
                })[0];
            var result;
            if (!valueToEdit) {
                result = addAttribute(attributes, attrType, uri, value, crosswalks, nestingLevel);
            } else {
                if (valueToEdit.value.toString() !== '[object Object]') { // edit values only for simple attributes
                    updateAttrValue(value, valueToEdit);

                    if (crosswalks) {
                        valueToEdit.crosswalks = crosswalks;
                    }

                    if (attrType.cardinality && attrType.cardinality.minValue === 1 && attrType.cardinality.maxValue === 1) {
                        values
                            .filter(function (val) {
                                return val.ov === false;
                            })
                            .forEach(updateAttrValue.bind(null, value));
                    }
                } else if (value.refEntity) {
                    valueToEdit.refRelation = value.refRelation;
                    valueToEdit.refEntity = value.refEntity;
                    valueToEdit.value = value.value;
                    valueToEdit.label = value.label;
                }
                result = valueToEdit.value;
            }
            return result;
        }
    }

    function updateAttrValue(value, attrValue) {
        if (_.isObject(value)) {
            attrValue.value = value.value;
            attrValue.lookupCode = value.lookupCode;
        } else {
            attrValue.value = value;

            if (attrValue.lookupCode) {
                attrValue.lookupCode = value;
            }
        }
    }

    /**
     * Use this function to remove attribute values.
     *
     * @param {Object} attributes - The attributes object
     * @param {Object} attrType - Attribute type from metadata
     * @param {string} uri - The uri of attribute to remove
     * @static
     * @example
     * ```
     *  var attributes = {
     *      FirstName: [{
     *          type: '/attributes/FirstName',
     *          uri: '/attributes/FirstName/1',
     *          value: 'Jon'
     *      }],
     *      LastName: [{
     *          type: '/attributes/LastName',
     *          uri: '/attributes/LastName/1',
     *          value: 'Snow'
     *      }]
     *  };
     *
     *  AttributesEditor.removeAttribute(
     *  attributes,
     *  {name: 'LastName', uri: '/attributes/LastName'},
     *  '/attributes/LastName/1');
     *
     *  expect(attributes).toEqual({
     *      FirstName: [{
     *          type: '/attributes/FirstName',
     *          uri: '/attributes/FirstName/1',
     *          value: 'Jon'
     *      }]
     *  });
     * ```
     */
    function removeAttribute(attributes, attrType, uri, nestingLevel) {
        if (nestingLevel === undefined) {
            nestingLevel = 0;
        }

        var info = getInfoByUri(uri)[nestingLevel];

        if (info.hasChildren) {
            var complexAttribute = (attributes[info.name] || []).filter(function (attr) {
                return attr.uri === info.valueUri;
            })[0];
            if (complexAttribute) {
                removeAttribute(complexAttribute.value, attrType, uri, nestingLevel + 1);
            }
        } else {
            var values = attributes[attrType.name];
            if (values) {
                values.forEach(function (val, index) {
                    if (val.uri === uri) {
                        values.splice(index, 1);
                    }
                });

                if (values.length === 0) {
                    delete attributes[attrType.name];
                }
            }
        }
    }

    /**
     * Use this function to set the roles for an entity.
     * NOTE: To set this value correctly, use the whole entity object.
     *
     * @param {Object} entity - The entity to modify roles
     * @param {Array|null} value - The string array with roles or null to remove all roles
     * @static
     * @example
     * ```
     *  var entity = {
     *      roles: null
     *  };
     *
     *  AttributesEditor.editRoles(entity, ['Rat Cook', 'Arson Iceaxe', 'First King']);
     *  expect(entity.roles).toEqual(['Rat Cook', 'Arson Iceaxe', 'First King']);
     *
     *  AttributesEditor.editRoles(entity, null);
     *  expect(entity.roles).toEqual([]);
     * ```
     */
    function editRoles(entity, value)
    {
        entity['roles'] = value || [];
    }

    /**
     * Use this function to set tags for an entity.
     * NOTE: To set this value correctly, use the whole entity object.
     *
     * @param {Object} entity - The entity to modify roles
     * @param {Array|null} value - The string array with tags or null to remove all tags
     * @static
     * @example
     * ```
     *  var entity = {
     *      roles: null
     *  };
     *
     *  AttributesEditor.editTags(entity, ['Bolton', 'Targaryen', 'Stark']);
     *  expect(entity.tags).toEqual(['Bolton', 'Targaryen', 'Stark']);
     *
     *  AttributesEditor.editTags(entity, null);
     *  expect(entity.tags).toEqual([]);
     * ```
     */
    function editTags(entity, value)
    {
        entity['tags'] = value || [];
    }

    /**
     * Use this function to set the start date for a relation.
     * NOTE: To set this value correctly, use the whole relation object.
     *
     * @param {Object} entity - Relation to modify start date
     * @param {number|null} value - The time in milliseconds or null to remove start date
     * @static
     * @example
     * ```
     *  var relation = {
     *      startDate: null
     *  };
     *
     *  AttributesEditor.editStartDate(relation, new Date().getTime());
     * ```
     */
    function editStartDate(entity, value)
    {
        entity['startDate'] = value || '';
    }

    /**
     * Use this function to set an end date for a relation.
     * NOTE: To set this value correctly, use the whole relation object.
     *
     * @param {Object} entity - Relation to modify end date
     * @param {number|null} value - The time in milliseconds or null to remove end date
     * @static
     * @example
     * ```
     *  var relation = {
     *      endDate: null
     *  };
     *
     *  AttributesEditor.editEndDate(relation, new Date().getTime());
     * ```
     */
    function editEndDate(entity, value)
    {
        entity['endDate'] = value || '';
    }

