var _ = require('lodash');

function isAttribute(obj) {
    return obj && typeof obj.uri !== 'undefined';
}

function isProfilePicValueEqual(v1, v2) {
    if (v1 !== v2) {
        if (isAttribute(v1) && isAttribute(v2)) {
            return v1.uri === v2.uri;
        }
        return false;
    } else {
        return true;
    }
}

/**
 * Use this module to work with attribute differences.
 * For more information about difference types, see {@link https://help.reltio.com/reltio-manual/api-reference/data-api/entities#toc-57}
 *
 * Typically, to construct a request for a cumulative update endpoint, do the following:
 * <ol>
 * <li>Get the diff values for the original and modified entities (or relations).</li>
 * <li>Apply crosswalks to the diff array.</li>
 * </ol>
 *
 * @module entity/editMode/AttributesDiff
 */
module.exports = {

    compareAttributes: compareAttributes,
    compareImages: compareImages,
    compareActivenessAttributes: compareActivenessAttributes,
    compareTags: compareTags,
    compareRoles: compareRoles,
    applyCrosswalks: applyCrosswalks,
    collectCrosswalksFromAttributes: collectCrosswalksFromAttributes
};

function compareArraysAndGenerateEvent(type) {
    return function checkEquality(a, b) {
        var result = [];
        if (!arraysEquality(a, b)) {
            result.push({
                type: type,
                newValue: b
            });
        }
        return result;
    };
}

function arraysEquality(array1, array2) {
    if (!array1 && !array2) {
        return true;
    } else if (!array1 || !array2) {
        return false;
    }
    return (array1.length === array2.length) && array1.every(function (element, index) {
            return element === array2[index];
        });
}

function refEquality(ref1, ref2) {
    function equalsByField(field) {
        if (ref1[field] && ref2[field]) {
            if (Array.isArray(ref1[field]) && Array.isArray(ref2[field])) {
                if (ref1[field].length !== ref2[field].length) {
                    return false;
                }
                if (ref1[field][0] && ref2[field][0] && ref1[field][0].objectURI !== ref2[field][0].objectURI) {
                    return false;
                }
            } else if (ref1[field].objectURI !== ref2[field].objectURI) {
                return false;
            }
        }

        return true;
    }

    return equalsByField('refEntity') && equalsByField('refRelation');
}

/**
 * Use this function to compare the attributes of two entities (or relations).
 *
 * @param {Object} a - Original entity or relation attributes
 * @param {Object} b - Modified entity or relation attributes
 * @param {Boolean} keepUri - Do not delete temporary uris for INSERT_ATTRIBUTE operation
 * @return {Array} A diff array.
 * @static
 * @example
 * ```
 *  var a = {
 *      FirstName: [{
 *          type: '/attributes/FirstName',
 *          uri: '/attributes/FirstName/1',
 *          value: 'Jon'
 *      }],
 *      LastName: [{
 *          type: '/attributes/LastName',
 *          uri: '/attributes/LastName/1',
 *          value: 'Snow'
 *      }]
 *  };
 *  var b = {
 *      FirstName: [{
 *          type: '/attributes/FirstName',
 *          uri: '/attributes/FirstName/1',
 *          value: 'Jon'
 *      }],
 *      LastName: [{
 *          uri: '/attributes/LastName/1',
 *          type: '/attributes/LastName',
 *          value: 'Snow'
 *      }, {
 *          type: '/attributes/LastName',
 *          uri: 'uri/attributes/LastName/$uri23',
 *          value: 'Stark'
 *      }, {
 *          type: '/attributes/LastName',
 *          uri: 'uri/attributes/LastName/$uri24',
 *          value: 'Targaryen'
 *      }]
 *  };
 *
 *  var result = AttributesDiff.compareAttributes(a, b);
 *
 *  expect(result[0]).toEqual({
 *      type: 'INSERT_ATTRIBUTE',
 *      uri: 'uri/attributes/LastName',
 *      newValue: [
 *          {value: 'Stark'},
 *          {value: 'Targaryen'}
 *      ]
 *  });
 * ```
 */
function compareAttributes(a, b, keepUri) {
    var result = [];

    for (var attrA in a) {
        if (a.hasOwnProperty(attrA)) {
            if (attrA === 'paging') {
                continue;
            }

            var currentAttribute = a[attrA];
            if (b[attrA] === undefined) {
                for (var currentVal in currentAttribute) {
                    if (currentAttribute.hasOwnProperty(currentVal)) {
                        result.push.apply(result, createEntryDeleteAttribute(currentAttribute[currentVal]));
                    }
                }
            } else {
                for (var val in currentAttribute) {
                    if (currentAttribute.hasOwnProperty(val)) {
                        var valFromA = currentAttribute[val];
                        var valFromB = findAttrValue(valFromA, b[attrA]);
                        if (valFromB === undefined) {
                            result.push.apply(result, createEntryDeleteAttribute(valFromA));
                        } else {
                            if (valFromA.value !== valFromB.value && (!valFromA.lookupCode || valFromA.lookupCode !== valFromB.lookupCode)) {
                                if (valFromB.value == null || valFromB.value === '') {
                                    result.push.apply(result, createEntryDeleteAttribute(valFromA));
                                } else if (getAttributeType(valFromA) === 'Nested') {
                                    if  (valFromA.ignored !== valFromB.ignored && _.isEqual(valFromA.value, valFromB.value)){
                                        result.push.apply(result, createIgnoreEvent(valFromB));
                                    } else {
                                        result = result.concat(compareAttributes(valFromA.value, valFromB.value, keepUri));
                                    }
                                } else if (getAttributeType(valFromA) === 'Reference') {
                                    if (!refEquality(valFromA, valFromB)) {
                                        result.push.apply(result, createEntryDeleteAttribute(valFromA));
                                        result.push(createEntryInsertAttribute([valFromB]));
                                    } else {
                                        result.push.apply(result, compareAttributes(valFromA.value, valFromB.value, keepUri));
                                    }
                                } else if (valFromB.value !== '') {
                                    if (valFromB.ov === false) {
                                        result.push.apply(result, createEntryUpdateNonOVAttribute(valFromB));
                                    } else {
                                        result.push.apply(result, createEntryUpdateAttribute(valFromB));
                                    }
                                }
                            } else if (valFromA.ignored !== valFromB.ignored) {
                                result.push.apply(result, createIgnoreEvent(valFromB));
                            }
                        }
                    }
                }
            }
        }
    }

    for (var attrB in b) {
        if (b.hasOwnProperty(attrB)) {
            if (attrB === 'paging') {
                continue;
            }

            var missedValues = (b[attrB] || []).filter(function (value) {
                return findAttrValue(value, a[attrB]) === undefined;
            });
            
            missedValues = removeEmptyValues(missedValues);

            if (missedValues.length) {
                result.push(createEntryInsertAttribute(missedValues));
            }
        }
    }

    return result;
    
    /*
     * values = [{
         "value": {
            "simpleLookup": [{
                "value": "",
                "uri": "entities/KcMEWnB/attributes/Identifiers/uri$$1487657773670/simpleLookup/uri$$1487657773826",
                "type": "configuration/entityTypes/HCP/attributes/Identifiers/attributes/simpleLookup"
            }],
            "simpleReqLookup": [{
                "value": "TheOne",
                "uri": "entities/KcMEWnB/attributes/Identifiers/uri$$1487657773670/simpleReqLookup/uri$$1487657773869",
                "type": "configuration/entityTypes/HCP/attributes/Identifiers/attributes/simpleReqLookup"
            }]
            },
         "uri": "entities/KcMEWnB/attributes/Identifiers/uri$$1487657773670",
         "type": "entities/KcMEWnB/attributes/Identifiers/uri$$1487657773670"
         }]
     */
    function removeEmptyValues (values) {
        if (values) {
            var mappedValues = values.map(function(valueObject) {
                var hasValue;
                if (valueObject) {
                    var value = valueObject.value;
                    
                    if (value != null) {
                        if (_.isObject(value)) {
                            var keys = Object.keys(value);
                            keys.forEach(function (key) {
                                var simpleValue = removeEmptyValues(value[key]);

                                if (simpleValue.length > 0) {
                                    value[key] = simpleValue;
                                } else {
                                    delete value[key];
                                }
                                if (!hasValue) {
                                    hasValue = simpleValue.length !== 0;
                                }

                            });

                        } else {
                            hasValue = value != null && value !== '';
                        }
                    }
                    return hasValue || valueObject.refEntity
                        ? valueObject
                        : null;
                }
            });

            return mappedValues.filter(function(value) {
                return value != null;
            });
        }
        return [];
    }
    
    function createEntryDeleteAttribute(attr) {
        return [{
            type: 'DELETE_ATTRIBUTE',
            uri: attr.uri
        }];
    }

    function createEntryIgnoreAttribute(attr) {
        return [{
            type: 'IGNORE_ATTRIBUTE',
            uri: attr.uri,
            newValue: {value: true}
        }];
    }

    function createEntryUndoIgnoreAttribute(attr) {
        return [{
            type: 'IGNORE_ATTRIBUTE',
            uri: attr.uri,
            newValue: {value: false}
        }];
    }

    function createEntryUpdateAttribute(attr) {
        return [{
            type: 'UPDATE_ATTRIBUTE',
            uri: attr.uri,
            newValue: {
                value: attr.lookupCode || attr.value
            }
        }];
    }

    function createIgnoreEvent(value) {
        return value && (value.ignored ? createEntryIgnoreAttribute(value) : createEntryUndoIgnoreAttribute(value));
    }

    //for internal use only
    function createEntryUpdateNonOVAttribute(attr) {
        var index = attr.uri.lastIndexOf('/');
        var typeUri = attr.uri.substr(0, index);
        return [{
            type: '_UPDATE_NON_OV_ATTRIBUTE',
            uri: attr.uri,
            typeUri: typeUri,
            value: attr.lookupCode || attr.value
        }];
    }

    function createEntryInsertAttribute(missedValues) {
        var uri = missedValues[0].uri;
        var index = uri.lastIndexOf('/');
        uri = uri.substr(0, index);

        var entry = {
            type: 'INSERT_ATTRIBUTE',
            uri: uri,
            newValue: missedValues.map(function (val) {
                var res;
                var newVal = {value: JSON.parse(JSON.stringify(val.lookupCode || val.value))};

                if (keepUri) {
                    newVal.uri = val.uri;
                }

                cleanAttrValue(newVal.value);

                if (val.refEntity) { // test it
                    res = val.refEntity;
                    if (Array.isArray(res)) {
                        res = res[0];
                    }
                    newVal.refEntity = res;
                }

                if (val.refRelation) {
                    res = val.refRelation;
                    if (Array.isArray(res)) {
                        res = res[0];
                    }

                    if (!keepUri){
                        delete res.objectURI;
                    }
                    newVal.refRelation = res;
                }
                return newVal;
            })
        };

        return entry;
    }

    function cleanAttrValue(value) {
        if (value === null || value.toString() !== '[object Object]') {
            return;
        }

        for (var key in value) {
            if (value.hasOwnProperty(key) && key !== 'paging') {
                value[key].forEach(function (attrValue) {
                    delete attrValue.crosswalks;
                    delete attrValue.type;

                    if (!keepUri) {
                        delete attrValue.uri;
                    }


                    cleanAttrValue(attrValue.value);
                });
            }
        }
    }

    function getAttributeType(attrValue) {
        var value = attrValue.value;
        if (_.isObject(value)) {
            return attrValue.hasOwnProperty('refEntity') ? 'Reference' : 'Nested';
        }
    }

    function findAttrValue(matchVal, attr) {
        if (attr === undefined) {
            return undefined;
        }

        return attr.filter(function (val) {
            return val.uri === matchVal.uri;
        })[0];
    }
}

function compareImages(a, b) {
    var result = [];
    if (a.defaultProfilePic !== b.defaultProfilePic) {
        result.push({
            type: 'UPDATE_PROFILE_PIC_BY_URI',
            newValue: {
                value: b.defaultProfilePic
            }
        });
    } else if (!isProfilePicValueEqual(a.defaultProfilePicValue, b.defaultProfilePicValue)) {
        result.push({
            type: 'UPDATE_PROFILE_PIC_BY_URL',
            newValue: {
                value: b.defaultProfilePicValue
            }
        });
    }
    return result;
}

/**
 * Use this method to compare the activeness attributes (start and end date) of two relations.
 *
 * @param {Object} a - Original relation
 * @param {Object} b - Modified relation
 * @return {Array} A diff array.
 * @static
 * @example
 * ```
 *  var a = {
 *      attributes: {}
 *  };
 *  var b = {
 *      attributes: {},
 *      startDate: 518727600000,
 *      endDate: 518727600000
 *  };
 *
 *  var result = AttributesDiff.compareActivenessAttributes(a, b);
 *
 *  expect(result[0]).toEqual({
 *      type: 'UPDATE_START_DATE',
 *      newValue: {
 *          value: '518727600000'
 *      }
 *  });
 *  expect(result[1]).toEqual({
 *      type: 'UPDATE_END_DATE',
 *      newValue: {
 *          value: '518727600000'
 *      }
 *  });
 * ```
 */
function compareActivenessAttributes(a, b) {
    var result = [];

    var startDate = checkAttribute(a.startDate, b.startDate);
    if (startDate !== undefined) {
        result.push({
            type: 'UPDATE_START_DATE',
            newValue: {
                value: startDate
            }
        });
    }

    var endDate = checkAttribute(a.endDate, b.endDate);
    if (endDate !== undefined) {
        result.push({
            type: 'UPDATE_END_DATE',
            newValue: {
                value: endDate
            }
        });
    }

    return result;


    function checkAttribute(a1, b1) {
        var checkResult;
        if (a1 !== b1) {
            if (b1 === undefined) {
                checkResult = '';
            } else {
                checkResult = b1 + '';
            }
        }
        return checkResult;
    }
}

/**
 * Use this function to add crosswalks information to a diff array.
 * You can add crosswalk information only to a diff object, or change some of the operations.
 * For example, you can transform <code>DELETE_ATTRIBUTE</code> to <code>IGNORE_ATTRIBUTE</code> for non-Reltio values.
 *
 * @param {Array} diff - Array of differences (result of compare functions)
 * @param {Array} crosswalks - Array of crosswalks
 * @return {Array} New differences array.
 * @static
 * @example
 * Apply crosswalk only.
 * ```
 *  var diff = [{
 *      type: 'UPDATE_ATTRIBUTE',
 *      uri: '/attributes/LastName/1',
 *      newValue: {
 *          value: 'Snow'
 *      }
 *  }];
 *
 *  var crosswalks = [{
 *      type: 'configuration/sources/Reltio',
 *      value: '123',
 *      attributes: [
 *          '/attributes/LastName/1'
 *      ]
 *  }];
 *
 *  diff = AttributesDiff.applyCrosswalks(diff, crosswalks);
 *
 *  expect(diff[0]).toEqual({
 *      type: 'UPDATE_ATTRIBUTE',
 *      uri: '/attributes/LastName/1',
 *      newValue: {
 *          value: 'Snow'
 *      },
 *      crosswalk: {
 *          type: 'configuration/sources/Reltio',
 *          value: '123'
 *      }
 *  });
 * ```
 * @example
 * DELETE_ATTRIBUTE becomes IGNORE_ATTRIBUTE.
 * ```
 *  var diff = [{
 *      type: 'DELETE_ATTRIBUTE',
 *      uri: '/attributes/LastName/1'
 *  }];
 *
 *  var crosswalks = [{
 *      type: 'configuration/sources/HMS',
 *      value: '123',
 *      attributes: [
 *          '/attributes/LastName/1'
 *      ]
 *  }];
 *
 *  diff = AttributesDiff.applyCrosswalks(diff, crosswalks);
 *
 *  expect(diff[0]).toEqual({
 *      type: 'IGNORE_ATTRIBUTE',
 *      uri: '/attributes/LastName/1',
 *      newValue: {
 *          value: true
 *      }
 *  });
 * ```
 */
function applyCrosswalks(diff, crosswalks) {
    var reltioCrosswalkType = 'configuration/sources/Reltio';

    function setCrosswalk(change, cw) {
        change.crosswalk = {
            type: cw.type,
            value: cw.value
        };

        return change;
    }

    function groupBy(xs, key) {
        return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    }

    function markToRemove(change) {
        return change.removeIt = true;
    }

    var newChanges = diff.map(function (change) {
        if (change.crosswalk) {
            return [change];
        }

        var crosswalksForMe = crosswalks.filter(function (cw) {
            return (cw.attributes || cw.attributeURIs || [])
                    .filter(function (attr) {
                        return attr === change.uri;
                    }).length === 1;
        });

        var reltioCrosswalksForMe = crosswalksForMe.filter(function (cw) {
            return cw.type === reltioCrosswalkType;
        });

        var nonReltioCrosswalksForMe = crosswalksForMe.filter(function (cw) {
            return cw.type !== reltioCrosswalkType;
        });

        var result = [];

        switch (change.type) {
            case 'INSERT_ATTRIBUTE':
                result.push(change);
                break;
            case '_UPDATE_NON_OV_ATTRIBUTE':
                result = result.concat(crosswalksForMe.map(function (cw) {
                    return setCrosswalk(change, cw);
                }));
                break;
            case 'DELETE_ATTRIBUTE':
                if (nonReltioCrosswalksForMe.length || reltioCrosswalksForMe.length == 0) {
                    result.push({
                        type: 'IGNORE_ATTRIBUTE',
                        uri: change.uri,
                        newValue: {value: true}
                    });
                }

                result = result.concat(reltioCrosswalksForMe.map(function (cw) {
                    return setCrosswalk({
                        type: 'DELETE_ATTRIBUTE',
                        uri: change.uri
                    }, cw);
                }));
                break;
            case 'UPDATE_ATTRIBUTE':
                if (!reltioCrosswalksForMe.length) {
                    result.push({
                        type: 'INSERT_ATTRIBUTE',
                        uri: change.uri.substr(0, change.uri.lastIndexOf('/')),
                        newValue: [change.newValue],
                        insteadOfUpdate: true
                    });
                } else {
                    result = result.concat(reltioCrosswalksForMe.map(function (cw) {
                        return setCrosswalk({
                            type: 'UPDATE_ATTRIBUTE',
                            uri: change.uri,
                            newValue: change.newValue
                        }, cw);
                    }));
                }

                if (nonReltioCrosswalksForMe.length) {
                    result.push({
                        type: 'IGNORE_ATTRIBUTE',
                        uri: change.uri,
                        newValue: {value: true}
                    });
                }
                break;
        }

        return result;
    });

    var flatResult = [].concat.apply([], newChanges);

    var groupsObject = groupBy(flatResult, 'typeUri');
    for (var key in groupsObject) {
        if (key === 'undefined') {
            continue;
        }

        var changes = groupsObject[key];

        var changesRelatedToThisGroup = flatResult
            .filter(function (change) {
                return change.type !== '_UPDATE_NON_OV_ATTRIBUTE';
            })
            .filter(function (change) {
                return change.uri.substr(0, change.uri.lastIndexOf('/')) === key || (change.type === 'INSERT_ATTRIBUTE' && change.uri === key);
            });

        var firstReltioValue = changes.filter(function (change) {
            return change.crosswalk && change.crosswalk.type === reltioCrosswalkType;
        })[0];

        var simpleUpdateAttributeChange = changesRelatedToThisGroup.filter(function (change) {
            return change.type === 'UPDATE_ATTRIBUTE';
        })[0];

        if (!simpleUpdateAttributeChange && firstReltioValue) {
            flatResult.push({
                type: 'UPDATE_ATTRIBUTE',
                uri: firstReltioValue.uri,
                newValue: {
                    value: firstReltioValue.value
                },
                crosswalk: firstReltioValue.crosswalk
            });

            changesRelatedToThisGroup.forEach(markToRemove);
        }

        changes.forEach(markToRemove);
    }

    flatResult = flatResult.filter(function (change) {
        return !change.removeIt;
    });

    flatResult.forEach(function (change) {
        delete change.insteadOfUpdate;
    });

    return flatResult;
}

/**
 * Relation attributes have their own crosswalks.
 * To get correct results when you use the <code>applyCrosswalks</code> function, all crosswalks are required.
 * This function returns all crosswalks from entities (or relations) and their reference attributes.
 *
 * @param {Object} a - attributes from entity or relation
 * @param {Array} crosswalks - Array of crosswalks
 * @return {Array} New array of crosswalks.
 * @static
 */
function collectCrosswalksFromAttributes(a, crosswalks) {
    crosswalks = crosswalks || [];
    for (var attrName in a) {
        if (a.hasOwnProperty(attrName)) {
            var arrAttr = a[attrName];
            for (var i = 0; i < arrAttr.length; i++) {
                var attr = arrAttr[i];
                if (attr.refRelation) {
                    var refRelation = attr.refRelation[0] || attr.refRelation;
                    if (refRelation.crosswalks) {
                        var relCrosswalks = refRelation.crosswalks.filter(function (cw) {
                            return cw.attributes || cw.attributeURIs;
                        });
                        for (var j = 0; j < relCrosswalks.length; j++) {
                            var append = true;
                            for (var k = 0; k < crosswalks.length; k++) {
                                if (crosswalks[k].type === relCrosswalks[j].type && crosswalks[k].value === relCrosswalks[j].value) {
                                    crosswalks[k].attributes = crosswalks[k].attributes || crosswalks[k].attributeURIs;
                                    crosswalks[k].attributes = crosswalks[k].attributes.concat(relCrosswalks[j].attributes || relCrosswalks[j].attributeURIs);

                                    //Unique values in array
                                    crosswalks[k].attributes = Object.keys(crosswalks[k].attributes.reduce(function (result, item) {
                                        result[item] = true;
                                        return result;
                                    }, {}));

                                    delete crosswalks[k].attributeURIs;
                                    append = false;
                                    break;
                                }
                            }
                            if (append) {
                                crosswalks.push(relCrosswalks[j]);
                            }
                        }
                    }
                }
            }
        }
    }

    return crosswalks;
}

/**
 * Use this function to compare the tags of two entities.
 *
 * @param {Object} a - Original entity
 * @param {Object} b - Modified entity
 * @return {Array} A diff array.
 * @static
 * @example
 * ```
 *  var diff = AttributesDiff.compareTags(null, ['Lannisters', 'Baratheon']);
 *
 *  expect(diff[0]).toEqual({
 *      type: 'UPDATE_TAGS',
 *      newValue: ['Lannisters', 'Baratheon']
 *  });
 * ```
 */
function compareTags(a, b) {
    return compareArraysAndGenerateEvent('UPDATE_TAGS').call(this, a, b);
}

/**
 * Use this function to compare the roles of two entities.
 *
 * @param {Object} a - Original entity
 * @param {Object} b - Modified entity
 * @return {Array} A diff array.
 * @static
 * @example
 * ```
 *  var diff = AttributesDiff.compareRoles(null, ['Lann', 'Azor Ahai']);
 *
 *  expect(diff[0]).toEqual({
 *      type: 'UPDATE_ROLES',
 *      newValue: ['Lann', 'Azor Ahai']
 *  });
 * ```
 */
function compareRoles(a, b) {
    return compareArraysAndGenerateEvent('UPDATE_ROLES').call(this, a, b);
}