/**
 * Use this module to implement transactions.
 * @module entity/transactions/Transactions
 */

module.exports = {
    createTransaction: createTransaction
};


function getProjection(source, events, mutator) {
    var clonnedSource = JSON.parse(JSON.stringify(source));
    (events).forEach(function (event) {
        mutator.call(null, clonnedSource, event)
    });
    return clonnedSource;
}

function notify(events, eventObserver) {
    if (eventObserver) {
        events.forEach(function (event) {
            eventObserver.call(null, event);
        });
    }
}
/**
 * Method creates new transaction
 * @static
 * @param source {*} source object
 * @param mutator {Function} mutator function
 * @param eventObserver {Function?} event observer function. It is necessary to listen for deleted events.
 * @returns {{setInitProjection: setInitProjection, cleanEvents: cleanEvents, branch: branch, getCurrentProjection: getCurrentProjection, pushEvent: pushEvent}}
 */
function createTransaction(source, mutator, eventObserver) {
    var events = [];
    return {

        /**
         * @private
         * @param projection
         */
        setInitProjection: function (projection) {
            source = projection;
        },

        /**
         * @private
         */
        cleanEvents: function (shouldNotify) {
            if (shouldNotify) {
                notify(events, eventObserver);
            }
            var old = events.slice();
            events = [];
            return old;
        },
        /**
         * Method creates new branch and returns it.
         * @memberof module:entity/transactions/Transactions
         * @instance
         * @returns {{checkRemoved: checkRemoved, cleanEvents: cleanEvents, saveEvents: saveEvents, setInitProjection: setInitProjection, pushEvent: pushEvent, getCurrentProjection: getCurrentProjection, branch: branch, save: save, remove: remove}} created branch
         */
        branch: function (previousBranch) {
            var newSource = this.getCurrentProjection();
            var removed = false;
            var savedEvents = [];
            previousBranch = previousBranch || this;
            var transaction = createTransaction(newSource, mutator, eventObserver);

            return {
                /**
                 * @private
                 */
                checkRemoved: function () {
                    if (removed) {
                        throw new Error('Removed branch can\'t be modified');
                    }
                },
                /**
                 * @private
                 * @returns {*}
                 */
                cleanEvents: function () {
                    this.checkRemoved();
                    return transaction.cleanEvents();
                },
                /**
                 * @private
                 */
                saveEvents: function (events) {
                    savedEvents = savedEvents.concat(events);
                },
                /**
                 * @private
                 * @param projection
                 */
                setInitProjection: function (projection) {
                    this.checkRemoved();
                    newSource = projection;
                    transaction.setInitProjection(projection);
                },
                /**
                 * Method add event to queue. All events will be applied on getProjection method execution
                 * @param event {*}
                 */
                pushEvent: function (event) {
                    this.checkRemoved();
                    transaction.pushEvent(event);
                },
                /**
                 * Method returns the current state of initial object
                 * @inner
                 * @returns {*}
                 */
                getCurrentProjection: function () {
                    this.checkRemoved();
                    return transaction.getCurrentProjection();
                },
                /**
                 * Method creates new branch and returns it.
                 * @inner
                 * @returns {{checkRemoved: checkRemoved, cleanEvents: cleanEvents, saveEvents: saveEvents, setInitProjection: setInitProjection, pushEvent: pushEvent, getCurrentProjection: getCurrentProjection, branch: branch, save: save, remove: remove}} created branch
                 */
                branch: function () {
                    this.checkRemoved();
                    return transaction.branch(this);
                },
                /**
                 * Method save the current branch. The initial state will be changed by actual state.
                 * @inner
                 * @returns {*|createTransaction} the previous branch or initial transaction
                 */
                save: function () {
                    this.checkRemoved();
                    var projection = this.getCurrentProjection();
                    previousBranch.setInitProjection(projection);
                    var events = previousBranch.cleanEvents().concat(transaction.cleanEvents());
                    if (previousBranch.saveEvents) {
                        previousBranch.saveEvents(events);
                    }
                    removed = true;
                    return previousBranch;
                },
                /**
                 * Method removes the current branch. All the deleted events will be passed to the observer
                 * @inner
                 * @returns {*|createTransaction} the previous branch or initial transaction
                 */
                remove: function () {
                    this.checkRemoved();
                    removed = true;
                    transaction.cleanEvents(true);
                    notify(savedEvents, eventObserver);
                    savedEvents = [];
                    return previousBranch;
                }
            }
        },
        /**
         * Method returns the current state of initial object
         * @instance
         * @memberof module:entity/transactions/Transactions
         * @returns {*}
         */
        getCurrentProjection: function () {
            return getProjection(source, events, mutator);
        },
        /**
         * Method add event to queue. All events will be applied on getProjection method execution
         * @instance
         * @memberof module:entity/transactions/Transactions
         * @param event {*}
         */
        pushEvent: function (event) {
            events.push(event);
        }

    }
}