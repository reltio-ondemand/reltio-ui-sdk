jest.unmock('../Transactions');
jest.unmock('../../editMode/AttributesEditor');
var Transactions = require('../Transactions');
var AttributesEditor = require('../../editMode/AttributesEditor');
describe('simple transaction test', function () {

    var mutator = function (source, event) {
        var payload = event.payload;
        switch (event.type) {
            case 'edit':
                AttributesEditor.editAttribute(source, payload.attrType, payload.uri, payload.value, payload.crosswalks);
                break;
            case 'remove':
                AttributesEditor.removeAttribute(source, payload.attrType, payload.uri);
        }
    };

    var createCommand = function (event) {
        return function (transaction) {
            return function (attrType, uri, value, crosswalks) {
                transaction.pushEvent({
                    type: event,
                    payload: {
                        attrType: attrType,
                        uri: uri,
                        value: value,
                        crosswalks: crosswalks
                    }
                })
            }
        }
    };
    var editCommand = createCommand('edit');
    var removeCommand = createCommand('remove');

    describe('simple attribute transaction editing test', function () {
        var attributes = {
            FirstName: [{
                type: '/attributes/FirstName',
                uri: '/attributes/FirstName/1',
                value: 'Jon'
            }],
            LastName: [{
                type: '/attributes/LastName',
                uri: '/attributes/LastName/1',
                value: 'Snow'
            }]
        };
        var transaction, edit, remove;
        beforeEach(function () {
            transaction = Transactions.createTransaction(attributes, mutator);
            edit = editCommand(transaction);
            remove = removeCommand(transaction)
        });
        it('should work for simple attribute editing', function () {
            edit({name: 'LastName'},
                '/attributes/LastName/1',
                'Stark');

            expect(transaction.getCurrentProjection()).toEqual({
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                LastName: [{
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/1',
                    value: 'Stark'
                }]
            });
        });
        it('should work in case several simple attribute editing', function () {
            var original = JSON.parse(JSON.stringify(attributes));
            edit({name: 'LastName'},
                '/attributes/LastName/1',
                'Stark');

            edit({name: 'LastName', uri: '/attributes/LastName'},
                '/attributes/LastName/2',
                'Azor Ahai');

            remove({name: 'LastName'},
                '/attributes/LastName/1');

            expect(transaction.getCurrentProjection()).toEqual({
                FirstName: [{
                    type: '/attributes/FirstName',
                    uri: '/attributes/FirstName/1',
                    value: 'Jon'
                }],
                LastName: [{
                    type: '/attributes/LastName',
                    uri: '/attributes/LastName/2',
                    value: 'Azor Ahai'
                }]
            });
            expect(original).toEqual(attributes);
        });
    });

    describe('nested attribute transaction editing test', function () {
        var attributes = {};
        var transaction, edit;
        beforeEach(function () {
            transaction = Transactions.createTransaction(attributes, mutator);
            edit = editCommand(transaction);
        });
        it('should create nested attribute', function () {
            edit({name: 'Owner', uri: '/attributes/Sword/attributes/Material/attributes/Owner'},
                'entity-uri/attributes/Sword/$uri_123/Material/$uri_456/Owner/$uri_789',
                'Jon Snow');
            var branch = transaction.branch();
            var editBranch = editCommand(branch);
            editBranch({name: 'Type', uri: '/attributes/Sword/attributes/Material/attributes/Type'},
                'entity-uri/attributes/Sword/$uri_123/Material/$uri_456/Type/$uri_789',
                'Valyrian steel');
            branch.save();
            var projection = transaction.getCurrentProjection();
            expect(projection['Sword'][0].value['Material'][0].value['Type']).toEqual(
                [{
                    type: '/attributes/Sword/attributes/Material/attributes/Type',
                    uri: 'entity-uri/attributes/Sword/$uri_123/Material/$uri_456/Type/$uri_789',
                    value: 'Valyrian steel'
                }]
            );
            expect(projection['Sword'][0].value['Material'][0].value['Owner']).toEqual(
                [{
                    type: '/attributes/Sword/attributes/Material/attributes/Owner',
                    uri: 'entity-uri/attributes/Sword/$uri_123/Material/$uri_456/Owner/$uri_789',
                    value: 'Jon Snow'
                }]
            );
        });
        it('shouldn\'t affect the main attribute during editing', function () {
            var branch = transaction.branch();
            var editBranch = editCommand(branch);
            editBranch({name: 'Owner', uri: '/attributes/Sword/attributes/Material/attributes/Owner'},
                'entity-uri/attributes/Sword/$uri_123/Material/$uri_456/Owner/$uri_789',
                'Jon Snow');
            var newBranch = branch.branch();
            editBranch = editCommand(newBranch);
            editBranch({name: 'Type', uri: '/attributes/Sword/attributes/Material/attributes/Type'},
                'entity-uri/attributes/Sword/$uri_123/Material/$uri_456/Type/$uri_789',
                'Valyrian steel');
            newBranch.remove();
            branch.save();

            var projection = transaction.getCurrentProjection();
            expect(projection['Sword'][0].value['Material'][0].value['Type']).toBeUndefined();
            expect(projection['Sword'][0].value['Material'][0].value['Owner']).toEqual(
                [{
                    type: '/attributes/Sword/attributes/Material/attributes/Owner',
                    uri: 'entity-uri/attributes/Sword/$uri_123/Material/$uri_456/Owner/$uri_789',
                    value: 'Jon Snow'
                }]
            );
        });
    })

});