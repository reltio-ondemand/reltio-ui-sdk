jest.unmock('../Transactions');
var Transactions = require('../Transactions');
describe('simple transaction test', function () {
    var createEvent = function (action) {
        return function (key, value) {
            return {
                action: action,
                payload: {
                    key: key,
                    value: value
                }
            }
        }
    };
    var addData = createEvent('add');
    var removeData = createEvent('remove');
    var mutator = function (source, event) {
        var payload = event.payload;
        if (event.action === 'add') {
            source[payload.key] = payload.value;
        } else if (event.action === 'remove') {
            delete source[payload.key];
        }
    };
    describe('main transaction tests', function () {
        it('should create correct projection for simple events and simple mutator', function () {
            var transaction = Transactions.createTransaction({}, mutator);
            transaction.pushEvent(addData('test', 'test'));
            expect(transaction.getCurrentProjection()).toEqual({test: 'test'});
        });

        it('should create correct projection for simple add/remove events and simple mutator', function () {
            var transaction = Transactions.createTransaction({}, mutator);
            transaction.pushEvent(addData('test', 'test'));
            transaction.pushEvent(removeData('test'));
            expect(transaction.getCurrentProjection()).toEqual({});
        });

        it('shouldn\'t modify original source', function () {
            var original = {};
            var transaction = Transactions.createTransaction({}, mutator);
            transaction.pushEvent(addData('test', 'test'));
            expect(transaction.getCurrentProjection()).toEqual({test: 'test'});
            expect(original).toEqual({});
        });
    });
    describe('branch simple tests', function () {
        it('should merge branch correctly', function () {
            var transaction = Transactions.createTransaction({}, mutator);
            transaction.pushEvent(addData('transaction', 'started'));
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'created'));
            branch.save();
            expect(transaction.getCurrentProjection()).toEqual({
                transaction: 'started',
                branch: 'created'
            })
        });

        it('should merge branch and branch correctly', function () {
            var transaction = Transactions.createTransaction({}, mutator);
            transaction.pushEvent(addData('transaction', 'started'));
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'created'));
            branch = branch.branch();
            branch.pushEvent(addData('branch2', 'created'));
            branch = branch.save();
            branch.pushEvent(removeData('branch'));
            branch.save();
            expect(transaction.getCurrentProjection()).toEqual({
                transaction: 'started',
                branch2: 'created'
            })
        });

        it('should merge events historically', function () {
            var transaction = Transactions.createTransaction({}, mutator);
            transaction.pushEvent(addData('transaction', 'started'));
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'created'));
            branch = branch.branch();
            branch.pushEvent(addData('branch2', 'created'));
            branch = branch.branch();
            branch.pushEvent(addData('branch3', 'created'));
            branch = branch.branch();
            branch.pushEvent(addData('branch4', 'created'));
            branch = branch.save();
            branch = branch.save();
            branch.pushEvent(addData('branch2', 'updated'));
            branch = branch.save();
            branch.save();
            expect(transaction.getCurrentProjection()).toEqual({
                branch: 'created',
                branch2: 'updated',
                branch3: 'created',
                branch4: 'created',
                transaction: 'started'

            })
        });

        it('should\'t affect the main transaction on branch removing', function () {
            var transaction = Transactions.createTransaction({}, mutator);
            transaction.pushEvent(addData('transaction', 'started'));
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'created'));
            branch.remove();
            expect(transaction.getCurrentProjection()).toEqual({
                transaction: 'started'
            })
        });

        it('should remove all child branches in case of the initial branch removing', function () {
            var transaction = Transactions.createTransaction({}, mutator);
            transaction.pushEvent(addData('transaction', 'started'));
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'created'));
            var newBranch = branch.branch();
            newBranch.pushEvent(addData('branch2', 'created'));
            branch.remove();
            expect(transaction.getCurrentProjection()).toEqual({
                transaction: 'started'
            })
        });
        it('should throw an error in case of operation on closed branch', function () {
            var transaction = Transactions.createTransaction({}, mutator);
            transaction.pushEvent(addData('transaction', 'started'));
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'created'));
            branch.remove();
            expect(function () {
                branch.pushEvent(addData('branch', 'updated'));
            }).toThrow(new Error('Removed branch can\'t be modified'));
        });
        it('should return the initial transaction when all branches were closed', function () {
            var transaction = Transactions.createTransaction({}, mutator);
            transaction.pushEvent(addData('transaction', 'started'));
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'created'));
            branch = branch.remove();
            expect(transaction).toBe(branch);
        });
    });

    describe('event observer test', function () {
        it('should notify about all branch events on branch remove', function () {
            var eventObserver = jest.fn();
            var transaction = Transactions.createTransaction({}, mutator, eventObserver);
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'started'));
            branch.pushEvent(addData('branch', 'updated'));
            branch.remove();
            expect(eventObserver.mock.calls.length).toBe(2);
            expect(eventObserver.mock.calls[0]).toContainEqual(
                {
                    action: 'add',
                    payload: {key: 'branch', value: 'started'}
                }
            );
            expect(eventObserver.mock.calls[1]).toContainEqual(
                {
                    action: 'add',
                    payload: {key: 'branch', value: 'updated'}
                }
            );
        });
        it('shouldn\'t notify about all branch events on branch save', function () {
            var eventObserver = jest.fn();
            var transaction = Transactions.createTransaction({}, mutator, eventObserver);
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'started'));
            branch.pushEvent(addData('branch', 'updated'));
            branch.save();
            expect(eventObserver.mock.calls.length).toBe(0);
        });
        it('should save events from sub branch', function () {
            var eventObserver = jest.fn();
            var transaction = Transactions.createTransaction({}, mutator, eventObserver);
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'started'));
            var newBranch = branch.branch();
            newBranch.pushEvent(addData('branch', 'updated'));
            newBranch.save();
            branch.remove();
            expect(eventObserver.mock.calls.length).toBe(2);
        });

        it('should save events for complex scenario', function () {
            var eventObserver = jest.fn();
            var transaction = Transactions.createTransaction({}, mutator, eventObserver);
            var branch = transaction.branch();
            branch.pushEvent(addData('branch', 'started'));
            var newBranch = branch.branch();
            newBranch.pushEvent(addData('branch', 'updated'));
            newBranch.remove();
            eventObserver.mockClear();
            branch.remove();
            expect(eventObserver.mock.calls.length).toBe(1);
        });
    });
});