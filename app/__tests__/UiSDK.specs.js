jest.unmock('../ui-sdk.js');
var uiSDK = require('../ui-sdk');
describe('UISDK interface tests', function () {
    it('should provide corrects parent modules', function () {
        expect(uiSDK).toBeDefined();
        expect(Object.keys(uiSDK).length).toBe(3);
    });

    it('should provide correct modules for entity', function () {
        expect(uiSDK.entity).toBeDefined();
        expect(Object.keys(uiSDK.entity).length).toBe(5);
        expect(uiSDK.entity.AttributesDiff).toBeDefined();
        expect(uiSDK.entity.AttributesEditor).toBeDefined();
        expect(uiSDK.entity.MultiparentTree).toBeDefined();
        expect(uiSDK.entity.HistoryManager).toBeDefined();
        expect(uiSDK.entity.Transactions).toBeDefined();
    });
    it('should provide correct modules for utils', function () {
        expect(uiSDK.utils).toBeDefined();
        expect(Object.keys(uiSDK.utils).length).toBe(3);
        expect(uiSDK.utils.Colors).toBeDefined();
        expect(uiSDK.utils.Util).toBeDefined();
        expect(uiSDK.utils.DependencyManager).toBeDefined();
    });
    it('should provide Metadata', function () {
        expect(uiSDK.Metadata).toBeDefined();
    });
});
