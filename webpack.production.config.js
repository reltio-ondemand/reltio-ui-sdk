const path = require('path');
const buildPath = path.resolve(__dirname, 'dist', 'build');
const mainPath = path.resolve(__dirname, 'app', 'ui-sdk.js');

var config = {
    devtool: 'source-map',
    entry: mainPath,
    output: {
        path: buildPath,
        filename: 'ui-sdk.js',
        libraryTarget: 'umd',
        library: 'uiSDK'
    }
};

module.exports = config;